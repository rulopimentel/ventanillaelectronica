﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VentanillaElectronica.Models
{

    public class VentanillaPeriodo
    {
        private string _Nombre;

        public VentanillaPeriodo()
        {
            InscripcionMovilidad = new List<VentanillaInscripcionMovilidad>();
            InscripcionBajas = new List<VentanillaInscripcionBajas>();
        }

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Nombre es requerido")]
        [Display(Name = "Nombre o descripción del periodo")]
        public string Nombre
        {
            get
            {
                return _Nombre;
            }
            set
            {
                _Nombre = value != null ? value.ToUpper() : null;
            }
        }

        public DateTime? FechaRegistro { get; set; }

        public DateTime? FechaActualizacion { get; set; }

        public virtual List<VentanillaInscripcionMovilidad> InscripcionMovilidad { get; set; }
        public virtual List<VentanillaInscripcionBajas> InscripcionBajas { get; set; }


    }
}
