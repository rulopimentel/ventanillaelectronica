﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VentanillaElectronica.Models
{

    public class VentanillaAcademico
    {
        private string _Nombre;

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Nombre es requerido")]
        [Display(Name = "Nombre completo del usuario")]
        public string Nombre
        {
            get
            {
                return _Nombre;
            }
            set
            {
                _Nombre = value != null ? value.ToUpper() : null;
            }
        }

       
        public DateTime? FechaRegistro { get; set; }

     


    }
}
