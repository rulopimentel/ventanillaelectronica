﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace VentanillaElectronica.Models
{

    public class VentanillaTramitesUltimaOportunidad
    {

        private string _ExperienciaEducativa;
        private string _Docente;
        private string _DocenteAplicacion1;
        private string _DocenteAplicacion2;


        [Key, ForeignKey("Tramite")]
        public int Id { get; set; }

        public virtual VentanillaTramite Tramite { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        public int? RegionProgramaId { get; set; }

        [ForeignKey("RegionProgramaId")]
        [Display(Name = "Programa")]
        public virtual RegionPrograma RegionPrograma { get; set; }

        [Display(Name = "¿Presenta algún inconveniente al presentar el examen con el titular?")]
        public bool PresentaInconveniente { get; set; }

        [NotMapped]
        [Range(typeof(bool), "true", "true", ErrorMessage = "Debes de aceptar esta casilla de verificación")]
        [Display(Name = "Acepto que: La no acreditación de este examen hará que cause baja definitiva del programa educativo")]
        public bool TerminosYCondiciones1 { get; set; }

        [Required(ErrorMessage = "El nombre de la Experiencia Educativa es requerido")]
        [Display(Name = "Experiencia Educativa")]
        public string ExperienciaEducativa
        {
            get
            {
                return _ExperienciaEducativa;
            }
            set
            {
                _ExperienciaEducativa = value != null ? value.ToUpper() : null;
            }
        }

        [Required(ErrorMessage = "El Nombre del titular es requerido")]
        [Display(Name = "Nombre del titular")]
        public string Docente
        {
            get
            {
                return _Docente;
            }
            set
            {
                _Docente = value != null ? value.ToUpper() : null;
            }
        }

        [DisplayFormat(DataFormatString = "{0:d MMM yy HH:mm}")]
        [Display(Name = "Fecha de revisión")]
        public DateTime? FechaRevisado
        {
            get; set;
        }

        public int? VentanillaAdministrativoRevisionId { get; set; }
        [ForeignKey("VentanillaAdministrativoRevisionId")]
        [Display(Name = "Revisado por")]
        public virtual VentanillaAdministrativo AdministrativoRevision { get; set; }

        [Display(Name = "Cardex")]
        public string Url { get; set; }


        [Display(Name = "Docente 1")]
        public string DocenteAplicacion1
        {
            get
            {
                return _DocenteAplicacion1;
            }
            set
            {
                _DocenteAplicacion1 = value != null ? value.ToUpper() : null;
            }
        }

        [Display(Name = "Docente 2")]
        public string DocenteAplicacion2
        {
            get
            {
                return _DocenteAplicacion2;
            }
            set
            {
                _DocenteAplicacion2 = value != null ? value.ToUpper() : null;
            }
        }

        [DisplayFormat(DataFormatString = "{0:d MMM yy}")]
        [Display(Name = "Fecha Consejo Técnico")]
        public DateTime? FechaConsejoTecnico
        {
            get; set;
        }

        [DisplayFormat(DataFormatString = "{0:d MMM yy HH:mm}")]
        [Display(Name = "Fecha del Examen")]
        public DateTime? FechaExamen
        {
            get; set;
        }

        [DisplayFormat(DataFormatString = "{0:d MMM yy HH:mm}")]
        [Display(Name = "Fecha de resolución")]
        public DateTime? FechaResolucion
        {
            get; set;
        }

        public int? VentanillaAdministrativoResolucionId { get; set; }
        [ForeignKey("VentanillaAdministrativoResolucionId")]
        [Display(Name = "Resolución guardada por")]
        public virtual VentanillaAdministrativo AdministrativoResolucion { get; set; }


        public EnumTramitesUltimaOportunidadEstatus? Estatus { get; set; }


    }
}
