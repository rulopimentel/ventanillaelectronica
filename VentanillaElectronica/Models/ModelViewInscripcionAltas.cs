﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace VentanillaElectronica.Models
{

    public class ModelViewInscripcionAltas
    {
        public ModelViewInscripcionAltas()
        {
            inscripcionAltas = new List<VentanillaInscripcionAltas>();
        }
        public VentanillaPeriodo periodo { get; set; }

        public List<VentanillaInscripcionAltas> inscripcionAltas { get; set; }

        public VentanillaAlumno alumno { get; set; }

    }

    public class ModelViewTramitesAltas
    {

        public VentanillaPeriodo periodo { get; set; }

        public VentanillaTramite tramite { get; set; }

        public VentanillaAlumno alumno { get; set; }

        public VentanillaInscripcionAltas inscripcionAlta { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Número de inscripción")]
        public EnumNumeroInscripcion NumeroInscripcion { get; set; }

    }


    public class ModelViewServeTramitesAlta
    {
         [Key]
        public int Id { get; set; }

        public int VentanillaOfertaAltasId { get; set; }

        [ForeignKey("VentanillaOfertaAltasId")]
        public virtual VentanillaOfertaAltas OfertaAlta { get; set; }

        public int VentanillaAlumnoId { get; set; }

        [ForeignKey("VentanillaAlumnoId")]
        public virtual VentanillaAlumno Alumno { get; set; }
        public EnumTramitesEstatus Estatus { get; set; }

        [Display(Name = "Estatus")]
        [Required(ErrorMessage = "Estatus requerido")]
        public EnumTramitesEstatusServe EstatusServe { get; set; }

        public int? VentanillaAdministrativoId { get; set; }

        [ForeignKey("VentanillaAdministrativoId")]
        [Display(Name = "Atendido por")]

        public virtual VentanillaAdministrativo Administrativo { get; set; }

        [DisplayFormat(DataFormatString = "{0:d MMM yy HH:mm}")]
        [Display(Name = "Fecha de registro")]
        public DateTime? FechaRegistro { get; set; }

        [Required(ErrorMessage = "Observaciones requeridas")]
        [Display(Name = "Observaciones")]
        public string Observaciones { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Número de inscripción")]
        public EnumNumeroInscripcion NumeroInscripcion { get; set; }

    }

}
