﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VentanillaElectronica.Models
{

    public class VentanillaAlumno
    {
        private string _Nombre;
        private string _UserName;
        private string _Email;
        private string _EmailAlterno;
        private string _ConfirmacionEmailAlterno;
       

        public VentanillaAlumno()
        {
            Tramites = new List<VentanillaTramite>();
        }

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Nombre es requerido")]
        [Display(Name = "Nombre completo")]
        public string Nombre
        {
            get
            {
                return _Nombre;
            }
            set
            {
                _Nombre = value != null ? value.ToUpper() : null;
            }
        }

        [Required(ErrorMessage = "Matrícula es requerida")]
        [Display(Name = "Matrícula")]
        public string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {
                _UserName = value != null ? value.ToLower() : null;
            }
        }

        [Required(ErrorMessage = "Correo institucional es requerido")]
        [Display(Name = "Correo institucional")]
        [EmailAddress(ErrorMessage = "Formato de correo no válido")]


        public string Email
        {
            get
            {
                return _Email;
            }
            set
            {
                _Email = value != null ? value.ToLower() : null;
            }
        }

        [Required(ErrorMessage = "Correo alterno es requerido")]
        [EmailAddress(ErrorMessage = "Formato de correo no válido")]
        [Display(Name = "Correo alterno")]

        public string EmailAlterno
        {
            get
            {
                return _EmailAlterno;
            }
            set
            {
                _EmailAlterno = value != null ? value.ToLower() : null;
            }
        }

        [NotMapped]
        [Required(ErrorMessage = "Confirmación de correo alterno requerido")]
        [EmailAddress(ErrorMessage = "Formato de correo no válido")]
        [Display(Name = "Confirmación de correo alterno")]
        [Compare("EmailAlterno", ErrorMessage = "Los correos no coinciden")]


        public string ConfirmacionEmailAlterno
        {
            get
            {
                return _ConfirmacionEmailAlterno;
            }
            set
            {
                _ConfirmacionEmailAlterno = value != null ? value.ToLower() : null;
            }
        }

        [Required(ErrorMessage = "PrefijoInternacional es requerido")]
        [StringLength(5)]
        [RegularExpression(@"^[+]\d{2,5}$", ErrorMessage = "Formato inválido en prefijo_internacional")]
        public string PrefijoInternacional { get; set; }

        [Required(ErrorMessage = "Teléfono es requerido")]
        [Display(Name = "Teléfono celular (10 dígitos)")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "Formato inválido en Teléfono")]
        public string Telefono { get; set; }

        public DateTime? FechaRegistro { get; set; }

        public DateTime? FechaActualizacion { get; set; }

        [Display(Name = "Región")]
        [Required(ErrorMessage = "Campo requerido")]
        public EnumRegion Region { get; set; }

        [Display(Name = "Programa")]
        [Required(ErrorMessage = "Campo requerido")]
        public EnumPrograma Programa { get; set; }

        public virtual List<VentanillaTramite> Tramites { get; set; }



    }
}
