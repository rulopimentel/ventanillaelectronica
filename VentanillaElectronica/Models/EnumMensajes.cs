﻿using System.ComponentModel.DataAnnotations;

namespace VentanillaElectronica.Models
{
    public enum EnumTramitesEscenarios
    {
        RegistroExitoso = 0,
        RegistroExitente = 1,
        CupoSaturado = 2,
        PeriodoNoDisponible = 3,
        Error = 4,
        ItentosAgotados = 5
    }

}