﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VentanillaElectronica.Models
{

    public class VentanillaInscripcionBajas
    {
        
        [Key]
        public int Id { get; set; }

        public int VentanillaPeriodoId { get; set; }

        [ForeignKey("VentanillaPeriodoId")]
        public virtual VentanillaPeriodo Periodo { get; set; } 

        [DisplayFormat(DataFormatString = "{0:d MMM HH:mm}")]
        public DateTime? FechaInscripcionInicial { get; set; }

        [DisplayFormat(DataFormatString = "{0:d MMM HH:mm}")]
        public DateTime? FechaInscripcionFinal { get; set; }

        public DateTime? FechaRegistro { get; set; }
        public DateTime? FechaActualizacion { get; set; }






    }
}
