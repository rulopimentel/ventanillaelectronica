﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace VentanillaElectronica.Models
{

    public enum EnumPrograma
    {
        CONTADURIA = 1,
        ADMINISTRACION = 2,
        DERECHO = 3,
        SOCIOLOGIA = 4,
        PEDAGOGIA = 5
    }

    public enum EnumRegion
    {
        XALAPA = 1,
        VERACRUZ = 2,
        ORIZABA = 3,
        POZA_RICA = 4,
        COATZACOALCOS = 5
    }
        
    public enum EnumBloqueTurno
    {
        I_MATUTINO = 1,
        I_VESPERTINO = 2,
        II_MATUTINO = 3,
        II_VESPERTINO = 4,
        III_MATUTINO = 5,
        III_VESPERTINO = 6,
        IV_MATUTINO = 7,
        IV_VESPERTINO = 8       
    }

    public enum EnumNumeroInscripcion
    {
        PRIMERA = 1,
        SEGUNDA = 2
    }

    public enum EnumTramitesTipo
    {
        [Description("Baja experiencia educativa")]
        Baja = 1,
        [Description("Alta experiencia educativa")]
        Alta = 2,
        [Description("Movilidad estudiantil")]
        MovilidadEstudiantil = 3,
        [Description("Última oportunidad")]
        UltimaOportunidad = 4
    }

    public enum EnumTramitesEstatus
    {
        Solicitado = 1,
        Aceptado = 2,
        Denegado = 3
    }

    public enum EnumTramitesUltimaOportunidadEstatus
    {
        Revisado = 4,
        DatosConsejoTecnico = 5

    }


    public enum EnumTramitesEstatusServe
    {
        Aceptado = 2,
        Denegado = 3
    }

    public enum EnumTramitesEstatusCheck
    {
        Aceptado = 2,
        Denegado = 3,
        Revisado = 4
    }

    public enum EnumTramitesEdit
    {
        Solicitado = 1,
        Denegado = 3
    }

    public enum EnumTramitesEditSolicitado
    {
        Solicitado = 1,
    }


}