﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace VentanillaElectronica.Models
{
    public class Usuario : GenericIdentity
    {

        public string Nombre { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public bool IsAlumno { get; set; }
        public bool Exist { get; set; }

        public Usuario(string userName, string password) : base(userName, "Basic")
        {

        }

        
    }
}