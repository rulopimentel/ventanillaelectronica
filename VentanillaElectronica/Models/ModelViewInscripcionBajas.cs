﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace VentanillaElectronica.Models
{

    public class ModelViewInscripcionBajas
    {
        
        public VentanillaPeriodo periodo { get; set; }

        public VentanillaAlumno alumno { get; set; }

        public VentanillaTramitesBajas tramiteBaja { get; set; }


    }

    public class ModelViewServeTramiteBaja
    {
        [Key]
        public int Id { get; set; }

        public int VentanillaTramiteId { get; set; }
        [ForeignKey("VentanillaTramiteId")]
        public VentanillaTramite Tramite { get; set; }


        public EnumTramitesEstatus Estatus { get; set; }

        [Display(Name = "Estatus")]
        [Required(ErrorMessage = "Estatus requerido")]
        public EnumTramitesEstatusServe EstatusServe { get; set; }

        public int? VentanillaAdministrativoId { get; set; }

        [ForeignKey("VentanillaAdministrativoId")]
        [Display(Name = "Atendido por")]

        public virtual VentanillaAdministrativo Administrativo { get; set; }

        [DisplayFormat(DataFormatString = "{0:d MMM yy HH:mm}")]
        [Display(Name = "Fecha de registro")]
        public DateTime? FechaRegistro { get; set; }

        [Required(ErrorMessage = "Observaciones requeridas")]
        [Display(Name = "Observaciones")]
        public string Observaciones { get; set; }

    }

}
