﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace VentanillaElectronica.Models
{

    public class VentanillaTramitesMovilidad
    {
        [Key, ForeignKey("Tramite")]
        public int Id { get; set; }

        public virtual VentanillaTramite Tramite { get; set; }

        [Required]
        public int VentanillaOfertaMovilidadId { get; set; }

        [ForeignKey("VentanillaOfertaMovilidadId")]
        public virtual VentanillaOfertaMovilidad OfertaMovilidad { get; set; }

        [Required]
        [Display(Name = "Formato de movilidad")]
        public string Url { get; set; }

       


    }
}