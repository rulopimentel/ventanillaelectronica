﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VentanillaElectronica.Models
{
    public class LogObject
    {
        public int StatusRespone { get; set; }
        public String Method { get; set; }
        public String OriginalString { get; set; }
        public String Username { get; set; }

        public LogObject()
        {

        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {{ USER: {3} }} ", StatusRespone, Method, OriginalString, Username);
        }
    }
}