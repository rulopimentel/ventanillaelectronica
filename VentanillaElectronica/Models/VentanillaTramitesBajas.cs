﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace VentanillaElectronica.Models
{

    public class VentanillaTramitesBajas
    {

        private string _ExperienciaEducativa;

        [Key, ForeignKey("Tramite")]

        public int Id { get; set; }

        [Required]      
        public virtual VentanillaTramite Tramite { get; set; }

        [Required]
        [Display(Name = "Bloque/turno")]
        public EnumBloqueTurno? BloqueTurno { get; set; }


        [RegularExpression(@"^\d{5}$", ErrorMessage = "Formato inválido en NRC")]
        [Required(ErrorMessage = "NRC es requerido")]
        [Display(Name = "NRC")]
        public string NRC { get; set; }

        [Required(ErrorMessage = "El nombre de la Experiencia Educativa es requerido")]
        [Display(Name = "Experiencia Educativa")]
        public string ExperienciaEducativa
        {
            get
            {
                return _ExperienciaEducativa;
            }
            set
            {
                _ExperienciaEducativa = value != null ? value.ToUpper() : null;
            }
        }      

    }
}
