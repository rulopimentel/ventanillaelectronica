﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VentanillaElectronica.Models
{

    public class VentanillaExperienciaEducativa
    {
        private string _Nombre;

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Nombre es requerido")]
        [Display(Name = "Nombre de la Experiencia Educativa")]
        public string Nombre
        {
            get
            {
                return _Nombre;
            }
            set
            {
                _Nombre = value != null ? value.ToUpper() : null;
            }
        }

            


    }
}
