﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VentanillaElectronica.Models
{

    public class VentanillaOfertaMovilidad
    {
        private string _Nombre;
        private string _NRC;

    
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Nombre es requerido")]
        [Display(Name = "Experiencia educativa")]
        public string Nombre
        {
            get
            {
                return _Nombre;
            }
            set
            {
                _Nombre = value != null ? value.ToUpper() : null;
            }
        }

        [Required(ErrorMessage = "NRC es requerido")]
        [Display(Name = "NRC (Número del Registro del Curso)")]
        public string NRC
        {
            get
            {
                return _NRC;
            }
            set
            {
                _NRC = value != null ? value.ToUpper() : null;
            }
        }

        [Required(ErrorMessage = "Cupo es requerido")]
        [Display(Name = "Cupo")]
        public short Cupo { get; set; }

        public DateTime? FechaRegistro { get; set; }

        public DateTime? FechaActualizacion { get; set; }

        [Required(ErrorMessage = "VentanillaInscripcionMovilidadId es requerido")]
        public int VentanillaInscripcionMovilidadId { get; set; }

        [ForeignKey("VentanillaInscripcionMovilidadId")]
        public virtual VentanillaInscripcionMovilidad InscripcionMovilidad { get; set; }


    }
}
