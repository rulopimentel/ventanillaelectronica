﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace VentanillaElectronica.Models
{

    public class ModelViewUltimaOportunidad
    {
        public ModelViewUltimaOportunidad()
        {
            RegionProgramas = new List<RegionPrograma>();
        }

        public List<RegionPrograma> RegionProgramas { get; set; }

        public VentanillaAlumno Alumno { get; set; }

        public VentanillaTramitesUltimaOportunidad TramiteUltimaOportunidad { get; set; }

    }

    public class ModelViewEditUltimaOportunidad
    {
        public ModelViewEditUltimaOportunidad()
        {
            RegionProgramas = new List<RegionPrograma>();
        }

        public List<RegionPrograma> RegionProgramas { get; set; }

        public VentanillaTramite Tramite { get; set; }

    }

    public class ModelViewServe1UltimaOportunidad
    {
        [Key]
        public int Id { get; set; }

        public int VentanillaTramiteId { get; set; }

        [ForeignKey("VentanillaTramiteId")]
        public virtual VentanillaTramite Tramite { get; set; }

        [Display(Name = "Estatus")]
        [Required(ErrorMessage = "Estatus requerido")]
        public EnumTramitesEstatusServe EstatusServe { get; set; }

        public int? VentanillaAdministrativoId { get; set; }

        [ForeignKey("VentanillaAdministrativoId")]
        [Display(Name = "Atendido por")]

        public virtual VentanillaAdministrativo Administrativo { get; set; }
        
        [Required(ErrorMessage = "Observaciones requeridas")]
        [Display(Name = "Observaciones")]
        public string Observaciones { get; set; }

      
    }

    public class ModelViewServeUltimaOportunidad
    {
        [Key]
        public int Id { get; set; }

        public int VentanillaTramiteId { get; set; }

        [ForeignKey("VentanillaTramiteId")]
        public virtual VentanillaTramite Tramite { get; set; }

        [Display(Name = "Estatus")]
        [Required(ErrorMessage = "Estatus requerido")]
        public EnumTramitesEstatusServe EstatusServe { get; set; }

        public int? VentanillaAdministrativoId { get; set; }

        [ForeignKey("VentanillaAdministrativoId")]
        [Display(Name = "Atendido por")]

        public virtual VentanillaAdministrativo Administrativo { get; set; }

        [Required(ErrorMessage = "Observaciones requeridas")]
        [Display(Name = "Observaciones")]
        public string Observaciones { get; set; }

        //[Required(ErrorMessage = "Oficio de aceptación requerido")]
        //[Display(Name = "Oficio de aceptación")]
        //[NotMapped]
        //public HttpPostedFileBase File { get; set; }
    }
    
    public class ModelViewCheckUltimaOportunidad
    {
        [Key]
        public int Id { get; set; }

        public int VentanillaTramiteId { get; set; }

        [ForeignKey("VentanillaTramiteId")]
        public virtual VentanillaTramite Tramite { get; set; }

        [Display(Name = "Estatus")]
        [Required(ErrorMessage = "Estatus requerido")]
        public EnumTramitesEstatusCheck EstatusServe { get; set; }

        public int? VentanillaAdministrativoId { get; set; }

        [ForeignKey("VentanillaAdministrativoId")]
        [Display(Name = "Atendido por")]

        public virtual VentanillaAdministrativo Administrativo { get; set; }

     

        [Required(ErrorMessage = "El Cardex es requerido")]
        [Display(Name = "Cardex")]
        [NotMapped]
        public HttpPostedFileBase File { get; set; }
    }

    public class ModelViewResolucionUltimaOportunidad
    {
        [Key]
        public int Id { get; set; }

        public int VentanillaTramiteId { get; set; }

        [ForeignKey("VentanillaTramiteId")]
        public virtual VentanillaTramite Tramite { get; set; }

        [Required(ErrorMessage = "Nombre del docente requerido")]
        [Display(Name = "Docente 1")]
        public string DocenteAplicacion1 { get; set; }

        [Required(ErrorMessage = "Nombre del docente requerido")]
        [Display(Name = "Docente 2")]
        public string DocenteAplicacion2 { get; set; }

        [Required(ErrorMessage = "Fecha requerida")]
        [Display(Name = "Fecha Consejo Técnico")]
        public string FechaConsejoTecnico { get; set; }

        [Required(ErrorMessage = "Fecha requerida")]
        [Display(Name = "Fecha del Examen")]
        public string FechaExamen { get; set; }

    }





}
