﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VentanillaElectronica.Models
{

    public class VentanillaAdministrativo
    {
        private string _Nombre;
        private string _UserName;
        private string _Email;

        public VentanillaAdministrativo()
        {
            AdministrativosPrograma = new List<VentanillaAdministrativosPrograma>();
            Permisos = new List<VentanillaPermisos>();


        }

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Nombre es requerido")]
        [Display(Name = "Nombre completo del usuario")]
        public string Nombre
        {
            get
            {
                return _Nombre;
            }
            set
            {
                _Nombre = value != null ? value.ToUpper() : null;
            }
        }

        public string Telefono { get; set; }


        [Required(ErrorMessage = "UserName es requerido")]
        [Display(Name = "UserName del usuario")]
        public string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {
                _UserName = value != null ? value.ToLower() : null;
            }
        }

        [Required(ErrorMessage = "Email es requerido")]
        public string Email
        {
            get
            {
                return _Email;
            }
            set
            {
                _Email = value != null ? value.ToLower() : null;
            }
        }

        public DateTime? FechaRegistro { get; set; }

        public DateTime? FechaActualizacion { get; set; }

        public virtual List<VentanillaAdministrativosPrograma> AdministrativosPrograma { get; set; }
        public virtual List<VentanillaPermisos> Permisos { get; set; }

    }
}
