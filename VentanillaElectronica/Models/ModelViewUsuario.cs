﻿using System.ComponentModel.DataAnnotations;

namespace VentanillaElectronica.Models
{

    public class ModelViewLogin
    {

        [Required(ErrorMessage = "Nombre de usuario o matrícula requerido")]
        [Display(Name = "Usuario")]
        public string Username { get; set; }


        [Required(ErrorMessage = "Contraseña es requerida")]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }
      
    }
}
