﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace VentanillaElectronica.Models
{

    public class ModelViewInscripcionMovilidad
    {
        public ModelViewInscripcionMovilidad()
        {
            inscripcionMovilidad = new List<VentanillaInscripcionMovilidad>();
        }
        public VentanillaPeriodo periodo { get; set; }

        public List<VentanillaInscripcionMovilidad> inscripcionMovilidad { get; set; }

        public VentanillaAlumno alumno { get; set; }

    }

    public class ModelViewTramitesMovilidad
    {

        public VentanillaPeriodo periodo { get; set; }

        public VentanillaTramite tramite { get; set; }

        public VentanillaAlumno alumno { get; set; }

        public VentanillaInscripcionMovilidad inscripcionMovilidad { get; set; }

        [Required(ErrorMessage = "Formato de movilidad requerido")]
        [Display(Name = "Formato de movilidad")]
        [NotMapped]
        public HttpPostedFileBase File { get; set; }

    }

    public class ModelViewTramitesEscenarios
    {
        public EnumTramitesEscenarios Estatus { get; set; }
        public string mensaje { get; set; }
        public int? Id { get; set; }
    }

    public class ModelViewServeTramitesMovilidad
    {
         [Key]
        public int Id { get; set; }

        public int VentanillaOfertaMovilidadId { get; set; }

        [ForeignKey("VentanillaOfertaMovilidadId")]
        public virtual VentanillaOfertaMovilidad OfertaMovilidad { get; set; }

        public int VentanillaAlumnoId { get; set; }

        [ForeignKey("VentanillaAlumnoId")]
        public virtual VentanillaAlumno Alumno { get; set; }
        public EnumTramitesEstatus Estatus { get; set; }

        [Display(Name = "Estatus")]
        [Required(ErrorMessage = "Estatus requerido")]
        public EnumTramitesEstatusServe EstatusServe { get; set; }

        public int? VentanillaAdministrativoId { get; set; }

        [ForeignKey("VentanillaAdministrativoId")]
        [Display(Name = "Atendido por")]

        public virtual VentanillaAdministrativo Administrativo { get; set; }

        [DisplayFormat(DataFormatString = "{0:d MMM yy HH:mm}")]
        [Display(Name = "Fecha de registro")]
        public DateTime? FechaRegistro { get; set; }

        [Required(ErrorMessage = "Observaciones requeridas")]
        [Display(Name = "Observaciones")]
        public string Observaciones { get; set; }

        [Display(Name = "Formato de movilidad")]
        public string Url { get; set; }

    }

}
