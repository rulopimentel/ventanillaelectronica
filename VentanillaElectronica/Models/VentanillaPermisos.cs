﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VentanillaElectronica.Models
{

    public class VentanillaPermisos
    {

        [Key]
        public int Id { get; set; }

        [Required]
        public int RegionProgramaId { get; set; }

        [ForeignKey("RegionProgramaId")]
        [Display(Name = "Programa")]
        public virtual RegionPrograma RegionPrograma { get; set; }

        [Required]
        public int VentanillaAdministrativoId { get; set; }
        [ForeignKey("VentanillaAdministrativoId")]
        [Display(Name = "Atendido por")]
        public virtual VentanillaAdministrativo Administrativo { get; set; }

        [Required]
        public int VentanillaModuloId { get; set; }
        [ForeignKey("VentanillaModuloId")]
        [Display(Name = "Módulo")]
        public virtual VentanillaModulo Modulo { get; set; }

        public DateTime FechaRegistro { get; set; }
        
    }
}
