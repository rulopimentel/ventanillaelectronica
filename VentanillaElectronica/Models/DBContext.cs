﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;



namespace VentanillaElectronica.Models
{
    public class VentanillaDBContext : DbContext
    {
        public VentanillaDBContext() : base("name=DBConnection")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();            
        }

        public virtual DbSet<VentanillaPeriodo> Periodos { get; set; }
        public virtual DbSet<VentanillaInscripcionMovilidad> InscripcionMovilidad { get; set; }
        public virtual DbSet<VentanillaInscripcionBajas> InscripcionBajas { get; set; }
        public virtual DbSet<VentanillaInscripcionAltas> InscripcionAltas { get; set; }
        public virtual DbSet<VentanillaOfertaMovilidad> OfertaMovilidad { get; set; }
        public virtual DbSet<VentanillaOfertaAltas> OfertaAltas { get; set; }
        public virtual DbSet<VentanillaAdministrativo> Administrativos { get; set; }
        public virtual DbSet<VentanillaAcademico> Academicos { get; set; }
        public virtual DbSet<VentanillaModulo> Modulos { get; set; }
        public virtual DbSet<VentanillaPermisos> Permisos { get; set; }
        public virtual DbSet<VentanillaExperienciaEducativa> ExperienciasEducativas { get; set; }
        public virtual DbSet<VentanillaAdministrativosPrograma> AdministrativosPrograma { get; set; }
        public virtual DbSet<VentanillaAlumno> Alumnos { get; set; }
        public virtual DbSet<VentanillaTramitesMovilidad> TramitesMovilidad { get; set; }
        public virtual DbSet<VentanillaTramitesUltimaOportunidad> TramitesUltimaOportunidad { get; set; }
        public virtual DbSet<VentanillaTramitesBajas> TramitesBajas { get; set; }
        public virtual DbSet<VentanillaTramitesAltas> TramitesAltas { get; set; }
        public virtual DbSet<VentanillaTramite> Tramites { get; set; }
        public virtual DbSet<RegionPrograma> RegionPrograma { get; set; }


        public static VentanillaDBContext Create()
        {
            return new VentanillaDBContext();
        }
    }
}


