﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace VentanillaElectronica.Models
{

    public class RegionPrograma
    {      

        [Key]
        public int Id { get; set; }
     
        public virtual EnumPrograma Programa { get; set; }
        public virtual EnumRegion Region { get; set; }

        [NotMapped]
        public string Value
        {
            get {
                return Enum.GetName(typeof(EnumRegion), Region) + " - " + Enum.GetName(typeof(EnumPrograma), Programa);
            }
        }

    }
}
