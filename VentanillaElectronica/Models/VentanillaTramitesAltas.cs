﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace VentanillaElectronica.Models
{

    public class VentanillaTramitesAltas
    {
        [Key, ForeignKey("Tramite")]
        public int Id { get; set; }

      
        public virtual VentanillaTramite Tramite { get; set; }

        [Required]
        public int VentanillaOfertaAltasId { get; set; }

        [ForeignKey("VentanillaOfertaAltasId")]
        public virtual VentanillaOfertaAltas OfertaAlta { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Número de inscripción")]
        public EnumNumeroInscripcion NumeroInscripcion { get; set; }

    }
}
