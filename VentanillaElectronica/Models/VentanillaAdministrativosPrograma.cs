﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VentanillaElectronica.Models
{

    public class VentanillaAdministrativosPrograma
    {      

        [Key]
        public int Id { get; set; }
      
        public EnumPrograma Programa { get; set; }

        public int VentanillaAdministrativoId { get; set; }
        [ForeignKey("VentanillaAdministrativoId")]
        public virtual VentanillaAdministrativo Administrativo { get; set; }

        public DateTime? FechaRegistro { get; set; }

        public DateTime? FechaActualizacion { get; set; }
        
    }
}
