﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VentanillaElectronica.Models
{

    public class VentanillaModulo
    {

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Descripción es requerido")]
        [Display(Name = "Descripción del módulo")]
        public string Descripcion { get; set; }

    
        [Required(ErrorMessage = "Clave es requerido")]
        [Display(Name = "Clave del módulo")]
        public string Clave { get; set; }
        

        public DateTime FechaRegistro { get; set; }
        
    }
}
