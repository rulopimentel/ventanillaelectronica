﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace VentanillaElectronica.Models
{

    public class VentanillaTramite
    {      

        [Key]
        public int Id { get; set; }

        public int VentanillaAlumnoId { get; set; }

        [ForeignKey("VentanillaAlumnoId")]
        public virtual VentanillaAlumno Alumno { get; set; }
      
        public virtual EnumPrograma Programa { get; set; }

        public EnumTramitesEstatus Estatus { get; set; }

        public EnumTramitesTipo Tipo { get; set; }


        public int? VentanillaAdministrativoId { get; set; }

        [ForeignKey("VentanillaAdministrativoId")]
        [Display(Name = "Atendido por")]
        public virtual VentanillaAdministrativo Administrativo { get; set; }

        public int? VentanillaPeriodoId { get; set; }

        [Display(Name = "Periodo")]
        [ForeignKey("VentanillaPeriodoId")]
        public virtual VentanillaPeriodo Periodo { get; set; }

        [DisplayFormat(DataFormatString = "{0:d MMM yy HH:mm}")]

        [Display(Name = "Fecha de registro")]

        public DateTime? FechaRegistro { get; set; }

        [DisplayFormat(DataFormatString = "{0:d MMM yy HH:mm}")]

        public DateTime? FechaActualizacion { get; set; }

        [DisplayFormat(DataFormatString = "{0:d MMM yy HH:mm}")]

        [Display(Name = "Fecha de atención")]
        public DateTime? FechaAtencion { get; set; }

        [Display(Name = "Observaciones")]
        public string Observaciones { get; set; }

        public virtual VentanillaTramitesBajas TramiteBaja { get; set; }

        public virtual VentanillaTramitesMovilidad TramiteMovilidad { get; set; }

        public virtual VentanillaTramitesAltas TramiteAlta { get; set; }

        public virtual VentanillaTramitesUltimaOportunidad TramiteUltimaOportunidad { get; set; }

    }
}
