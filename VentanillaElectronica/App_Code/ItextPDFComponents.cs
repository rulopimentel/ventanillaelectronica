﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Globalization;
using System.IO;
using System.Web;
using VentanillaElectronica.Models;

namespace VentanillaElectronica.App_Code
{


    class FormatEvents : PdfPageEventHelper
    {
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            string imageURL = HttpContext.Current.Server.MapPath("~/Content/images/LogotipoUV.jpg");
            Image logotipo = Image.GetInstance(imageURL);

            PdfPTable table = new PdfPTable(1);
            table.DefaultCell.Border = Rectangle.NO_BORDER;
            table.TotalWidth = document.PageSize.Width - document.LeftMargin - document.RightMargin;

            PdfPTable table2 = new PdfPTable(2);
            table2.DefaultCell.Border = Rectangle.NO_BORDER;

            //logo
            PdfPCell cell2 = new PdfPCell(logotipo);
            cell2.Colspan = 2;
            cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell2.Border = Rectangle.NO_BORDER;

            table2.AddCell(cell2);

           

            PdfPCell cell = new PdfPCell(table2);
            cell.Border = Rectangle.NO_BORDER;

            table.AddCell(cell);

            table.WriteSelectedRows(0, -1, document.LeftMargin, document.Top + ((document.TopMargin + table.TotalHeight) / 2), writer.DirectContent);
        }

    }

    public class ItextPDFComponents
    {




        public static byte[] createPDF(VentanillaTramite Tramite)
        {


            try
            {
                string normal_font = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
                    "verdana.ttf"
                    );
                FontFactory.Register(normal_font);
                BaseFont baseFont_normal = BaseFont.CreateFont(
                    normal_font,
                    BaseFont.IDENTITY_H,
                    BaseFont.NOT_EMBEDDED
                    );
                Font font_main = new Font(baseFont_normal, 10, Font.NORMAL);

                // bold font
                string bold_font = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
                    "verdanab.ttf"
                    );
                FontFactory.Register(bold_font);
                BaseFont baseFont_bold = BaseFont.CreateFont(
                    bold_font,
                    BaseFont.IDENTITY_H,
                    BaseFont.NOT_EMBEDDED
                    );
                Font font_bold = new Font(baseFont_bold, 10, Font.NORMAL);

                Font font_bold_header = new Font(baseFont_bold, 12, Font.NORMAL);

                // italic font
                string italic_font = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
                    "verdanai.ttf"
                    );
                FontFactory.Register(italic_font);
                BaseFont baseFont_italic = BaseFont.CreateFont(
                    italic_font,
                    BaseFont.IDENTITY_H,
                    BaseFont.NOT_EMBEDDED
                    );
                Font font_italic = new Font(baseFont_italic, 8, Font.NORMAL);

                // header font
                string header_font = Path.Combine(
                   Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
                   "verdanab.ttf"
                   );
                FontFactory.Register(header_font);
                BaseFont baseFont_header = BaseFont.CreateFont(
                    bold_font,
                    BaseFont.IDENTITY_H,
                    BaseFont.NOT_EMBEDDED
                    );
                Font font_header = new Font(baseFont_header, 16, Font.NORMAL);


                FormatEvents events = new FormatEvents();


                using (MemoryStream memoryStream = new MemoryStream())
                using (Document doc = new Document(PageSize.LETTER, 80, 80, 170, 70))
                using (PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream))
                {
                    writer.ViewerPreferences = PdfWriter.HideMenubar;
                    writer.PageEvent = events;

                    doc.Open();

                    // --- meta information

                    doc.AddTitle("Formato de solicitud para examen de Última Oportunidad");
                    doc.AddSubject("Match results");
                    doc.AddKeywords("Examen de Última Oportunidad, SEA, UV");
                    doc.AddAuthor("rcolorado@uv.mx");
                    doc.AddCreator("Sistema de de Enseñanza Abierta");

                    // --- document contents
                  
                    doc.Add(new Paragraph("H. CONSEJO TÉCNICO", font_bold_header));
                    doc.Add(new Paragraph(string.Format("PROGRAMA EDUCATIVO DE {0}", Tramite.TramiteUltimaOportunidad.RegionPrograma.Value), font_bold_header));
                    doc.Add(new Paragraph("PRESENTE", font_bold_header));

                    doc.Add(Chunk.NEWLINE);
                    doc.Add(Chunk.NEWLINE);

                    doc.Add(new Paragraph(string.Format("El que suscribe {0}, con número de matrícula {1}, se dirige a usted para solicitar autorización, con fundamento en el Artículo 70 del Estatuto de Alumnos 2008, para presentar examen con carácter de última oportunidad de la experiencia educativa {2}, cuyo titular es {3}.", Tramite.Alumno.Nombre, Tramite.Alumno.UserName, Tramite.TramiteUltimaOportunidad.ExperienciaEducativa, Tramite.TramiteUltimaOportunidad.Docente)));
                    doc.Add(Chunk.NEWLINE);
                    doc.Add(new Paragraph(string.Format("Así mismo, declaro que {0} existe inconveniente en presentar el examen con el titular de la Experiencia Educativa.", Tramite.TramiteUltimaOportunidad.PresentaInconveniente == true ? "SÍ" : "NO")));
                    doc.Add(Chunk.NEWLINE);
                    doc.Add(new Paragraph("Sin otro particular envío un cordial saludo."));
                    doc.Add(Chunk.NEWLINE);
                    doc.Add(Chunk.NEWLINE);
                    Paragraph p = new Paragraph("ATENTAMENTE");
                    p.Alignment = Element.ALIGN_CENTER;
                    doc.Add(p);

                    DateTime fechaSolicitud = (DateTime)Tramite.FechaRegistro;

                    p = new Paragraph(string.Format("Fecha de solicitud {0}", fechaSolicitud.ToString("dd MMM, yyyy", CultureInfo.CreateSpecificCulture("es-MX"))));
                    p.Alignment = Element.ALIGN_CENTER;
                    doc.Add(p);
                    doc.Add(Chunk.NEWLINE);
                    doc.Add(Chunk.NEWLINE);
                    doc.Add(Chunk.NEWLINE);
                    p = new Paragraph("Firma del alumno");
                    p.Alignment = Element.ALIGN_CENTER;
                    doc.Add(p);
                    p = new Paragraph(string.Format("{0}", Tramite.Alumno.Nombre));
                    p.Alignment = Element.ALIGN_CENTER;
                    doc.Add(p);
                    p = new Paragraph(string.Format("{0}", Tramite.Alumno.Email));
                    p.Alignment = Element.ALIGN_CENTER;
                    doc.Add(p);
                    p = new Paragraph(string.Format("{0}", Tramite.Alumno.EmailAlterno));
                    p.Alignment = Element.ALIGN_CENTER;
                    doc.Add(p);
                    doc.Add(Chunk.NEWLINE);
                    doc.Add(Chunk.NEWLINE);
                    DateTime FechaImpresion = DateTime.Now;

                    p = new Paragraph(string.Format("Nota: La NO acreditación de este examen hará que el alumno cause baja definitiva del programa educativo. Este documento es generado a través de la Ventanilla Electrónica del Sistema de Enseñanza Abierta, con fecha del {0}.", FechaImpresion.ToString("dd MMM, yyyy HH:mm", CultureInfo.CreateSpecificCulture("es-MX"))), font_italic);
                    p.Alignment = Element.ALIGN_RIGHT;
                    doc.Add(p);


                    doc.Close();
                    writer.Close();



                    return memoryStream.ToArray();



                }

            }
            catch (Exception ex)
            {
                return null;
            }

        }

        static Paragraph CustomParagraph(string text, Font font, int alignment)
        {

            Paragraph p = new Paragraph(text, font);
            p.Alignment = alignment;
            return p;
        }

    }
}
