﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using VentanillaElectronica.Models;

namespace VentanillaElectronica.App_Code
{
    public class CustomMembershipProvider : MembershipProvider
    {
        private VentanillaDBContext db = new VentanillaDBContext();
        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool EnablePasswordReset
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override bool EnablePasswordRetrieval
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override int MinRequiredPasswordLength
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override int PasswordAttemptWindow
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override string PasswordStrengthRegularExpression
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override bool RequiresUniqueEmail
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }


        public Usuario ValidaUsuario(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password)) return null;
            username = username.ToUpper();
            if (Regex.Match(username, @"^[Z]?[S]\d{8,10}$", RegexOptions.IgnoreCase).Success)
            {
                if (username[0] == 'S') username = "Z" + username;
                Usuario usuario = null;
                LdapAutenticacion ldapAutenticacion = new LdapAutenticacion("ESTUDIANTES");
                usuario = ldapAutenticacion.IsAuthenticatedV2(username, password);
                if(usuario != null)
                {
                    if (db.Alumnos.Where(x => x.UserName == username).FirstOrDefault() != null) usuario.Exist = true;
                    usuario.IsAlumno = true;
                }
                return usuario;
               
            }
            else if (Regex.Match(username, @"^\w{5,}$", RegexOptions.IgnoreCase).Success)
            {
                Usuario usuario = null;
                LdapAutenticacion ldapAutenticacion = null;
                string[] regiones = new string[] { "XALAPA", "POZARICA", "COATZACOALCOS", "ORIZABA", "VERACRUZ" };
                for (int i = 0; i < regiones.Length && usuario == null; i++)
                {
                    ldapAutenticacion = new LdapAutenticacion(regiones[i]);
                    usuario = ldapAutenticacion.IsAuthenticatedV2(username, password);
                }

                if (usuario == null) return null;
                else
                {
                    //ROLES de usuario y alumno
                    if (db.Administrativos.Where(x => x.UserName == username).FirstOrDefault() == null) return null;
                    usuario.IsAlumno = false;
                    usuario.Exist = true;
                    return usuario;
                }
            }
            return null;
        }

        public override bool ValidateUser(string username, string password)
        {
            throw new NotImplementedException();

        }
    }
}