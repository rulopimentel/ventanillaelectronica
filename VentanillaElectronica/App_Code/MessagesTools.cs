﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Web;
using VentanillaElectronica.Models;

namespace VentanillaElectronica.App_Code
{


    public class MessagesTools
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void SendMailBienvenida(VentanillaTramite tramite)
        {
            var path = HttpContext.Current.Server.MapPath("~/Content/Templates/Bienvenida.html");
            try
            {
                string _body = File.ReadAllText(path);               
                _body = _body.Replace("[#NombreCompleto#]", tramite.Alumno.Nombre);
                SendAsync(tramite, _body, "Bienvenido a la Ventanilla Electrónica - Sistema de Enseñanza Abierta - Universidad Veracruzana");
            }
            catch
            {

            }
        }

        public void SendMailTramiteBaja(VentanillaTramite tramite)
        {
            var path = HttpContext.Current.Server.MapPath("~/Content/Templates/TramiteBaja.html");
            try
            {
                string _body = File.ReadAllText(path);
                _body = _body.Replace("[#NombreCompleto#]", tramite.Alumno.Nombre);
                _body = _body.Replace("[#Periodo#]", tramite.Periodo.Nombre);
                _body = _body.Replace("[#Programa#]", tramite.Programa.Description());
                _body = _body.Replace("[#ExperienciaEducativa#]", "NRC(" + tramite.TramiteBaja.NRC + ") " + tramite.TramiteBaja.ExperienciaEducativa);
                _body = _body.Replace("[#Bloque#]", tramite.TramiteBaja.BloqueTurno.Description());

                _body = _body.Replace("[#Fecha#]", tramite.FechaRegistro.ToString());
                _body = _body.Replace("[#Estatus#]", tramite.Estatus.Description());     

                SendAsync(tramite, _body, "Ventanilla electrónica SEA - Solicitud de trámite de baja de experiencia educativa");
            }
            catch
            {

            }
        }

        public void SendMailTramiteAlta(VentanillaTramite tramite)
        {
            var path = HttpContext.Current.Server.MapPath("~/Content/Templates/TramiteAlta.html");
            try
            {
                string _body = File.ReadAllText(path);
                _body = _body.Replace("[#NombreCompleto#]", tramite.Alumno.Nombre);
                _body = _body.Replace("[#Periodo#]", tramite.Periodo.Nombre);
                _body = _body.Replace("[#Programa#]", tramite.Programa.Description());
                _body = _body.Replace("[#ExperienciaEducativa#]", "NRC(" + tramite.TramiteAlta.OfertaAlta.NRC + ") " + tramite.TramiteAlta.OfertaAlta.Nombre);
                _body = _body.Replace("[#NumeroInscripcion#]", tramite.TramiteAlta.NumeroInscripcion.Description());

                _body = _body.Replace("[#Fecha#]", tramite.FechaRegistro.ToString());
                _body = _body.Replace("[#Estatus#]", tramite.Estatus.Description());

                SendAsync(tramite, _body, "Ventanilla electrónica SEA - Solicitud de trámite de alta de experiencia educativa");
            }
            catch
            {

            }
        }



        public void SendMailTramitUltimaOportunidad(VentanillaTramite tramite)
        {
            var path = HttpContext.Current.Server.MapPath("~/Content/Templates/TramiteUltimaOportunidad.html");
            try
            {
                string _body = File.ReadAllText(path);

                _body = _body.Replace("[#NombreCompleto#]", tramite.Alumno.Nombre);
                _body = _body.Replace("[#Programa#]", tramite.TramiteUltimaOportunidad.RegionPrograma.Value);
                _body = _body.Replace("[#ExperienciaEducativa#]",  tramite.TramiteUltimaOportunidad.ExperienciaEducativa);
                _body = _body.Replace("[#Titular#]", tramite.TramiteUltimaOportunidad.Docente);
                if(tramite.TramiteUltimaOportunidad.PresentaInconveniente)
                _body = _body.Replace("[#Inconvenientes#]", "Tengo inconveniente en presentar el examen con el titular de la experiencia educativa.");
                else
                    _body = _body.Replace("[#Inconvenientes#]", "Ninguno");


                _body = _body.Replace("[#Fecha#]", tramite.FechaRegistro.ToString());
                _body = _body.Replace("[#Estatus#]", tramite.Estatus.Description());

                SendAsync(tramite, _body, "Ventanilla electrónica SEA - Solicitud de examen de última oportunidad.");
            }
            catch
            {

            }
        }



        public void SendMailTramitUltimaOportunidadRevisado(VentanillaTramite tramite)
        {
            var path = HttpContext.Current.Server.MapPath("~/Content/Templates/TramiteUltimaOportunidadRevisado.html");
            try
            {
                string _body = File.ReadAllText(path);

                _body = _body.Replace("[#NombreCompleto#]", tramite.Alumno.Nombre);
                _body = _body.Replace("[#Programa#]", tramite.TramiteUltimaOportunidad.RegionPrograma.Value);
                _body = _body.Replace("[#ExperienciaEducativa#]", tramite.TramiteUltimaOportunidad.ExperienciaEducativa);
                _body = _body.Replace("[#Titular#]", tramite.TramiteUltimaOportunidad.Docente);
                if (tramite.TramiteUltimaOportunidad.PresentaInconveniente)
                    _body = _body.Replace("[#Inconvenientes#]", "Tengo inconveniente en presentar el examen con el titular de la experiencia educativa.");
                else
                    _body = _body.Replace("[#Inconvenientes#]", "Ninguno");


                _body = _body.Replace("[#Fecha#]", tramite.FechaRegistro.ToString());
                _body = _body.Replace("[#Estatus#]", tramite.Estatus.Description());
                _body = _body.Replace("[#AdministrativoRevision#]", tramite.TramiteUltimaOportunidad.AdministrativoRevision.Nombre);
                _body = _body.Replace("[#FechaRevision#]", tramite.TramiteUltimaOportunidad.FechaRevisado.ToString());



                SendAsync(tramite, _body, "Ventanilla electrónica SEA - Solicitud de examen de última oportunidad.");
            }
            catch
            {

            }
        }

        public void SendMailTramitUltimaOportunidadPreDenegado(VentanillaTramite tramite)
        {
            var path = HttpContext.Current.Server.MapPath("~/Content/Templates/TramiteUltimaOportunidadPreDenegado.html");
            try
            {
                string _body = File.ReadAllText(path);

                _body = _body.Replace("[#NombreCompleto#]", tramite.Alumno.Nombre);
                _body = _body.Replace("[#Programa#]", tramite.TramiteUltimaOportunidad.RegionPrograma.Value);
                _body = _body.Replace("[#ExperienciaEducativa#]", tramite.TramiteUltimaOportunidad.ExperienciaEducativa);
                _body = _body.Replace("[#Titular#]", tramite.TramiteUltimaOportunidad.Docente);
                if (tramite.TramiteUltimaOportunidad.PresentaInconveniente)
                    _body = _body.Replace("[#Inconvenientes#]", "Tengo inconveniente en presentar el examen con el titular de la experiencia educativa.");
                else
                    _body = _body.Replace("[#Inconvenientes#]", "Ninguno");


                _body = _body.Replace("[#Fecha#]", tramite.FechaRegistro.ToString());
                _body = _body.Replace("[#Estatus#]", tramite.Estatus.Description());
                _body = _body.Replace("[#AdministrativoAtiende#]", tramite.Administrativo.Nombre);
                _body = _body.Replace("[#FechaAtencion#]", tramite.FechaAtencion.ToString());
                _body = _body.Replace("[#Observaciones#]", tramite.Observaciones);



                SendAsync(tramite, _body, "Ventanilla electrónica SEA - Solicitud de examen de última oportunidad.");
            }
            catch
            {

            }
        }

        public void SendMailTramitUltimaOportunidadAceptado(VentanillaTramite tramite)
        {
            var path = HttpContext.Current.Server.MapPath("~/Content/Templates/TramiteUltimaOportunidadAceptado.html");
            try
            {
                string _body = File.ReadAllText(path);

                _body = _body.Replace("[#NombreCompleto#]", tramite.Alumno.Nombre);
                _body = _body.Replace("[#Programa#]", tramite.TramiteUltimaOportunidad.RegionPrograma.Value);
                _body = _body.Replace("[#ExperienciaEducativa#]", tramite.TramiteUltimaOportunidad.ExperienciaEducativa);
                _body = _body.Replace("[#Titular#]", tramite.TramiteUltimaOportunidad.Docente);
                if (tramite.TramiteUltimaOportunidad.PresentaInconveniente)
                    _body = _body.Replace("[#Inconvenientes#]", "Tengo inconveniente en presentar el examen con el titular de la experiencia educativa.");
                else
                    _body = _body.Replace("[#Inconvenientes#]", "Ninguno");


                _body = _body.Replace("[#Fecha#]", tramite.FechaRegistro.ToString());
                _body = _body.Replace("[#Estatus#]", tramite.Estatus.Description());
                _body = _body.Replace("[#AdministrativoAtiende#]", tramite.Administrativo.Nombre);
                _body = _body.Replace("[#FechaAtencion#]", tramite.FechaAtencion.ToString());
                _body = _body.Replace("[#Observaciones#]", tramite.Observaciones);
                _body = _body.Replace("[#URL#]", tramite.TramiteUltimaOportunidad.Url);



                SendAsync(tramite, _body, "Ventanilla electrónica SEA - Solicitud de examen de última oportunidad.");
            }
            catch
            {

            }
        }


        public void SendMailTramiteAltaServe(VentanillaTramite tramite)
        {
            var path = HttpContext.Current.Server.MapPath("~/Content/Templates/TramiteAltaServe.html");
            try
            {
                string _body = File.ReadAllText(path);
                _body = _body.Replace("[#NombreCompleto#]", tramite.Alumno.Nombre);
                _body = _body.Replace("[#Periodo#]", tramite.Periodo.Nombre);
                _body = _body.Replace("[#Programa#]", tramite.Programa.Description());
                _body = _body.Replace("[#ExperienciaEducativa#]", "NRC(" + tramite.TramiteAlta.OfertaAlta.NRC + ") " + tramite.TramiteAlta.OfertaAlta.Nombre);
                _body = _body.Replace("[#NumeroInscripcion#]", tramite.TramiteAlta.NumeroInscripcion.Description());
                _body = _body.Replace("[#Fecha#]", tramite.FechaRegistro.ToString());

                _body = _body.Replace("[#FechaServe#]", tramite.FechaAtencion.ToString());
                _body = _body.Replace("[#NombreServe#]", tramite.Administrativo.Nombre);
                _body = _body.Replace("[#EmailServe#]", tramite.Administrativo.Email);
                _body = _body.Replace("[#TelefonoServe#]", tramite.Administrativo.Telefono);
                _body = _body.Replace("[#ObservacionesServe#]", tramite.Observaciones);
                _body = _body.Replace("[#EstatusServe#]", tramite.Estatus.Description());

                SendAsync(tramite, _body, "Ventanilla electrónica SEA - Se ha atendido un trámite de alta de experiencia educativa");
            }
            catch
            {

            }
        }

        public void SendMailTramiteBajaServe(VentanillaTramite tramite)
        {
            var path = HttpContext.Current.Server.MapPath("~/Content/Templates/TramiteBajaServe.html");
            try
            {
                string _body = File.ReadAllText(path);
                _body = _body.Replace("[#NombreCompleto#]", tramite.Alumno.Nombre);
                _body = _body.Replace("[#Periodo#]", tramite.Periodo.Nombre);
                _body = _body.Replace("[#Programa#]", tramite.Programa.Description());
                _body = _body.Replace("[#ExperienciaEducativa#]", "NRC(" + tramite.TramiteBaja.NRC + ") " + tramite.TramiteBaja.ExperienciaEducativa);
                _body = _body.Replace("[#Bloque#]", tramite.TramiteBaja.BloqueTurno.Description());

                _body = _body.Replace("[#Fecha#]", tramite.FechaRegistro.ToString());

                _body = _body.Replace("[#FechaServe#]", tramite.FechaAtencion.ToString());
                _body = _body.Replace("[#NombreServe#]", tramite.Administrativo.Nombre);
                _body = _body.Replace("[#EmailServe#]", tramite.Administrativo.Email);
                _body = _body.Replace("[#TelefonoServe#]", tramite.Administrativo.Telefono);
                _body = _body.Replace("[#ObservacionesServe#]", tramite.Observaciones);
                _body = _body.Replace("[#EstatusServe#]", tramite.Estatus.Description());

                SendAsync(tramite, _body, "Ventanilla electrónica SEA - Se ha atendido un trámite de baja de experiencia educativa");
            }
            catch
            {

            }
        }


        public void SendMailTramitemovilidadServe(VentanillaTramite tramite)
        {
            var path = HttpContext.Current.Server.MapPath("~/Content/Templates/TramiteMovilidadServe.html");
            try
            {
                string _body = File.ReadAllText(path);
                _body = _body.Replace("[#NombreCompleto#]", tramite.Alumno.Nombre);
                _body = _body.Replace("[#Periodo#]", tramite.Periodo.Nombre);
                _body = _body.Replace("[#Programa#]", tramite.Programa.Description());
                _body = _body.Replace("[#ExperienciaEducativa#]", "NRC(" + tramite.TramiteMovilidad.OfertaMovilidad.NRC + ") " + tramite.TramiteMovilidad.OfertaMovilidad.Nombre);
                _body = _body.Replace("[#URL#]", tramite.TramiteMovilidad.Url);

                _body = _body.Replace("[#Fecha#]", tramite.FechaRegistro.ToString());

                _body = _body.Replace("[#FechaServe#]", tramite.FechaAtencion.ToString());
                _body = _body.Replace("[#NombreServe#]", tramite.Administrativo.Nombre);
                _body = _body.Replace("[#EmailServe#]", tramite.Administrativo.Email);
                _body = _body.Replace("[#TelefonoServe#]", tramite.Administrativo.Telefono);
                _body = _body.Replace("[#ObservacionesServe#]", tramite.Observaciones);
                _body = _body.Replace("[#EstatusServe#]", tramite.Estatus.Description());

                SendAsync(tramite, _body, "Ventanilla electrónica SEA - Se ha atendido un trámite de movilidad estudiantil");
            }
            catch
            {

            }
        }


        public void SendMailTramitemovilidad(VentanillaTramite tramite)
        {
            var path = HttpContext.Current.Server.MapPath("~/Content/Templates/TramiteMovilidad.html");
            try
            {
                string _body = File.ReadAllText(path);
                _body = _body.Replace("[#NombreCompleto#]", tramite.Alumno.Nombre);
                _body = _body.Replace("[#Periodo#]", tramite.Periodo.Nombre);
                _body = _body.Replace("[#Programa#]", tramite.Programa.Description());
                _body = _body.Replace("[#ExperienciaEducativa#]", "NRC(" + tramite.TramiteMovilidad.OfertaMovilidad.NRC + ") " + tramite.TramiteMovilidad.OfertaMovilidad.Nombre);
                _body = _body.Replace("[#URL#]",  tramite.TramiteMovilidad.Url);

                _body = _body.Replace("[#Fecha#]", tramite.FechaRegistro.ToString());
                _body = _body.Replace("[#Estatus#]", tramite.Estatus.Description());

                SendAsync(tramite, _body, "Ventanilla electrónica SEA - Solicitud de trámite de movilidad estudiantil");
            }
            catch
            {

            }
        }

        public static void SendAsync(VentanillaTramite tramite, string message, string Subject)
        {
            string EmailHostImages = ConfigurationManager.AppSettings["EmailHostImages"];
            VentanillaDBContext db = new VentanillaDBContext();

            MailMessage email = new MailMessage();

            email.From = new MailAddress("ventanillasea@uv.mx");
            email.Subject = Subject;
            email.Body = message.Replace("[#ImagesPath#]", EmailHostImages);
            email.IsBodyHtml = true;
            email.Priority = MailPriority.Normal;

            email.To.Add(new MailAddress(tramite.Alumno.Email));
            email.To.Add(new MailAddress(tramite.Alumno.EmailAlterno));

          
            SmtpClient smtp = new SmtpClient("smtp.office365.com");
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.Credentials = new System.Net.NetworkCredential("ventanillasea@uv.mx", "SEAMANylosamos");
            smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            smtp.TargetName = "STARTTLS/smtp.office365.com";

            smtp.SendCompleted += new SendCompletedEventHandler(CheckIfError);

            ThreadPool.QueueUserWorkItem(o => smtp.SendAsync(email, null));
        }

        private static void CheckIfError(object sender, AsyncCompletedEventArgs e)
        {
          
          
            if (e.Error != null || e.Cancelled)
            {
                LogObject logObject = new LogObject();
                logObject.StatusRespone = 0;
                logObject.Username = "";
               

                if (e.Error.InnerException != null)
                {
                    logObject.Method = e.Error.InnerException.Message.ToString();
                }
                else
                {
                    logObject.OriginalString = e.Error.Message.ToString();
                }


                log.Info(logObject.ToString());

            }
        }

    }
}