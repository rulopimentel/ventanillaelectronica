﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Web;
using VentanillaElectronica.Models;
using System.DirectoryServices.AccountManagement;
using System.Net.Mime;

/// <summary>
/// Clase para autenticación al Active Directory
/// </summary>

namespace VentanillaElectronica.App_Code
{

    public class LdapAutenticacion
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string _path;
        private string _dominio;
        private string _filterAttribute;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="domain">Recibe el CAMPUS o REGIÓN de la cuenta institucional</param>
        public LdapAutenticacion(string domain)
        {
            //this._path = "LDAP://";
            switch (domain)
            {
                case "XALAPA":
                    this._path += System.Configuration.ConfigurationManager.AppSettings["XALAPA"].ToString();
                    break;
                case "VERACRUZ":
                    this._path += System.Configuration.ConfigurationManager.AppSettings["VERACRUZ"].ToString();
                    break;
                case "POZARICA":
                    this._path += System.Configuration.ConfigurationManager.AppSettings["POZARICA"].ToString();
                    break;
                case "COATZACOALCOS":
                    this._path += System.Configuration.ConfigurationManager.AppSettings["COATZACOALCOS"].ToString();
                    break;
                case "ORIZABA":
                    this._path += System.Configuration.ConfigurationManager.AppSettings["ORIZABA"].ToString();
                    break;
                case "ESTUDIANTES":
                    this._path += System.Configuration.ConfigurationManager.AppSettings["ESTUDIANTES"].ToString();
                    break;
            }
            this._dominio = domain;
        }

        /// <summary>
        /// Método para autenticar a un usuario con cuenta institucional
        /// </summary>
        /// <param name="username">La cuenta institucional del usuario</param>
        /// <param name="password">La contraseña del usuario</param>
        /// <returns>Regrasa un objeto del tipo UsuarioActiveDirectory con sus atributos</returns>


        public Usuario IsAuthenticatedV2(string username, string password)
        {
            Usuario usuario = null;

            PrincipalContext oPrincipalContext = new PrincipalContext(ContextType.Domain, this._path, null, ContextOptions.SimpleBind, username, password);
            if (oPrincipalContext.ValidateCredentials(username, password))
            {
                usuario = new Usuario(username, "");

                if (this._dominio == "ESTUDIANTES") usuario.Email = username.ToLower() + "@estudiantes.uv.mx";
                else usuario.Email = username.ToLower() + "@estudiantes.uv.mx";
                usuario.Nombre = "";
                usuario.UserName = username.ToLower();

                return usuario;

            }
            else
            {
                return usuario;
            };           
        }
        

        public Usuario IsAuthenticated(string username, string password)
        {
            Usuario usuario = null;
            
            try
            {

                String domainAndUsername = this._dominio + @"\" + username.Trim();
                DirectoryEntry Entry = new DirectoryEntry(this._path, domainAndUsername, password, AuthenticationTypes.Secure | AuthenticationTypes.Sealing | AuthenticationTypes.ServerBind);
                DirectorySearcher Search = new DirectorySearcher(Entry);
                Search.Filter = "(SAMAccountName=" + username + ")";
                SearchResult result = Search.FindOne();

                if (null == result)
                {
                    LogObject logObject = new LogObject();
                    logObject.StatusRespone = 0;
                    logObject.Username = "";                  
                    logObject.OriginalString = "No encontrado";
                    log.Info(logObject.ToString());

                    return usuario;
                }
                else
                {
                    DirectoryEntry cuenta = result.GetDirectoryEntry();
                    usuario = new Usuario(username, "");
                    usuario.Email = cuenta.Properties["mail"][0].ToString();
                    usuario.Nombre = cuenta.Properties["givenName"][0].ToString();
                    usuario.UserName = cuenta.Properties["SAMAccountName"][0].ToString();
                }
                this._path = result.Path;
                this._filterAttribute = (String)result.Properties["cn"][0];
                return usuario;
            }
            catch (Exception e)
            {
              

                LogObject logObject = new LogObject();
                logObject.StatusRespone = 0;
                logObject.Username = "";

                if (e.InnerException != null)
                {
                    logObject.Method = e.InnerException.Message.ToString();
                }
               
                logObject.OriginalString = e.Message.ToString();        
                log.Info(logObject.ToString());
                return null;
            }
        }
      
        /// <summary>
        /// Función para obtener las propiedades dentro del registro del Active Directory
        /// </summary>
        /// <param name="searchResult">El objeto de dónde se obtiene la propiedad</param>
        /// <param name="PropertyName">Nombre de la propiedad a obtener</param>
        /// <returns></returns>
        public static string GetProperty(SearchResult searchResult, string PropertyName)
        {
            if (searchResult.Properties.Contains(PropertyName))
            {
                return searchResult.Properties[PropertyName][0].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }
}