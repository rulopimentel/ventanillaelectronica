﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;

using System.Globalization;
using System.IO;
using System.Web;
using VentanillaElectronica.Models;

namespace VentanillaElectronica.App_Code
{


    class FormatEventsPDFReporteUltimaOportunidadRevisado : PdfPageEventHelper
    {
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            string bold_font = Path.Combine(
                   Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
                   "verdanab.ttf"
                   );
            FontFactory.Register(bold_font);
            BaseFont baseFont_bold = BaseFont.CreateFont(
                bold_font,
                BaseFont.IDENTITY_H,
                BaseFont.NOT_EMBEDDED
                );
            Font font_bold = new Font(baseFont_bold, 10, Font.NORMAL);
            // header font
            string header_font = Path.Combine(
               Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
               "verdanab.ttf"
               );
            FontFactory.Register(header_font);
            BaseFont baseFont_header = BaseFont.CreateFont(
                bold_font,
                BaseFont.IDENTITY_H,
                BaseFont.NOT_EMBEDDED
                );
            Font font_header = new Font(baseFont_header, 16, Font.NORMAL);


            string imageURL = HttpContext.Current.Server.MapPath("~/Content/images/logotipoReportes.png");
            Image logotipo = Image.GetInstance(imageURL);

            PdfPTable table = new PdfPTable(1);
            table.DefaultCell.Border = Rectangle.NO_BORDER;
            table.TotalWidth = document.PageSize.Width - document.LeftMargin - document.RightMargin;

            PdfPTable table2 = new PdfPTable(2);
            table2.DefaultCell.Border = Rectangle.NO_BORDER;




            PdfPCell cell2 = new PdfPCell();
            cell2.AddElement(new Paragraph("REPORTE DE SOLICITUDES PARA CONSEJO TÉCNICO", font_header));
            cell2.AddElement(new Paragraph("Solicitudes revisadas."));
            cell2.HorizontalAlignment = Element.ALIGN_LEFT;
            cell2.Border = Rectangle.NO_BORDER;

            table2.AddCell(cell2);

            cell2 = new PdfPCell(logotipo);
            cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell2.Border = Rectangle.NO_BORDER;

            table2.AddCell(cell2);

           

            PdfPCell cell = new PdfPCell(table2);
            cell.Border = Rectangle.NO_BORDER;

            table.AddCell(cell);

            table.WriteSelectedRows(0, -1, document.LeftMargin, document.Top + ((document.TopMargin + table.TotalHeight) / 2), writer.DirectContent);
        }

    }

    public class PDFReporteUltimaOportunidadRevisado
    {




        public static byte[] createPDF(List<VentanillaTramite> Tramites)
        {


            try
            {
                string normal_font = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
                    "verdana.ttf"
                    );
                FontFactory.Register(normal_font);
                BaseFont baseFont_normal = BaseFont.CreateFont(
                    normal_font,                    
                    BaseFont.IDENTITY_H,
                    BaseFont.NOT_EMBEDDED
                    );
                Font font_main = new Font(baseFont_normal, 9, Font.NORMAL);

                Font font_main_link = new Font(baseFont_normal, 9, Font.NORMAL, new BaseColor(43, 145, 175));


                // bold font
                string bold_font = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
                    "verdanab.ttf"
                    );
                FontFactory.Register(bold_font);
                BaseFont baseFont_bold = BaseFont.CreateFont(
                    bold_font,
                    BaseFont.IDENTITY_H,
                    BaseFont.NOT_EMBEDDED
                    );
                Font font_bold = new Font(baseFont_bold, 10, Font.NORMAL);

                Font font_bold_header = new Font(baseFont_bold, 12, Font.NORMAL);

                // italic font
                string italic_font = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
                    "verdanai.ttf"
                    );
                FontFactory.Register(italic_font);
                BaseFont baseFont_italic = BaseFont.CreateFont(
                    italic_font,
                    BaseFont.IDENTITY_H,
                    BaseFont.NOT_EMBEDDED
                    );
                Font font_italic = new Font(baseFont_italic, 8, Font.NORMAL);

                // header font
                string header_font = Path.Combine(
                   Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
                   "verdanab.ttf"
                   );
                FontFactory.Register(header_font);
                BaseFont baseFont_header = BaseFont.CreateFont(
                    bold_font,
                    BaseFont.IDENTITY_H,
                    BaseFont.NOT_EMBEDDED
                    );
                Font font_header = new Font(baseFont_header, 16, Font.NORMAL);


                FormatEventsPDFReporteUltimaOportunidadRevisado events = new FormatEventsPDFReporteUltimaOportunidadRevisado();


                using (MemoryStream memoryStream = new MemoryStream())
                using (Document doc = new Document(PageSize.LETTER, 80, 80, 170, 70))
                using (PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream))
                {
                    writer.ViewerPreferences = PdfWriter.HideMenubar;
                    writer.PageEvent = events;

                    doc.Open();

                    // --- meta information

                    doc.AddTitle("Reporte de solicitudes para examen de Última Oportunidad");
                    doc.AddSubject("SEA");
                    doc.AddKeywords("Examen de Última Oportunidad, SEA, UV");
                    doc.AddAuthor("rcolorado@uv.mx");
                    doc.AddCreator("Sistema de de Enseñanza Abierta");

                    // --- document contents

                

                    List<VentanillaTramite> ListaTramites = null;

                    foreach (EnumRegion region in Enum.GetValues(typeof(EnumRegion)))
                    {
                        ListaTramites = Tramites.Where(x => x.TramiteUltimaOportunidad.RegionPrograma.Region == region ).ToList();
                        if(ListaTramites != null && ListaTramites.Count > 0)
                        {
                            if(ListaTramites.Count == 1) doc.Add(new Paragraph(region.Description() + " -- " +  ListaTramites.Count.ToString() + " petición", font_bold_header));
                            else doc.Add(new Paragraph(region.Description() + " -- " + ListaTramites.Count.ToString() +  " peticiones", font_bold_header));
                            doc.Add(Chunk.NEWLINE);

                            foreach (VentanillaTramite tramite in ListaTramites)
                            {
                                doc.Add(new Paragraph("Programa " + tramite.TramiteUltimaOportunidad.RegionPrograma.Programa.ToString(), font_main));
                                doc.Add(new Paragraph("Alumno " + tramite.Alumno.Nombre, font_main));
                                doc.Add(new Paragraph("Correos " + tramite.Alumno.Email + ", " + tramite.Alumno.EmailAlterno, font_main));
                                doc.Add(new Paragraph("Experiencia Educativa " + tramite.TramiteUltimaOportunidad.ExperienciaEducativa, font_main));
                                doc.Add(new Paragraph("Docente " + tramite.TramiteUltimaOportunidad.Docente, font_main));
                                if (tramite.TramiteUltimaOportunidad.PresentaInconveniente)
                                {
                                    doc.Add(new Paragraph("SÍ Presenta inconveniente con el titular de la Experiencia Educativa", font_main));

                                }
                                else
                                {
                                    doc.Add(new Paragraph("Revisó solicitud " + tramite.TramiteUltimaOportunidad.AdministrativoRevision.Nombre, font_italic));

                                }

                                Paragraph cardex = new Paragraph("Cardex ", font_main);

                                Chunk url = new Chunk("Ver Cardex", font_main_link);
                                url.SetAnchor(tramite.TramiteUltimaOportunidad.Url);
                                cardex.Add(url);
                                doc.Add(cardex);

                                 doc.Add(new Paragraph("Fecha de solicitud " + tramite.FechaRegistro.ToString() + ", Revisó " + tramite.TramiteUltimaOportunidad.AdministrativoRevision.Nombre, font_italic));


                                PdfPTable myTable = new PdfPTable(4);
                                float[] widths = new float[] { 250f, 350f, 200f, 500f };

                                myTable.SetWidths(widths);

                                myTable.DefaultCell.Border = Rectangle.NO_BORDER;
                                myTable.TotalWidth = doc.PageSize.Width - doc.LeftMargin - doc.RightMargin;


                                //create a 3 cells and add them to the table
                                PdfPCell cellOne = new PdfPCell(new Paragraph("Fecha y hora:", font_main));
                                PdfPCell cellTwo = new PdfPCell(new Paragraph("", font_main));
                                PdfPCell cellThree = new PdfPCell(new Paragraph("Docentes:", font_main));
                                PdfPCell cellFour = new PdfPCell(new Paragraph("", font_main));


                                cellOne.Border = Rectangle.NO_BORDER;
                                cellTwo.Border = Rectangle.BOTTOM_BORDER;
                                cellThree.Border = Rectangle.NO_BORDER;
                                cellFour.Border = Rectangle.BOTTOM_BORDER;



                                myTable.AddCell(cellOne);
                                myTable.AddCell(cellTwo);
                                myTable.AddCell(cellThree);
                                myTable.AddCell(cellFour);

                                doc.Add(Chunk.NEWLINE);

                                doc.Add(myTable);


                                doc.Add(Chunk.NEWLINE);
                            }
                        }

                    }

                   
                    
                    Paragraph p = new Paragraph(string.Format("Este documento es generado a través de la Ventanilla Electrónica del Sistema de Enseñanza Abierta, con fecha del {0}.", DateTime.Now.ToString("dd MMM, yyyy HH:mm", CultureInfo.CreateSpecificCulture("es-MX"))), font_italic);
                    p.Alignment = Element.ALIGN_RIGHT;
                    doc.Add(p);
                    
                    doc.Close();
                    writer.Close();
                    
                    return memoryStream.ToArray();
                    
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        static Paragraph CustomParagraph(string text, Font font, int alignment)
        {

            Paragraph p = new Paragraph(text, font);
            p.Alignment = alignment;
            return p;
        }

    }
}
