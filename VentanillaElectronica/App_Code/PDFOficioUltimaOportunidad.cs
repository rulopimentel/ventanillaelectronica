﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;

using System.Globalization;
using System.IO;
using System.Web;
using VentanillaElectronica.Models;

namespace VentanillaElectronica.App_Code
{


    class FormatEventsPDFOficioUltimaOportunidad : PdfPageEventHelper
    {
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            string bold_font = Path.Combine(
                   Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
                   "verdanab.ttf"
                   );
            FontFactory.Register(bold_font);
            BaseFont baseFont_bold = BaseFont.CreateFont(
                bold_font,
                BaseFont.IDENTITY_H,
                BaseFont.NOT_EMBEDDED
                );
            Font font_bold = new Font(baseFont_bold, 10, Font.NORMAL);
            // header font
            string header_font = Path.Combine(
               Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
               "verdanab.ttf"
               );
            FontFactory.Register(header_font);
            BaseFont baseFont_header = BaseFont.CreateFont(
                bold_font,
                BaseFont.IDENTITY_H,
                BaseFont.NOT_EMBEDDED
                );
            Font font_header = new Font(baseFont_header, 16, Font.NORMAL);


            string imageURL = HttpContext.Current.Server.MapPath("~/Content/images/LogotipoUV.png");
            Image logotipo = Image.GetInstance(imageURL);

            PdfPTable table = new PdfPTable(1);
            table.DefaultCell.Border = Rectangle.NO_BORDER;
            table.TotalWidth = document.PageSize.Width - document.LeftMargin - document.RightMargin;

            PdfPTable table2 = new PdfPTable(2);
            table2.DefaultCell.Border = Rectangle.NO_BORDER;




            PdfPCell cell2 = new PdfPCell();
            //cell2.AddElement(new Paragraph("REPORTE DE SOLICITUDES PARA CONSEJO TÉCNICO", font_header));
            //cell2.AddElement(new Paragraph("Solicitudes revisadas."));
            cell2.HorizontalAlignment = Element.ALIGN_LEFT;
            cell2.Border = Rectangle.NO_BORDER;

            table2.AddCell(cell2);

            cell2 = new PdfPCell(logotipo);
            cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell2.Border = Rectangle.NO_BORDER;

            table2.AddCell(cell2);

           

            PdfPCell cell = new PdfPCell(table2);
            cell.Border = Rectangle.NO_BORDER;

            table.AddCell(cell);

            table.WriteSelectedRows(0, -1, document.LeftMargin, document.Top + ((document.TopMargin + table.TotalHeight) / 2), writer.DirectContent);
        }

    }

    public class PDFOficioUltimaOportunidad
    {




        public static byte[] createPDF(VentanillaTramite Tramite)
        {


            try
            {
                string normal_font = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
                    "verdana.ttf"
                    );
                FontFactory.Register(normal_font);
                BaseFont baseFont_normal = BaseFont.CreateFont(
                    normal_font,                    
                    BaseFont.IDENTITY_H,
                    BaseFont.NOT_EMBEDDED
                    );
                Font font_main = new Font(baseFont_normal, 10, Font.NORMAL);

                Font font_main_link = new Font(baseFont_normal, 9, Font.NORMAL, new BaseColor(43, 145, 175));


                // bold font
                string bold_font = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
                    "verdanab.ttf"
                    );
                FontFactory.Register(bold_font);
                BaseFont baseFont_bold = BaseFont.CreateFont(
                    bold_font,
                    BaseFont.IDENTITY_H,
                    BaseFont.NOT_EMBEDDED
                    );
                Font font_bold = new Font(baseFont_bold, 10, Font.NORMAL);

                Font font_bold_header = new Font(baseFont_bold, 12, Font.NORMAL);

                // italic font
                string italic_font = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
                    "verdanai.ttf"
                    );
                FontFactory.Register(italic_font);
                BaseFont baseFont_italic = BaseFont.CreateFont(
                    italic_font,
                    BaseFont.IDENTITY_H,
                    BaseFont.NOT_EMBEDDED
                    );
                Font font_small = new Font(baseFont_normal, 9, Font.NORMAL);

                // header font
                string header_font = Path.Combine(
                   Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
                   "verdanab.ttf"
                   );
                FontFactory.Register(header_font);
                BaseFont baseFont_header = BaseFont.CreateFont(
                    bold_font,
                    BaseFont.IDENTITY_H,
                    BaseFont.NOT_EMBEDDED
                    );
                Font font_header = new Font(baseFont_header, 16, Font.NORMAL);


                FormatEventsPDFOficioUltimaOportunidad events = new FormatEventsPDFOficioUltimaOportunidad();


                using (MemoryStream memoryStream = new MemoryStream())
                using (Document doc = new Document(PageSize.LETTER, 80, 80, 150, 70))
                using (PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream))
                {
                    writer.ViewerPreferences = PdfWriter.HideMenubar;
                    writer.PageEvent = events;

                    doc.Open();

                    // --- meta information

                    doc.AddTitle("Reporte de Oficios para examen de Última Oportunidad");
                    doc.AddSubject("SEA");
                    doc.AddKeywords("Examen de Última Oportunidad, SEA, UV");
                    doc.AddAuthor("rcolorado@uv.mx");
                    doc.AddCreator("Sistema de de Enseñanza Abierta");

                    // --- document contents

                    Paragraph p = new Paragraph("Of. SA/VE/" + DateTime.Now.Year + "/Tramite-" + Tramite.Id, font_small);
                    p.Alignment = Element.ALIGN_RIGHT;
                    doc.Add(p);

                    p = new Paragraph("Xalapa, Ver., " + DateTime.Now.ToLongDateString() ,font_small);
                    p.Alignment = Element.ALIGN_RIGHT;
                    doc.Add(p);
                    doc.Add(Chunk.NEWLINE);



                    doc.Add(new Paragraph(Tramite.TramiteUltimaOportunidad.DocenteAplicacion1, font_main));
                    doc.Add(new Paragraph(Tramite.TramiteUltimaOportunidad.DocenteAplicacion2, font_main));
                    doc.Add(new Paragraph("Programa educativo de " + Tramite.TramiteUltimaOportunidad.RegionPrograma.Programa.ToString(), font_main));
                    doc.Add(new Paragraph("Región " + Tramite.TramiteUltimaOportunidad.RegionPrograma.Region.ToString(), font_main));
                    doc.Add(Chunk.NEWLINE);
                    doc.Add(Chunk.NEWLINE);


                    doc.Add(new Paragraph(string.Format("Por medio del presente me permito informarles que en sesión de Consejo Técnico celebrado el {0}, le fue autorizado al alumno C. {1}, matrícula {2}, presentar examen en carácter de ÚLTIMA OPORTUNIDAD la Experiencia Educativa {3}.", Tramite.TramiteUltimaOportunidad.FechaConsejoTecnico.Value.ToLongDateString(), Tramite.Alumno.Nombre, Tramite.Alumno.UserName, Tramite.TramiteUltimaOportunidad.ExperienciaEducativa), font_main));
                    doc.Add(Chunk.NEWLINE);
                    doc.Add(new Paragraph(string.Format("Por lo anterior, les solicito realizar y aplicar de manera coordinada dicho examen el próximo {0}, a las {1} y entregar el acta de calificación debidamente firmada, en un plazo no mayor a cinco días, posteriores al mismo.", Tramite.TramiteUltimaOportunidad.FechaExamen.Value.ToLongDateString(), Tramite.TramiteUltimaOportunidad.FechaExamen.Value.ToShortTimeString()), font_main));
                    doc.Add(Chunk.NEWLINE);
                    doc.Add(new Paragraph(string.Format("Sin otro particular, reciba un cordial saludo."), font_main));
                    doc.Add(Chunk.NEWLINE);
                    doc.Add(Chunk.NEWLINE);
                    doc.Add(Chunk.NEWLINE);

                    p = new Paragraph("A t e n t a m e n t e", font_main);
                    p.Alignment = Element.ALIGN_CENTER;
                    doc.Add(p);

                    p = new Paragraph("“Lis de Veracruz: Arte, Ciencia, Luz”", font_main);
                    p.Alignment = Element.ALIGN_CENTER;
                    doc.Add(p);
                    doc.Add(Chunk.NEWLINE);
                    doc.Add(Chunk.NEWLINE);
                    doc.Add(Chunk.NEWLINE);

                    p = new Paragraph("Mtra.Gabriela Calles Barradas", font_main);
                    p.Alignment = Element.ALIGN_CENTER;
                    doc.Add(p);
                    p = new Paragraph("Secretaria", font_main);
                    p.Alignment = Element.ALIGN_CENTER;
                    doc.Add(p);

                    doc.Add(Chunk.NEWLINE);
                    doc.Add(Chunk.NEWLINE);





                    doc.Add(new Paragraph(string.Format("c.c.p. Interesado(a)"), font_small));
                    doc.Add(new Paragraph(string.Format("       Coordinador(a) Regional"), font_small));
                    doc.Add(new Paragraph(string.Format("       Encargado(a) de cardex"), font_small));
                    doc.Add(new Paragraph(string.Format("       Archivo"), font_small));
                    doc.Add(new Paragraph(string.Format("       Mags/"), font_small));


                    doc.Close();
                    writer.Close();
                    
                    return memoryStream.ToArray();
                    
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        static Paragraph CustomParagraph(string text, Font font, int alignment)
        {

            Paragraph p = new Paragraph(text, font);
            p.Alignment = alignment;
            return p;
        }

    }
}
