﻿using VentanillaElectronica.App_Start;
using System;
using System.Diagnostics;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Optimization;
using System.Web;
using System.Text.RegularExpressions;
using VentanillaElectronica.Models;
using System.Linq;
using System.Security.Principal;
using System.Collections.Generic;

namespace VentanillaElectronica
{
    public class Global : System.Web.HttpApplication
    {
        VentanillaDBContext db = new VentanillaDBContext();

        protected void Application_Start(object sender, EventArgs e)
        {
            FilterConfig.Configure(GlobalFilters.Filters);
            RouteConfig.Configure(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            log4net.Config.XmlConfigurator.Configure();
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                
                string username = HttpContext.Current.User.Identity.Name;
                Usuario usuario = new Usuario(username, "");

                if (Regex.Match(username, @"^[Z]?[S]\d{8,10}$", RegexOptions.IgnoreCase).Success)
                {
                    VentanillaAlumno alumno = db.Alumnos.Where(x => x.UserName == username).FirstOrDefault();
                    if(alumno!= null)
                    {
                        usuario.Nombre = alumno.Nombre;
                        usuario.Exist = true;
                        usuario.IsAlumno = true;
                        usuario.Email = alumno.Email;
                        usuario.UserName = alumno.UserName;
                    }
                    String[] roles = { "ALUMNOS"};
                    var genericPrincipal = new GenericPrincipal(usuario, roles);
                    HttpContext.Current.User = genericPrincipal;

                }
                else
                {
                    VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == username).FirstOrDefault();
                    List<string> roles = new List<string>();
                    if (administrativo != null)
                    {
                        usuario.Nombre = administrativo.Nombre;
                        usuario.Exist = true;
                        usuario.IsAlumno = true;
                        usuario.Email = administrativo.Email;
                        usuario.UserName = administrativo.UserName;

                        roles = administrativo.AdministrativosPrograma.Select(x => x.Programa.ToString()).ToList();

                        HashSet<string> permisos = new HashSet<string>();
                        foreach(VentanillaPermisos permiso in administrativo.Permisos)
                        {
                            permisos.Add(permiso.Modulo.Clave);
                            permisos.Add(permiso.Modulo.Clave + "-" + permiso.RegionPrograma.Region + "-" + permiso.RegionPrograma.Programa);

                        }

                        roles.AddRange(permisos);
                       

                    }
                    roles.Add("ADMINISTRATIVOS");
                   

                    var genericPrincipal = new GenericPrincipal(usuario, roles.ToArray());
                    HttpContext.Current.User = genericPrincipal;
                }           

            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            if (ex != null)
            {
                Trace.TraceError(ex.ToString());
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}