﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using VentanillaElectronica.App_Code;
using VentanillaElectronica.Models;

namespace VentanillaElectronica.Controllers
{
    [Authorize(Roles = "ADMINISTRATIVOS")]

    public class AdministrativosTramiteAltasController : Controller
    {
        VentanillaDBContext db = new VentanillaDBContext();


        [HttpGet]
        public ActionResult View(int? Id)
        {
            if (Id == null) return RedirectToAction("Dashboard", "Administrativos");

            VentanillaTramite tramite = db.Tramites.Where(x => x.Id == Id).FirstOrDefault();

            if (tramite != null)
            {
                return View(tramite);
            }
            else { return RedirectToAction("Dashboard", "Administrativos"); }



        }

        [HttpGet]
        public ActionResult Serve(int? Id)
        {
            if (Id == null) return RedirectToAction("Dashboard", "Administrativos");

            VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            int[] programas = administrativo.AdministrativosPrograma.Select(x => (int) x.Programa).ToArray();

            VentanillaTramite tramite = db.Tramites.Where(x => x.Id == Id && programas.Contains((int) x.TramiteAlta.OfertaAlta.InscripcionAltas.Programa)).FirstOrDefault();

            if (tramite != null)
            {
                if (tramite.Estatus != EnumTramitesEstatus.Solicitado) return RedirectToAction("View", "AdministrativosTramiteAltas", new { Id = Id });

                ModelViewServeTramitesAlta modelViewServeTramitesAlta = new ModelViewServeTramitesAlta();
                modelViewServeTramitesAlta.Administrativo = administrativo;
                modelViewServeTramitesAlta.Alumno = tramite.Alumno;
                modelViewServeTramitesAlta.Estatus = (EnumTramitesEstatus) tramite.Estatus;

                modelViewServeTramitesAlta.FechaRegistro = tramite.FechaRegistro;
                modelViewServeTramitesAlta.OfertaAlta = tramite.TramiteAlta.OfertaAlta;
                modelViewServeTramitesAlta.VentanillaAdministrativoId = administrativo.Id;
                modelViewServeTramitesAlta.VentanillaAlumnoId = tramite.Alumno.Id;
                modelViewServeTramitesAlta.VentanillaOfertaAltasId = tramite.TramiteAlta.OfertaAlta.Id;
                modelViewServeTramitesAlta.NumeroInscripcion = tramite.TramiteAlta.NumeroInscripcion;

                return View(modelViewServeTramitesAlta);
            }

            return RedirectToAction("Dashboard", "Administrativos");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [NoCache]
        public ActionResult Serve(ModelViewServeTramitesAlta serveTramitesAlta)
        {
            if (serveTramitesAlta != null && ModelState.IsValid)
            {

                VentanillaTramite tramite = db.Tramites.Where(x => x.Id == serveTramitesAlta.Id).FirstOrDefault();
                if (tramite == null) return RedirectToAction("Dashboard", "Administrativos");
                if (tramite.Estatus != EnumTramitesEstatus.Solicitado)
                {
                    return RedirectToAction("View", "AdministrativosTramiteAltas", new { Id = tramite.Id });
                }

                VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
                                
                tramite.Estatus = (EnumTramitesEstatus) serveTramitesAlta.EstatusServe;
                tramite.FechaActualizacion = DateTime.Now;
                tramite.FechaAtencion = DateTime.Now;
                tramite.Observaciones = serveTramitesAlta.Observaciones;
                tramite.VentanillaAdministrativoId = administrativo.Id;

                db.Entry(tramite).State = EntityState.Modified;
                db.SaveChanges();

                MessagesTools message = new MessagesTools();
                message.SendMailTramiteAltaServe(tramite);

                return RedirectToAction("View", "AdministrativosTramiteAltas", new { Id = tramite.Id });
            }
            return View(serveTramitesAlta);
        }





    }
}