﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using VentanillaElectronica.App_Code;
using VentanillaElectronica.Models;

namespace VentanillaElectronica.Controllers
{
    [Authorize(Roles = "ADMINISTRADORES")]

    public class AdministradoresTramiteMovilidadController : Controller
    {
        VentanillaDBContext db = new VentanillaDBContext();


        [HttpGet]
        public ActionResult Edit(int? Id)
        {
            if (Id == null) return RedirectToAction("Dashboard", "Administrativos");

            VentanillaTramite tramite = db.Tramites.Where(x => x.Id == Id && (x.Estatus == EnumTramitesEstatus.Solicitado || x.Estatus == EnumTramitesEstatus.Denegado)).FirstOrDefault();

            if (tramite != null)
            {

                ModelViewVentanillaTramiteMovilidad tramiteModelView = new ModelViewVentanillaTramiteMovilidad();
                tramiteModelView.Administrativo = tramite.Administrativo;
                tramiteModelView.Alumno = tramite.Alumno;
                tramiteModelView.Estatus = tramite.Estatus;
                tramiteModelView.FechaActualizacion = tramite.FechaActualizacion;
                tramiteModelView.FechaAtencion = tramite.FechaAtencion;
                tramiteModelView.FechaRegistro = tramite.FechaRegistro;
                tramiteModelView.Id = tramite.Id;
                tramiteModelView.Observaciones = tramite.Observaciones;
                tramiteModelView.Periodo = tramite.Periodo;
                tramiteModelView.Programa = tramite.Programa;
                tramiteModelView.Tipo = tramite.Tipo;
                tramiteModelView.NewEstatus = tramite.Estatus;
                tramiteModelView.TramiteMovilidad = tramite.TramiteMovilidad;
                tramiteModelView.VentanillaAdministrativoId = tramite.VentanillaAdministrativoId;
                tramiteModelView.VentanillaAlumnoId = tramite.VentanillaAlumnoId;
                tramiteModelView.VentanillaPeriodoId = tramite.VentanillaPeriodoId;


                return View(tramiteModelView);
            }
            else { return RedirectToAction("Dashboard", "Administrativos"); }
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        [NoCache]
        public ActionResult Edit(int? Id, HttpPostedFileBase File, EnumTramitesEstatus NewEstatus)
        {
      
            if (Id == null) return RedirectToAction("Dashboard", "Administrativos");

            VentanillaTramite tramite = db.Tramites.Where(z => z.Id == Id && z.Estatus != EnumTramitesEstatus.Aceptado).FirstOrDefault();

            ModelViewTramitesEscenarios escenario = new ModelViewTramitesEscenarios();
            
            if (tramite != null)
            {
                if (File != null)
                {
                    string extension = Path.GetExtension(File.FileName);
                    string name = tramite.TramiteMovilidad.OfertaMovilidad.InscripcionMovilidad.Programa + "_NRC" + tramite.TramiteMovilidad.OfertaMovilidad.NRC + "_" + tramite.Alumno.UserName + "_" + String.Format("{0:ddMMyyyy}", DateTime.Now) + extension;
                    var path = Path.Combine(Server.MapPath("~/Uploads/Movilidad"), name);
                    File.SaveAs(path);
                    string URL = System.Configuration.ConfigurationManager.AppSettings["ServerApp"].ToString() + "Uploads/Movilidad/" + name;
                    tramite.TramiteMovilidad.Url = URL;
                }

                if (NewEstatus == EnumTramitesEstatus.Solicitado)
                {
                    tramite.Administrativo = null;
                    tramite.Estatus = EnumTramitesEstatus.Solicitado;
                    tramite.Observaciones = null;
                    tramite.FechaAtencion = null;
                    tramite.VentanillaAdministrativoId = null;
                }

                tramite.FechaActualizacion = DateTime.Now;
                db.Entry(tramite).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("View", "AdministrativosTramiteMovilidad", new { Id = tramite.Id });

            }
            return RedirectToAction("Dashboard", "Administrativos");

        }
           
    





}
}