﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using VentanillaElectronica.App_Code;
using VentanillaElectronica.Models;

namespace VentanillaElectronica.Controllers
{
    [Authorize(Roles = "ADMINISTRATIVOS")]

    public class AdministrativosTramiteMovilidadController : Controller
    {
        VentanillaDBContext db = new VentanillaDBContext();


        [HttpGet]
        public ActionResult View(int? Id)
        {
            if (Id == null) return RedirectToAction("Dashboard", "Administrativos");

            VentanillaTramite tramite = db.Tramites.Where(x => x.Id == Id).FirstOrDefault();

            if (tramite != null)
            {
                return View(tramite);
            }
            else { return RedirectToAction("Dashboard", "Administrativos"); }



        }

        [HttpGet]
        public ActionResult Serve(int? Id)
        {
            if (Id == null) return RedirectToAction("Dashboard", "Administrativos");

            VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            int[] programas = administrativo.AdministrativosPrograma.Select(x => (int) x.Programa).ToArray();

            VentanillaTramite tramite = db.Tramites.Where(x => x.Id == Id && programas.Contains((int) x.TramiteMovilidad.OfertaMovilidad.InscripcionMovilidad.Programa)).FirstOrDefault();

            if (tramite != null)
            {
                if (tramite.Estatus != EnumTramitesEstatus.Solicitado) return RedirectToAction("View", "AdministrativosTramiteMovilidad", new { Id = Id });

                ModelViewServeTramitesMovilidad modelViewServeTramitesMovilidad = new ModelViewServeTramitesMovilidad();
                modelViewServeTramitesMovilidad.Administrativo = administrativo;
                modelViewServeTramitesMovilidad.Alumno = tramite.Alumno;
                modelViewServeTramitesMovilidad.Estatus = (EnumTramitesEstatus) tramite.Estatus;

                modelViewServeTramitesMovilidad.FechaRegistro = tramite.FechaRegistro;
                modelViewServeTramitesMovilidad.OfertaMovilidad = tramite.TramiteMovilidad.OfertaMovilidad;
                modelViewServeTramitesMovilidad.Url = tramite.TramiteMovilidad.Url;
                modelViewServeTramitesMovilidad.VentanillaAdministrativoId = administrativo.Id;
                modelViewServeTramitesMovilidad.VentanillaAlumnoId = tramite.Alumno.Id;
                modelViewServeTramitesMovilidad.VentanillaOfertaMovilidadId = tramite.TramiteMovilidad.OfertaMovilidad.Id;

                return View(modelViewServeTramitesMovilidad);
            }

            return RedirectToAction("Dashboard", "Administrativos");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [NoCache]
        public ActionResult Serve(ModelViewServeTramitesMovilidad serveTramitesMovilidad)
        {
            if (serveTramitesMovilidad != null && ModelState.IsValid)
            {

                VentanillaTramite tramite = db.Tramites.Where(x => x.Id == serveTramitesMovilidad.Id).FirstOrDefault();
                if (tramite == null) return RedirectToAction("Dashboard", "Administrativos");
                if (tramite.Estatus != EnumTramitesEstatus.Solicitado)
                {
                    return RedirectToAction("View", "AdministrativosTramiteMovilidad", new { Id = tramite.Id });
                }

                VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
                                
                tramite.Estatus = (EnumTramitesEstatus) serveTramitesMovilidad.EstatusServe;
                tramite.FechaActualizacion = DateTime.Now;
                tramite.FechaAtencion = DateTime.Now;
                tramite.Observaciones = serveTramitesMovilidad.Observaciones;
                tramite.VentanillaAdministrativoId = administrativo.Id;

                db.Entry(tramite).State = EntityState.Modified;
                db.SaveChanges();

                MessagesTools message = new MessagesTools();
                message.SendMailTramitemovilidadServe(tramite);

                return RedirectToAction("View", "AdministrativosTramiteMovilidad", new { Id = tramite.Id });
            }
            return View(serveTramitesMovilidad);
        }





    }
}