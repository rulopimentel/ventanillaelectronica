﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VentanillaElectronica.App_Code;
using VentanillaElectronica.Models;

namespace VentanillaElectronica.Controllers
{
    [Authorize(Roles = "ALUMNOS")]

    public class AlumnoTramiteAltasController : Controller
    {
        VentanillaDBContext db = new VentanillaDBContext();

        [HttpGet]
        public ActionResult Index()
        {
            int periodoId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["IdPeriodoActivo"]);

            ModelViewInscripcionAltas modelViewInscripcionAltas = new ModelViewInscripcionAltas();
            modelViewInscripcionAltas.periodo = db.Periodos.Where(x => x.Id == periodoId).FirstOrDefault();
            modelViewInscripcionAltas.inscripcionAltas = db.InscripcionAltas.Where(x => x.VentanillaPeriodoId == modelViewInscripcionAltas.periodo.Id).ToList();
            modelViewInscripcionAltas.alumno = db.Alumnos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            return View(modelViewInscripcionAltas);
        }

        [HttpGet]
        public ActionResult View(int? Id)
        {
            if (Id == null) return RedirectToAction("Dashboard", "Alumnos");

            VentanillaAlumno alumno = db.Alumnos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            VentanillaTramite tramite = db.Tramites.Where(x => x.Id == Id && x.VentanillaAlumnoId == alumno.Id).FirstOrDefault();

            if (tramite != null)
            {
                return View(tramite);
            }
            else { return RedirectToAction("Dashboard", "Alumnos"); }
        }

        [HttpGet]
        public ActionResult Estatus()
        {
            ModelViewTramitesEscenarios escenario = (ModelViewTramitesEscenarios)TempData["escenario"];

            return View(escenario);
        }

        [NoCache]
        [HttpGet]
        public ActionResult Register(int? Id)
        {
            if (Id == null) return RedirectToAction("Index", "AlumnoTramiteAltas");

            VentanillaOfertaAltas ofertaAlta = db.OfertaAltas.Where(x => x.Id == Id).FirstOrDefault();

            if (ofertaAlta != null)
            {

                ModelViewTramitesEscenarios escenario = new ModelViewTramitesEscenarios();

                if (DateTime.Now >= ofertaAlta.InscripcionAltas.FechaInscripcionInicial && DateTime.Now < ofertaAlta.InscripcionAltas.FechaInscripcionFinal)
                {

                    VentanillaAlumno alumno = db.Alumnos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();

                    int periodoId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["IdPeriodoActivo"]);
                    int contadorTramitesExistentes = db.TramitesAltas.Where(x => x.OfertaAlta.Id == ofertaAlta.Id).Count();

                  
                    if ((ofertaAlta.Cupo - contadorTramitesExistentes) <= 0)
                    {
                        escenario.Estatus = EnumTramitesEscenarios.CupoSaturado;
                        escenario.mensaje = "El cupo de la oferta a la que has intentado realizar el trámite ya ha sido cubierto. Lamentamos las molestias. <br/><br/>" + ofertaAlta.InscripcionAltas.Programa + "<br/><strong>NRC(" + ofertaAlta.NRC + ")</strong> " + ofertaAlta.Nombre;
                        TempData["escenario"] = escenario;
                        return RedirectToAction("Estatus", "AlumnoTramiteAltas");
                    }

                    if (db.TramitesAltas.Where(x => x.Tramite.VentanillaAlumnoId == alumno.Id && x.VentanillaOfertaAltasId == ofertaAlta.Id).FirstOrDefault() != null)
                    {
                        escenario.Estatus = EnumTramitesEscenarios.RegistroExitente;
                        escenario.mensaje = "El sistema ha detectado que ya has solicitado previamente este trámite. Puedes consultar el seguimiento de tu trámite a través de tu correo electrónico y mediante esta plataforma.<br/><br/>" + ofertaAlta.InscripcionAltas.Programa + "<br/><strong>NRC(" + ofertaAlta.NRC + ")</strong> " + ofertaAlta.Nombre;
                        TempData["escenario"] = escenario;
                        return RedirectToAction("Estatus", "AlumnoTramiteAltas");
                    }

                    ModelViewTramitesAltas modelViewTramitesAltas = new ModelViewTramitesAltas();
                    VentanillaTramite tramite = new VentanillaTramite();
                    tramite.VentanillaAlumnoId = alumno.Id;
                    tramite.TramiteAlta = new VentanillaTramitesAltas();
                    tramite.Tipo = EnumTramitesTipo.Alta;
                    tramite.Programa = ofertaAlta.InscripcionAltas.Programa;

                    tramite.TramiteAlta.VentanillaOfertaAltasId = ofertaAlta.Id;
                    tramite.TramiteAlta.OfertaAlta = ofertaAlta;
                    modelViewTramitesAltas.periodo = db.Periodos.Where(x => x.Id == ofertaAlta.InscripcionAltas.Periodo.Id).FirstOrDefault();
                    modelViewTramitesAltas.tramite = tramite;
                    modelViewTramitesAltas.alumno = alumno;
                    modelViewTramitesAltas.inscripcionAlta = ofertaAlta.InscripcionAltas;
                    return View(modelViewTramitesAltas);
                }
                else
                {
                    escenario.Estatus = EnumTramitesEscenarios.PeriodoNoDisponible;
                    escenario.mensaje = "Las fechas para la realización del trámite no están disponibles. Por favor atiende a las fechas disponibles del programa de estudios.<br/><br/>" + ofertaAlta.InscripcionAltas.Programa + "<br/><strong>NRC(" + ofertaAlta.NRC + ")</strong> " + ofertaAlta.Nombre + "<br/>" + ofertaAlta.InscripcionAltas.FechaInscripcionInicial + " al " + ofertaAlta.InscripcionAltas.FechaInscripcionFinal;
                    TempData["escenario"] = escenario;
                    return RedirectToAction("Estatus", "AlumnoTramiteAltas");
                }
            }
            else { return RedirectToAction("Index", "AlumnoTramiteAltas"); }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [NoCache]
        public ActionResult Register(int? Id, ModelViewTramitesAltas modelViewTramitesAltas)
        {
            if (Id == null) return RedirectToAction("Index", "AlumnoTramiteAltas");

            VentanillaOfertaAltas ofertaAlta = db.OfertaAltas.Where(x => x.Id == Id).FirstOrDefault();
            VentanillaAlumno alumno = db.Alumnos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            ModelViewTramitesEscenarios escenario = new ModelViewTramitesEscenarios();


            if (ofertaAlta != null)
            {
                try
                {
                    int periodoId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["IdPeriodoActivo"]);
                    VentanillaPeriodo periodo = db.Periodos.Where(x => x.Id == periodoId).FirstOrDefault();

                    int contadorTramitesExistentes = db.TramitesAltas.Where(x => x.OfertaAlta.Id == ofertaAlta.Id).Count();

                 
                    if (DateTime.Now >= ofertaAlta.InscripcionAltas.FechaInscripcionInicial && DateTime.Now < ofertaAlta.InscripcionAltas.FechaInscripcionFinal)
                    {                                             

                        VentanillaTramite tramite = new VentanillaTramite();
                        tramite.VentanillaAlumnoId = alumno.Id;
                        tramite.Estatus = EnumTramitesEstatus.Solicitado;
                        tramite.Periodo = periodo;
                        tramite.FechaRegistro = DateTime.Now;
                        tramite.FechaActualizacion = DateTime.Now;
                        tramite.Programa = ofertaAlta.InscripcionAltas.Programa;
                        tramite.VentanillaPeriodoId = ofertaAlta.InscripcionAltas.Periodo.Id;
                        tramite.Tipo = EnumTramitesTipo.Alta;
                        tramite.TramiteAlta = new VentanillaTramitesAltas();
                        tramite.TramiteAlta.VentanillaOfertaAltasId = ofertaAlta.Id;
                        tramite.TramiteAlta.NumeroInscripcion = modelViewTramitesAltas.NumeroInscripcion;


                        if ((ofertaAlta.Cupo - contadorTramitesExistentes) <= 0)
                        {
                            escenario.Estatus = EnumTramitesEscenarios.CupoSaturado;
                            escenario.mensaje = "El cupo de la oferta a la que has intentado realizar el trámite, ya ha sido cubierto. Lamentamos las molestias. <br/><br/>" + ofertaAlta.InscripcionAltas.Programa + "<br/><strong>NRC(" + ofertaAlta.NRC + ")</strong> " + ofertaAlta.Nombre;
                            TempData["escenario"] = escenario;
                            return RedirectToAction("Estatus", "AlumnoTramiteAltas");
                        }

                        if (db.Tramites.Where(x => x.VentanillaAlumnoId == alumno.Id && x.TramiteAlta.VentanillaOfertaAltasId == ofertaAlta.Id).FirstOrDefault() != null)
                        {
                            escenario.Estatus = EnumTramitesEscenarios.RegistroExitente;
                            escenario.mensaje = "El sistema ha detectado que ya has solicitado previamente este trámite. Puedes consultar el seguimiento de tu trámite a través de tu correo electrónico y mediante esta plataforma.<br/><br/>" + ofertaAlta.InscripcionAltas.Programa + "<br/><strong>NRC(" + ofertaAlta.NRC + ")</strong> " + ofertaAlta.Nombre;
                            TempData["escenario"] = escenario;
                            return RedirectToAction("Estatus", "AlumnoTramiteAltas");
                        }

                        db.Tramites.Add(tramite);
                        db.SaveChangesAsync();

                       
                        MessagesTools message = new MessagesTools();
                        message.SendMailTramiteAlta(tramite);

                        escenario.Estatus = EnumTramitesEscenarios.RegistroExitoso;
                        escenario.mensaje = "La solicitud se ha efectuado con éxito. El estatus de tu solicitud es <span class='label label-warning'>" + tramite.Estatus + "</span>. Puedes consultar el estatus de tu trámite a través de tu correo electrónico y mediante esta plataforma.<br/><br/>" + ofertaAlta.InscripcionAltas.Programa + "<br/><strong>NRC(" + ofertaAlta.NRC + ")</strong> " + ofertaAlta.Nombre;
                        TempData["escenario"] = escenario;
                        return RedirectToAction("Estatus", "AlumnoTramiteAltas");
                    }
                    else
                    {
                        escenario.Estatus = EnumTramitesEscenarios.PeriodoNoDisponible;
                        escenario.mensaje = "Las fechas para la realización del trámite no están disponibles. Por favor atiende a las fechas disponibles del programa de estudios.<br/><br/>" + ofertaAlta.InscripcionAltas.Programa + "<br/><strong>NRC(" + ofertaAlta.NRC + ")</strong> " + ofertaAlta.Nombre + "<br/>" + ofertaAlta.InscripcionAltas.FechaInscripcionInicial + " al " + ofertaAlta.InscripcionAltas.FechaInscripcionFinal;
                        TempData["escenario"] = escenario;
                        return RedirectToAction("Estatus", "AlumnoTramiteAltas");
                    }
                }
                catch (Exception ex)
                {
                    escenario.Estatus = EnumTramitesEscenarios.Error;
                    escenario.mensaje = "Hubo un error al procesar la solicitud.<br/>" + ex.Message.ToString();
                    TempData["escenario"] = escenario;
                    return RedirectToAction("Estatus", "AlumnoTramiteAltas");
                }

            }




            return RedirectToAction("Index", "AlumnoTramiteAltas");
        }

       
    }



}