﻿using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VentanillaElectronica.App_Code;
using VentanillaElectronica.Models;

namespace VentanillaElectronica.Controllers
{
    [Authorize(Roles = "ALUMNOS, ADMINISTRATIVOS")]

    public class CatalogosController : Controller
    {
        private VentanillaDBContext db = new VentanillaDBContext();

        [HttpPost]
        public JsonResult GetExperienciasEducativas(string keyword)
        {
            var ExperienciasEducativas = db.ExperienciasEducativas.Where(y => y.Nombre.Contains(keyword)).Select(a => a.Nombre);

            return Json(ExperienciasEducativas, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetAcademicos(string keyword)
        {
            var Academicos = db.Academicos.Where(y => y.Nombre.Contains(keyword)).Select(a => a.Nombre);

            return Json(Academicos, JsonRequestBehavior.AllowGet);
        }
    }
}
