﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using VentanillaElectronica.App_Code;
using VentanillaElectronica.Models;

namespace VentanillaElectronica.Controllers
{
    [Authorize(Roles = "ADMINISTRATIVOS")]

    public class AdministrativosController : Controller
    {
        VentanillaDBContext db = new VentanillaDBContext();

        [HttpGet]
        public ActionResult Header()
        {
            Usuario usuario = (Usuario)HttpContext.User.Identity;
            return View(usuario);
        }

        [HttpGet]
        public ViewResult Dashboard(string currentFilter, int? page, int? EstatusFilter, string searchString)
        {
            EnumTramitesEstatus estatus = EnumTramitesEstatus.Solicitado;
            ViewBag.EstatusFilter = EnumTramitesEstatus.Solicitado;
            ViewBag.EstatusFilterInt = (int)EnumTramitesEstatus.Solicitado;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            if (EstatusFilter != null)
            {
                if (Enum.IsDefined(typeof(EnumTramitesEstatus), EstatusFilter)) estatus = (EnumTramitesEstatus)EstatusFilter;
                ViewBag.EstatusFilter = estatus;
                ViewBag.EstatusFilterInt = (int)estatus;
            }

            VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            int[] programas = administrativo.AdministrativosPrograma.Select(x => (int)x.Programa).ToArray();

            List<VentanillaTramite> tramites;
            if (estatus == EnumTramitesEstatus.Solicitado)
            {
                tramites = db.Tramites.Where(x => (x.Estatus == estatus) && programas.Contains((int)x.Programa)).ToList();
            }
            else
            {
                tramites = db.Tramites.Where(x => x.Estatus == estatus && programas.Contains((int)x.Programa)).ToList();
            }
            if (!String.IsNullOrEmpty(searchString))
            {
                int[] alumnos = db.Alumnos.Where(y => y.Nombre.Contains(searchString)).Select(x => (int)x.Id).ToArray();
                tramites = tramites.Where(s => alumnos.Contains(s.VentanillaAlumnoId)).ToList();
            }

            if (estatus == EnumTramitesEstatus.Denegado || estatus == EnumTramitesEstatus.Aceptado)
            {
                tramites = tramites.OrderByDescending(x => x.FechaRegistro).OrderBy(x => x.Estatus).ToList();

            }
            else
            {
                tramites = tramites.OrderBy(x => x.FechaRegistro).OrderBy(x => x.Estatus).ToList();
            }

            int pageSize = int.Parse(System.Configuration.ConfigurationManager.AppSettings["PageSize"].ToString());
            int pageNumber = (page ?? 1);
            return View(tramites.ToPagedList(pageNumber, pageSize));
        }


        [HttpGet]
        [Authorize(Roles = "T407")]
        public ActionResult ReporteExamenUltimaOportunidadRevisados()
        {
            try
            {
                
                VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();

                int[] Permisos = administrativo.Permisos.Where(z => z.Modulo.Clave.Equals("T407")).Select(x => (int) x.RegionPrograma.Id).ToArray();
                
                List<VentanillaTramite> Tramites = db.Tramites.Where(x => x.Estatus == EnumTramitesEstatus.Solicitado && x.TramiteUltimaOportunidad.Estatus == EnumTramitesUltimaOportunidadEstatus.Revisado && x.Tipo == EnumTramitesTipo.UltimaOportunidad && (Permisos.Contains((int)  x.TramiteUltimaOportunidad.RegionPrograma.Id))).ToList();

                if (Tramites == null) return null;
                Tramites = Tramites.OrderBy(x => x.TramiteUltimaOportunidad.RegionPrograma.Region).OrderBy(y => y.TramiteUltimaOportunidad.FechaRevisado).OrderBy(z => z.TramiteUltimaOportunidad.RegionPrograma.Programa).ToList();


                byte[] memoryStream = PDFReporteUltimaOportunidadRevisado.createPDF(Tramites);
                if (memoryStream != null)
                {
                    return File(memoryStream, "application/pdf", "Reporte_Ultima_Oportunidad_SEA.pdf");
                }
                else
                {
                    return Json(new { Data = "memoryStream is null" }, JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception ex)
            {
                return Json(new { Data = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}