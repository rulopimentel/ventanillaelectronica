﻿using PagedList;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using VentanillaElectronica.App_Code;
using VentanillaElectronica.Models;

namespace VentanillaElectronica.Controllers
{
    [Authorize(Roles = "ALUMNOS")]

    public class AlumnosController : Controller
    {
        VentanillaDBContext db = new VentanillaDBContext();

        [HttpGet]
        public ActionResult Header()
        {
            Usuario usuario = (Usuario)HttpContext.User.Identity;
            return View(usuario);
        }

        [HttpGet]
        public ActionResult Register(string Nombre, string Email)
        {
            VentanillaAlumno alumno = db.Alumnos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            if(alumno == null)
            {
                alumno = new VentanillaAlumno();
                alumno.UserName = User.Identity.Name.ToLower();
                alumno.Nombre = Nombre != null? Nombre.ToUpper() : "";
                alumno.Email = Email != null ? Email.ToLower() : "";  
                alumno.PrefijoInternacional = "+52";
            }
            return View(alumno);
        }


        [HttpGet]
        public ActionResult Dashboard(int? page)
        {
            VentanillaAlumno alumno = db.Alumnos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            alumno.Tramites = alumno.Tramites.OrderByDescending(x => x.FechaRegistro).ToList();
            return View(alumno);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(VentanillaAlumno ventanillaAlumno)
        {

            if (ventanillaAlumno != null && ModelState.IsValid)
            {
                VentanillaAlumno alumno = db.Alumnos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
                if (alumno == null)
                {
                    ventanillaAlumno.FechaActualizacion = DateTime.Now;
                    ventanillaAlumno.FechaRegistro = DateTime.Now;
                    ventanillaAlumno.PrefijoInternacional = "+52";
                  
                    db.Alumnos.Add(ventanillaAlumno);
                    db.SaveChanges();

                    MessagesTools message = new MessagesTools();
                    VentanillaTramite tramite = new VentanillaTramite();
                    tramite.Alumno = ventanillaAlumno;                
                    message.SendMailBienvenida(tramite);

                    return RedirectToAction("Dashboard", "Alumnos");

                }
                else
                {
                    return RedirectToAction("Edit", "Alumnos");
                }               
            }
            return View(ventanillaAlumno);
        }

        [HttpGet]
        public ActionResult Edit()
        {
            VentanillaDBContext db = new VentanillaDBContext();
            VentanillaAlumno alumno = db.Alumnos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            return View(alumno);
        }

        [HttpPost]
        public ActionResult Edit(VentanillaAlumno ventanillaAlumno)
        {
            if (ventanillaAlumno != null && ModelState.IsValid)
            {
                ventanillaAlumno.FechaActualizacion = DateTime.Now;
                db.Entry(ventanillaAlumno).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Dashboard", "Alumnos");
            }
            return View(ventanillaAlumno);
        }     


       

    }
}