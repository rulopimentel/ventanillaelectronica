﻿using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VentanillaElectronica.App_Code;
using VentanillaElectronica.Models;

namespace VentanillaElectronica.Controllers
{
    [Authorize(Roles = "ALUMNOS")]

    public class AlumnoTramiteUltimaOportunidadController : Controller
    {
        private VentanillaDBContext db = new VentanillaDBContext();

        //TODO Mostrar los pasos en la ventana de registrarse
        [NoCache]
        [HttpGet]
        public ActionResult Register()
        {

            ModelViewUltimaOportunidad ModelViewUltimaOportunidad = new ModelViewUltimaOportunidad();
            ModelViewUltimaOportunidad.TramiteUltimaOportunidad = new VentanillaTramitesUltimaOportunidad();
            ModelViewUltimaOportunidad.Alumno = db.Alumnos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            ModelViewUltimaOportunidad.RegionProgramas = db.RegionPrograma.ToList();
            return View(ModelViewUltimaOportunidad);
        }

        [HttpGet]
        public ActionResult Estatus()
        {
            ModelViewTramitesEscenarios escenario = (ModelViewTramitesEscenarios)TempData["escenario"];
            return View(escenario);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [NoCache]
        public ActionResult Register(ModelViewUltimaOportunidad modelo)
        {
            VentanillaAlumno alumno = db.Alumnos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            RegionPrograma RegionPrograma = db.RegionPrograma.Where(x => x.Id == modelo.TramiteUltimaOportunidad.RegionProgramaId).FirstOrDefault();

            ModelViewTramitesEscenarios escenario = new ModelViewTramitesEscenarios();

            try
            {

                VentanillaTramite Tramite = new VentanillaTramite();

                Tramite.VentanillaAlumnoId = alumno.Id;
                Tramite.Estatus = EnumTramitesEstatus.Solicitado;
                Tramite.FechaRegistro = DateTime.Now;
                Tramite.FechaActualizacion = DateTime.Now;
                Tramite.Programa = RegionPrograma.Programa;
                Tramite.Tipo = EnumTramitesTipo.UltimaOportunidad;

                Tramite.TramiteUltimaOportunidad = new VentanillaTramitesUltimaOportunidad();
                Tramite.TramiteUltimaOportunidad.PresentaInconveniente = modelo.TramiteUltimaOportunidad.PresentaInconveniente;
                Tramite.TramiteUltimaOportunidad.Docente = modelo.TramiteUltimaOportunidad.Docente;
                Tramite.TramiteUltimaOportunidad.ExperienciaEducativa = modelo.TramiteUltimaOportunidad.ExperienciaEducativa;
                Tramite.TramiteUltimaOportunidad.RegionProgramaId = RegionPrograma.Id;
                Tramite.TramiteUltimaOportunidad.TerminosYCondiciones1 = true;


                db.Tramites.Add(Tramite);
                db.SaveChanges();


                MessagesTools message = new MessagesTools();
                message.SendMailTramitUltimaOportunidad(Tramite);

                escenario.Id = Tramite.Id;
                escenario.Estatus = EnumTramitesEscenarios.RegistroExitoso;
                escenario.mensaje = "La solicitud se ha efectuado con éxito. El estatus de tu solicitud es <span class='label label-warning'>" + Tramite.Estatus + "</span>. Puedes consultar el estatus de tu trámite a través de tu correo electrónico y mediante esta plataforma.<br/><br/>";
                TempData["escenario"] = escenario;
                return RedirectToAction("Estatus", "AlumnoTramiteUltimaOportunidad");
            }
            catch (Exception ex)
            {
                escenario.Estatus = EnumTramitesEscenarios.Error;
                escenario.mensaje = "Hubo un error al procesar la solicitud.<br/>" + ex.Message.ToString();
                TempData["escenario"] = escenario;
                return RedirectToAction("Estatus", "AlumnoTramiteUltimaOportunidad");
            }


        }

        [HttpGet]
        public ActionResult View(int? Id)
        {
            if (Id == null) return RedirectToAction("Dashboard", "Alumnos");

            VentanillaAlumno alumno = db.Alumnos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            VentanillaTramite tramite = db.Tramites.Where(x => x.Id == Id && x.VentanillaAlumnoId == alumno.Id && x.Tipo == EnumTramitesTipo.UltimaOportunidad).FirstOrDefault();

            if (tramite != null)
            {
                return View(tramite);
            }
            else { return RedirectToAction("Dashboard", "Alumnos"); }
        }

        [HttpGet]
        public ActionResult Delete(int? Id)
        {
            if (Id == null) return RedirectToAction("Dashboard", "Alumnos");

            VentanillaAlumno alumno = db.Alumnos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            VentanillaTramite tramite = db.Tramites.Where(x => x.Id == Id && x.VentanillaAlumnoId == alumno.Id && x.Tipo == EnumTramitesTipo.UltimaOportunidad && x.Estatus == EnumTramitesEstatus.Solicitado).FirstOrDefault();

            if (tramite == null) return RedirectToAction("Dashboard", "Alumnos");
            try
            {
                db.TramitesUltimaOportunidad.Remove(tramite.TramiteUltimaOportunidad);
                db.Tramites.Remove(tramite);
                db.SaveChanges();

            }
            catch (Exception ex)
            {
                return RedirectToAction("Dashboard", "Alumnos");
            }
            return RedirectToAction("Dashboard", "Alumnos");
        }


        [HttpGet]
        public ActionResult Formato(int? Id)
        {
            if (Id == null) return null;

            VentanillaAlumno Alumno = db.Alumnos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            VentanillaTramite Tramite = db.Tramites.Where(x => x.Id == Id && x.VentanillaAlumnoId == Alumno.Id && x.Tipo == EnumTramitesTipo.UltimaOportunidad).FirstOrDefault();

            if (Tramite == null) return null;

            RegionPrograma RegionPrograma = db.RegionPrograma.Where(x => x.Id == Tramite.TramiteUltimaOportunidad.RegionProgramaId).FirstOrDefault();

            byte[] memoryStream = ItextPDFComponents.createPDF(Tramite);
            return memoryStream != null ? File(memoryStream, "application/pdf", "Solicitud_Última_Oportunidad_SEA.pdf") : null;



        }
    }
}
