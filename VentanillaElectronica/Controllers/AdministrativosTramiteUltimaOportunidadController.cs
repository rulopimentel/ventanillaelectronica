﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VentanillaElectronica.App_Code;
using VentanillaElectronica.Models;

namespace VentanillaElectronica.Controllers
{

    public class AdministrativosTramiteUltimaOportunidadController : Controller
    {
        private VentanillaDBContext db = new VentanillaDBContext();

        [NoCache]
        [HttpGet]
        [Authorize(Roles = "T400")]
        public ActionResult View(int? id)
        {
            if (id == null) return RedirectToAction("Dashboard", "Administrativos");

            VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            int[] Permisos = administrativo.Permisos.Where(z => z.Modulo.Clave.Equals("T407")).Select(x => (int)x.RegionPrograma.Id).ToArray();
            
            VentanillaTramite Tramite = db.Tramites.Where(x => x.Id == id && x.Tipo == EnumTramitesTipo.UltimaOportunidad && Permisos.Contains((int)x.TramiteUltimaOportunidad.RegionPrograma.Id).FirstOrDefault();
            if (Tramite == null) return RedirectToAction("Dashboard", "Administrativos");

            return View(Tramite);
        }

        [HttpGet]
        public ActionResult Formato(int? Id)
        {
            if (Id == null) return null;

            VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            int[] programas = administrativo.AdministrativosPrograma.Select(x => (int)x.Programa).ToArray();

            VentanillaTramite Tramite = db.Tramites.Where(x => x.Id == Id && programas.Contains((int)x.Programa) && x.Estatus == EnumTramitesEstatus.Solicitado && x.Tipo == EnumTramitesTipo.UltimaOportunidad).FirstOrDefault();
            if (Tramite == null) return RedirectToAction("Dashboard", "Administrativos");

            byte[] memoryStream = ItextPDFComponents.createPDF(Tramite);
            return memoryStream != null ? File(memoryStream, "application/pdf", "Solicitud_Última_Oportunidad_SEA" + Tramite.Alumno.UserName + ".pdf") : null;



        }

        [NoCache]
        [HttpGet]
        public ActionResult Mark(int? id)
        {
            if (id == null) return RedirectToAction("Dashboard", "Administrativos");

            VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            int[] programas = administrativo.AdministrativosPrograma.Select(x => (int)x.Programa).ToArray();

            VentanillaTramite Tramite = db.Tramites.Where(x => x.Id == id && programas.Contains((int)x.Programa) && (x.Estatus == EnumTramitesEstatus.Solicitado) && x.Tipo == EnumTramitesTipo.UltimaOportunidad).FirstOrDefault();
            if (Tramite == null) return RedirectToAction("Dashboard", "Administrativos");


            ModelViewCheckUltimaOportunidad ModelViewCheckUltimaOportunidad = new ModelViewCheckUltimaOportunidad();
            ModelViewCheckUltimaOportunidad.Tramite = Tramite;
            ModelViewCheckUltimaOportunidad.EstatusServe = EnumTramitesEstatusCheck.Revisado;
            ModelViewCheckUltimaOportunidad.Administrativo = administrativo;
            ModelViewCheckUltimaOportunidad.VentanillaAdministrativoId = administrativo.Id;


            return View(ModelViewCheckUltimaOportunidad);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [NoCache]
        public ActionResult Mark(int? id, ModelViewCheckUltimaOportunidad modelo, HttpPostedFileBase file)
        {
            if (id == null) return RedirectToAction("Dashboard", "Administrativos");

            VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            int[] programas = administrativo.AdministrativosPrograma.Select(x => (int)x.Programa).ToArray();

            VentanillaTramite Tramite = db.Tramites.Where(x => x.Id == id && programas.Contains((int)x.Programa) && (x.Estatus == EnumTramitesEstatus.Solicitado) && x.Tipo == EnumTramitesTipo.UltimaOportunidad).FirstOrDefault();
            if (Tramite == null) return RedirectToAction("Dashboard", "Administrativos");


            Tramite.TramiteUltimaOportunidad.Estatus = EnumTramitesUltimaOportunidadEstatus.Revisado;
            Tramite.VentanillaAdministrativoId = administrativo.Id;

            Tramite.TramiteUltimaOportunidad.FechaRevisado = DateTime.Now;
            Tramite.TramiteUltimaOportunidad.VentanillaAdministrativoRevisionId = administrativo.Id;



            Tramite.TramiteUltimaOportunidad.TerminosYCondiciones1 = true;



            try
            {
                string extension = Path.GetExtension(file.FileName);

                string name = "Cardex" + Tramite.TramiteUltimaOportunidad.RegionPrograma.Value + " " + Tramite.Alumno.UserName + "_" + String.Format("{0:ddMMyyyy}", DateTime.Now) + extension;
                var path = Path.Combine(Server.MapPath("~/Uploads/CardexUltimaOportunidad"), name);

                if (!Directory.Exists(Server.MapPath("~/Uploads/CardexUltimaOportunidad")))
                {

                    Directory.CreateDirectory(Server.MapPath("~/Uploads/CardexUltimaOportunidad"));

                }

                file.SaveAs(path);

                string URL = System.Configuration.ConfigurationManager.AppSettings["ServerApp"].ToString() + "Uploads/CardexUltimaOportunidad/" + name;
                Tramite.TramiteUltimaOportunidad.Url = URL;



                db.TramitesUltimaOportunidad.Attach(Tramite.TramiteUltimaOportunidad);

                var entry2 = db.Entry(Tramite.TramiteUltimaOportunidad);
                entry2.Property(e => e.Url).IsModified = true;
                entry2.Property(e => e.Estatus).IsModified = true;
                entry2.Property(e => e.FechaRevisado).IsModified = true;
                entry2.Property(e => e.VentanillaAdministrativoRevisionId).IsModified = true;

                db.SaveChanges();

                MessagesTools message = new MessagesTools();
                message.SendMailTramitUltimaOportunidadRevisado(Tramite);

                return RedirectToAction("View", "AdministrativosTramiteUltimaOportunidad", new { Id = Tramite.Id });
            }
            catch (Exception ex)
            {
                return RedirectToAction("Dashboard", "Administrativos");

            }




        }

        [NoCache]
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null) return RedirectToAction("Dashboard", "Administrativos");

            VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            int[] programas = administrativo.AdministrativosPrograma.Select(x => (int)x.Programa).ToArray();

            VentanillaTramite Tramite = db.Tramites.Where(x => x.Id == id && programas.Contains((int)x.Programa) && (x.Estatus == EnumTramitesEstatus.Solicitado) && x.Tipo == EnumTramitesTipo.UltimaOportunidad).FirstOrDefault();
            if (Tramite == null) return RedirectToAction("Dashboard", "Administrativos");


            Tramite.TramiteUltimaOportunidad.TerminosYCondiciones1 = true;
            ModelViewEditUltimaOportunidad ModelViewEditUltimaOportunidad = new ModelViewEditUltimaOportunidad();
            ModelViewEditUltimaOportunidad.Tramite = Tramite;
            ModelViewEditUltimaOportunidad.RegionProgramas = db.RegionPrograma.ToList();

            return View(ModelViewEditUltimaOportunidad);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [NoCache]
        public ActionResult Edit(int? Id, ModelViewEditUltimaOportunidad modelo)
        {

            if (Id == null) return RedirectToAction("Dashboard", "Administrativos");

            VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            int[] programas = administrativo.AdministrativosPrograma.Select(x => (int)x.Programa).ToArray();

            VentanillaTramite Tramite = db.Tramites.Where(x => x.Id == Id && programas.Contains((int)x.Programa) && (x.Estatus == EnumTramitesEstatus.Solicitado) && x.Tipo == EnumTramitesTipo.UltimaOportunidad).FirstOrDefault();
            if (Tramite == null) return RedirectToAction("Dashboard", "Administrativos");


            Tramite.TramiteUltimaOportunidad.ExperienciaEducativa = modelo.Tramite.TramiteUltimaOportunidad.ExperienciaEducativa;
            Tramite.TramiteUltimaOportunidad.Docente = modelo.Tramite.TramiteUltimaOportunidad.Docente;
            Tramite.TramiteUltimaOportunidad.RegionProgramaId = modelo.Tramite.TramiteUltimaOportunidad.RegionProgramaId;
            Tramite.TramiteUltimaOportunidad.PresentaInconveniente = modelo.Tramite.TramiteUltimaOportunidad.PresentaInconveniente;
            Tramite.TramiteUltimaOportunidad.TerminosYCondiciones1 = true;

            Tramite.FechaActualizacion = DateTime.Now;

            try
            {

                if (db.Academicos.Where(x => x.Nombre.Equals(modelo.Tramite.TramiteUltimaOportunidad.Docente)).ToList().Count == 0)
                {
                    db.Academicos.Add(new VentanillaAcademico() { Nombre = modelo.Tramite.TramiteUltimaOportunidad.Docente });
                }

             

                db.Tramites.Attach(Tramite);
                db.TramitesUltimaOportunidad.Attach(Tramite.TramiteUltimaOportunidad);


                var entry = db.Entry(Tramite);
                entry.Property(e => e.FechaActualizacion).IsModified = true;

                var entry2 = db.Entry(Tramite.TramiteUltimaOportunidad);
                entry2.Property(e => e.ExperienciaEducativa).IsModified = true;
                entry2.Property(e => e.Docente).IsModified = true;
                entry2.Property(e => e.RegionProgramaId).IsModified = true;
                entry2.Property(e => e.PresentaInconveniente).IsModified = true;

                db.SaveChanges();


                return RedirectToAction("View", "AdministrativosTramiteUltimaOportunidad", new { Id = Tramite.Id });
            }
            catch (Exception ex)
            {
                return RedirectToAction("Dashboard", "Administrativos");

            }


        }

        /// <summary>
        /// Acepta el trámite y notifica al Alumno
        /// </summary>

        [NoCache]
        [HttpGet]
        public ActionResult Serve(int? id)
        {
            if (id == null) return RedirectToAction("Dashboard", "Administrativos");

            VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            int[] programas = administrativo.AdministrativosPrograma.Select(x => (int)x.Programa).ToArray();

            VentanillaTramite Tramite = db.Tramites.Where(x => x.Id == id && programas.Contains((int)x.Programa) && (x.Estatus == EnumTramitesEstatus.Solicitado && x.TramiteUltimaOportunidad.Estatus == EnumTramitesUltimaOportunidadEstatus.DatosConsejoTecnico) && x.Tipo == EnumTramitesTipo.UltimaOportunidad).FirstOrDefault();
            if (Tramite == null) return RedirectToAction("Dashboard", "Administrativos");



            string observaciones = String.Format("El Consejo Técnico Avala la solicitud de tu trámite para presentar en carácter de Última Oportunidad la Experiencia Educativa {0}. El examen será aplicado el día {1} a las {2} por los docentes designados: {3}, {4}.", Tramite.TramiteUltimaOportunidad.ExperienciaEducativa, Tramite.TramiteUltimaOportunidad.FechaExamen.Value.ToLongDateString(), Tramite.TramiteUltimaOportunidad.FechaExamen.Value.ToLongTimeString(), Tramite.TramiteUltimaOportunidad.DocenteAplicacion1, Tramite.TramiteUltimaOportunidad.DocenteAplicacion2 );

            ModelViewServeUltimaOportunidad ModelViewServeUltimaOportunidad = new ModelViewServeUltimaOportunidad();
            ModelViewServeUltimaOportunidad.Tramite = Tramite;
            ModelViewServeUltimaOportunidad.EstatusServe = EnumTramitesEstatusServe.Aceptado;
            ModelViewServeUltimaOportunidad.Administrativo = administrativo;
            ModelViewServeUltimaOportunidad.VentanillaAdministrativoId = administrativo.Id;
            ModelViewServeUltimaOportunidad.Observaciones = observaciones;

            
            return View(ModelViewServeUltimaOportunidad);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [NoCache]
        public ActionResult Serve(int? id, ModelViewServeUltimaOportunidad modelo)
        {
            if (id == null) return RedirectToAction("Dashboard", "Administrativos");

            VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            int[] programas = administrativo.AdministrativosPrograma.Select(x => (int)x.Programa).ToArray();

            VentanillaTramite Tramite = db.Tramites.Where(x => x.Id == id && programas.Contains((int)x.Programa) && (x.Estatus == EnumTramitesEstatus.Solicitado && x.TramiteUltimaOportunidad.Estatus == EnumTramitesUltimaOportunidadEstatus.DatosConsejoTecnico) && x.Tipo == EnumTramitesTipo.UltimaOportunidad).FirstOrDefault();
            if (Tramite == null) return RedirectToAction("Dashboard", "Administrativos");


            Tramite.Estatus = EnumTramitesEstatus.Aceptado;
            Tramite.VentanillaAdministrativoId = administrativo.Id;
            Tramite.Observaciones = modelo.Observaciones;
            Tramite.FechaAtencion = DateTime.Now;
            Tramite.TramiteUltimaOportunidad.TerminosYCondiciones1 = true;



            try
            {
                //string extension = Path.GetExtension(file.FileName);

                //string name = "UltimaOportunidad" + Tramite.TramiteUltimaOportunidad.RegionPrograma.Value + " " + Tramite.Alumno.UserName + "_" + String.Format("{0:ddMMyyyy}", DateTime.Now) + extension;
                //var path = Path.Combine(Server.MapPath("~/Uploads/UltimaOportunidad"), name);

                //if (!Directory.Exists(Server.MapPath("~/Uploads/UltimaOportunidad")))
                //{

                //    Directory.CreateDirectory(Server.MapPath("~/Uploads/UltimaOportunidad"));

                //}

                //file.SaveAs(path);

                //string URL = System.Configuration.ConfigurationManager.AppSettings["ServerApp"].ToString() + "Uploads/UltimaOportunidad/" + name;
                //Tramite.TramiteUltimaOportunidad.Url = URL;


                db.Tramites.Attach(Tramite);

                var entry = db.Entry(Tramite);
                entry.Property(e => e.Estatus).IsModified = true;
                entry.Property(e => e.VentanillaAdministrativoId).IsModified = true;
                entry.Property(e => e.Observaciones).IsModified = true;
                entry.Property(e => e.FechaAtencion).IsModified = true;

               


                db.SaveChanges();

                MessagesTools message = new MessagesTools();
                message.SendMailTramitUltimaOportunidadAceptado(Tramite);

                return RedirectToAction("View", "AdministrativosTramiteUltimaOportunidad", new { Id = Tramite.Id });
            }
            catch (Exception ex)
            {
                return RedirectToAction("Dashboard", "Administrativos");

            }


        }

        [NoCache]
        [HttpGet]
        public ActionResult Serve1(int? id)
        {
            if (id == null) return RedirectToAction("Dashboard", "Administrativos");

            VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            int[] programas = administrativo.AdministrativosPrograma.Select(x => (int)x.Programa).ToArray();

            VentanillaTramite Tramite = db.Tramites.Where(x => x.Id == id && programas.Contains((int)x.Programa) && (x.Estatus == EnumTramitesEstatus.Solicitado) && x.Tipo == EnumTramitesTipo.UltimaOportunidad).FirstOrDefault();
            if (Tramite == null) return RedirectToAction("Dashboard", "Administrativos");


            ModelViewServe1UltimaOportunidad ModelViewServe1UltimaOportunidad = new ModelViewServe1UltimaOportunidad();
            ModelViewServe1UltimaOportunidad.Tramite = Tramite;
            ModelViewServe1UltimaOportunidad.EstatusServe = EnumTramitesEstatusServe.Denegado;
            ModelViewServe1UltimaOportunidad.Administrativo = administrativo;
            ModelViewServe1UltimaOportunidad.VentanillaAdministrativoId = administrativo.Id;


            return View(ModelViewServe1UltimaOportunidad);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [NoCache]
        public ActionResult Serve1(int? id, ModelViewServe1UltimaOportunidad modelo)
        {
            if (id == null) return RedirectToAction("Dashboard", "Administrativos");

            VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            int[] programas = administrativo.AdministrativosPrograma.Select(x => (int)x.Programa).ToArray();

            VentanillaTramite Tramite = db.Tramites.Where(x => x.Id == id && programas.Contains((int)x.Programa) && (x.Estatus == EnumTramitesEstatus.Solicitado) && x.Tipo == EnumTramitesTipo.UltimaOportunidad).FirstOrDefault();
            if (Tramite == null) return RedirectToAction("Dashboard", "Administrativos");


            Tramite.Estatus = EnumTramitesEstatus.Denegado;
            Tramite.VentanillaAdministrativoId = administrativo.Id;
            Tramite.Observaciones = modelo.Observaciones;
            Tramite.FechaAtencion = DateTime.Now;
            try
            {

                db.Tramites.Attach(Tramite);


                var entry = db.Entry(Tramite);
                entry.Property(e => e.Estatus).IsModified = true;
                entry.Property(e => e.VentanillaAdministrativoId).IsModified = true;
                entry.Property(e => e.Observaciones).IsModified = true;
                entry.Property(e => e.FechaAtencion).IsModified = true;

                db.SaveChanges();

                MessagesTools message = new MessagesTools();
                message.SendMailTramitUltimaOportunidadPreDenegado(Tramite);

                return RedirectToAction("View", "AdministrativosTramiteUltimaOportunidad", new { Id = Tramite.Id });
            }
            catch (Exception ex)
            {
                return RedirectToAction("Dashboard", "Administrativos");

            }


        }

        [NoCache]
        [HttpGet]
        public ActionResult Resolucion(int? id)
        {
            if (id == null) return RedirectToAction("Dashboard", "Administrativos");

            VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            int[] programas = administrativo.AdministrativosPrograma.Select(x => (int)x.Programa).ToArray();

            VentanillaTramite Tramite = db.Tramites.Where(x => x.Id == id && programas.Contains((int)x.Programa) && (x.Estatus == EnumTramitesEstatus.Solicitado && ( x.TramiteUltimaOportunidad.Estatus == EnumTramitesUltimaOportunidadEstatus.Revisado || x.TramiteUltimaOportunidad.Estatus == EnumTramitesUltimaOportunidadEstatus.DatosConsejoTecnico)) && x.Tipo == EnumTramitesTipo.UltimaOportunidad).FirstOrDefault();
            if (Tramite == null) return RedirectToAction("Dashboard", "Administrativos");

            ModelViewResolucionUltimaOportunidad ModelViewResolucionUltimaOportunidad = new ModelViewResolucionUltimaOportunidad();
            ModelViewResolucionUltimaOportunidad.FechaConsejoTecnico = Tramite.TramiteUltimaOportunidad.FechaConsejoTecnico == null? DateTime.Now.ToString("dd/MM/yyyy") : Tramite.TramiteUltimaOportunidad.FechaConsejoTecnico.Value.ToString("dd/MM/yyyy");
            ModelViewResolucionUltimaOportunidad.FechaExamen = Tramite.TramiteUltimaOportunidad.FechaExamen == null ? "" : Tramite.TramiteUltimaOportunidad.FechaExamen.Value.ToString("dd/MM/yyyy HH:mm");

            ModelViewResolucionUltimaOportunidad.Tramite = Tramite;
            ModelViewResolucionUltimaOportunidad.DocenteAplicacion1 = Tramite.TramiteUltimaOportunidad.DocenteAplicacion1 == null? Tramite.TramiteUltimaOportunidad.PresentaInconveniente? "" : Tramite.TramiteUltimaOportunidad.Docente : Tramite.TramiteUltimaOportunidad.DocenteAplicacion1;
            ModelViewResolucionUltimaOportunidad.DocenteAplicacion2 = Tramite.TramiteUltimaOportunidad.DocenteAplicacion2;



            return View(ModelViewResolucionUltimaOportunidad);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [NoCache]
        public ActionResult Resolucion(int? id, ModelViewResolucionUltimaOportunidad modelo)
        {
            if (id == null) return RedirectToAction("Dashboard", "Administrativos");

            VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            int[] programas = administrativo.AdministrativosPrograma.Select(x => (int)x.Programa).ToArray();

            VentanillaTramite Tramite = db.Tramites.Where(x => x.Id == id && programas.Contains((int)x.Programa) && (x.Estatus == EnumTramitesEstatus.Solicitado && (x.TramiteUltimaOportunidad.Estatus == EnumTramitesUltimaOportunidadEstatus.Revisado || x.TramiteUltimaOportunidad.Estatus == EnumTramitesUltimaOportunidadEstatus.DatosConsejoTecnico)) && x.Tipo == EnumTramitesTipo.UltimaOportunidad).FirstOrDefault();
            if (Tramite == null) return RedirectToAction("Dashboard", "Administrativos");


            Tramite.TramiteUltimaOportunidad.Estatus = EnumTramitesUltimaOportunidadEstatus.DatosConsejoTecnico;
            Tramite.TramiteUltimaOportunidad.VentanillaAdministrativoResolucionId = administrativo.Id;
            Tramite.TramiteUltimaOportunidad.DocenteAplicacion1 = modelo.DocenteAplicacion1;
            Tramite.TramiteUltimaOportunidad.DocenteAplicacion2 = modelo.DocenteAplicacion2;

            Tramite.TramiteUltimaOportunidad.FechaConsejoTecnico = DateTime.ParseExact(modelo.FechaConsejoTecnico, "dd/MM/yyyy",System.Globalization.CultureInfo.InvariantCulture); 
            Tramite.TramiteUltimaOportunidad.FechaExamen = DateTime.ParseExact(modelo.FechaExamen, "dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
            Tramite.TramiteUltimaOportunidad.FechaResolucion = DateTime.Now;
            Tramite.TramiteUltimaOportunidad.TerminosYCondiciones1 = true;

            try
            {

                if(db.Academicos.Where(x => x.Nombre.Equals(modelo.DocenteAplicacion1)).ToList().Count == 0)
                {
                    db.Academicos.Add(new VentanillaAcademico() { Nombre = modelo.DocenteAplicacion1 });
                }

                if (db.Academicos.Where(x => x.Nombre.Equals(modelo.DocenteAplicacion2)).ToList().Count == 0)
                {
                    db.Academicos.Add(new VentanillaAcademico() { Nombre = modelo.DocenteAplicacion2 });
                }

                db.TramitesUltimaOportunidad.Attach(Tramite.TramiteUltimaOportunidad);


                var entry = db.Entry(Tramite.TramiteUltimaOportunidad);
                entry.Property(e => e.Estatus).IsModified = true;
                entry.Property(e => e.VentanillaAdministrativoResolucionId).IsModified = true;
                entry.Property(e => e.DocenteAplicacion1).IsModified = true;
                entry.Property(e => e.DocenteAplicacion2).IsModified = true;
                entry.Property(e => e.FechaConsejoTecnico).IsModified = true;
                entry.Property(e => e.FechaExamen).IsModified = true;
                entry.Property(e => e.FechaResolucion).IsModified = true;


                db.SaveChanges();

              
                return RedirectToAction("View", "AdministrativosTramiteUltimaOportunidad", new { Id = Tramite.Id });
            }
            catch (Exception ex)
            {
                return RedirectToAction("Dashboard", "Administrativos");

            }
        }

        [HttpGet]
        public ActionResult OficiosTramiteUltimaOportunidad(int? id)
        {
            if (id == null) return null;

            try
            {
                VentanillaTramite Tramite = db.Tramites.Where(x =>  x.Id == id && (x.Estatus == EnumTramitesEstatus.Solicitado || x.Estatus == EnumTramitesEstatus.Aceptado) && x.TramiteUltimaOportunidad.Estatus == EnumTramitesUltimaOportunidadEstatus.DatosConsejoTecnico && x.Tipo == EnumTramitesTipo.UltimaOportunidad).FirstOrDefault();
                if (Tramite == null) return null;

                byte[] memoryStream = PDFOficioUltimaOportunidad.createPDF(Tramite);
                if (memoryStream != null)
                {
                    return File(memoryStream, "application/pdf", "Oficios_Examen_UltimaOportunidad" + Tramite.Id + ".pdf");
                }
                else
                {
                    return Json(new { Data = "memoryStream is null" }, JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception ex)
            {
                return Json(new { Data = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

    }
}
