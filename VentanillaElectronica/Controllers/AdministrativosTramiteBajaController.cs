﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using VentanillaElectronica.App_Code;
using VentanillaElectronica.Models;

namespace VentanillaElectronica.Controllers
{
    [Authorize(Roles = "ADMINISTRATIVOS")]

    public class AdministrativosTramiteBajaController : Controller
    {
        VentanillaDBContext db = new VentanillaDBContext();


        [HttpGet]
        public ActionResult View(int? Id)
        {
            if (Id == null) return RedirectToAction("Dashboard", "Administrativos");

            VentanillaTramite tramite = db.Tramites.Where(x => x.Id == Id).FirstOrDefault();

            if (tramite != null)
            {
                return View(tramite);
            }
            else { return RedirectToAction("Dashboard", "Administrativos"); }
        }

        [HttpGet]
        public ActionResult Serve(int? Id)
        {
            if (Id == null) return RedirectToAction("Dashboard", "Administrativos");

            VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            int[] programas = administrativo.AdministrativosPrograma.Select(x => (int) x.Programa).ToArray();

            VentanillaTramite tramite = db.Tramites.Where(x => x.Id == Id && programas.Contains((int) x.Programa)).FirstOrDefault();

            if (tramite != null)
            {
                if (tramite.Estatus != EnumTramitesEstatus.Solicitado) return RedirectToAction("View", "AdministrativosTramiteBaja", new { Id = Id });

                ModelViewServeTramiteBaja modelViewServeTramiteBaja = new ModelViewServeTramiteBaja();
                modelViewServeTramiteBaja.Administrativo = administrativo;
                modelViewServeTramiteBaja.Tramite = tramite;
                modelViewServeTramiteBaja.VentanillaTramiteId = tramite.Id;
                modelViewServeTramiteBaja.Estatus = (EnumTramitesEstatus)tramite.Estatus;
                modelViewServeTramiteBaja.FechaRegistro = tramite.FechaRegistro;
                modelViewServeTramiteBaja.VentanillaAdministrativoId = administrativo.Id;

                return View(modelViewServeTramiteBaja);
            }

            return RedirectToAction("Dashboard", "Administrativos");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [NoCache]
        public ActionResult Serve(ModelViewServeTramiteBaja serveTramite)
        {
            if (serveTramite != null && ModelState.IsValid)
            {

                VentanillaTramite tramite = db.Tramites.Where(x => x.Id == serveTramite.Id).FirstOrDefault();
                if (tramite == null) return RedirectToAction("Dashboard", "Administrativos");
                if (tramite.Estatus != EnumTramitesEstatus.Solicitado)
                {
                    return RedirectToAction("View", "AdministrativosTramiteBaja", new { Id = tramite.Id });
                }

                VentanillaAdministrativo administrativo = db.Administrativos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();


                tramite.Estatus = (EnumTramitesEstatus)serveTramite.EstatusServe;
                tramite.FechaActualizacion = DateTime.Now;
                tramite.FechaAtencion = DateTime.Now;
                tramite.Observaciones = serveTramite.Observaciones;
                tramite.VentanillaAdministrativoId = administrativo.Id;

                db.Entry(tramite).State = EntityState.Modified;
                db.SaveChanges();

                MessagesTools message = new MessagesTools();
                message.SendMailTramiteBajaServe(tramite);

                return RedirectToAction("View", "AdministrativosTramiteBaja", new { Id = tramite.Id });
            }
            return View(serveTramite);
        }





    }
}