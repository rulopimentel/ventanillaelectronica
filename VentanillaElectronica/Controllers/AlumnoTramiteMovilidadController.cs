﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VentanillaElectronica.App_Code;
using VentanillaElectronica.Models;

namespace VentanillaElectronica.Controllers
{
    [Authorize(Roles = "ALUMNOS")]

    public class AlumnoTramiteMovilidadController : Controller
    {
        VentanillaDBContext db = new VentanillaDBContext();

        [HttpGet]
        public ActionResult Index()
        {
            int periodoId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["IdPeriodoActivo"]);

            ModelViewInscripcionMovilidad modelViewInscripcionMovilidad = new ModelViewInscripcionMovilidad();
            modelViewInscripcionMovilidad.periodo = db.Periodos.Where(x => x.Id == periodoId).FirstOrDefault();
            modelViewInscripcionMovilidad.inscripcionMovilidad = db.InscripcionMovilidad.Where(x => x.VentanillaPeriodoId == modelViewInscripcionMovilidad.periodo.Id).ToList();
            modelViewInscripcionMovilidad.alumno = db.Alumnos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            return View(modelViewInscripcionMovilidad);
        }

        [HttpGet]
        public ActionResult View(int? Id)
        {
            if (Id == null) return RedirectToAction("Dashboard", "Alumnos");

            VentanillaAlumno alumno = db.Alumnos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            VentanillaTramite tramite = db.Tramites.Where(x => x.Id == Id && x.VentanillaAlumnoId == alumno.Id).FirstOrDefault();

            if (tramite != null)
            {
                return View(tramite);
            }
            else { return RedirectToAction("Dashboard", "Alumnos"); }
        }

        [HttpGet]
        public ActionResult Estatus()
        {
            ModelViewTramitesEscenarios escenario = (ModelViewTramitesEscenarios)TempData["escenario"];

            return View(escenario);
        }

        [NoCache]
        [HttpGet]
        public ActionResult Register(int? Id)
        {
            if (Id == null) return RedirectToAction("Index", "AlumnoTramiteMovilidad");

            VentanillaOfertaMovilidad ofertaMovilidad = db.OfertaMovilidad.Where(x => x.Id == Id).FirstOrDefault();

            if (ofertaMovilidad != null)
            {

                ModelViewTramitesEscenarios escenario = new ModelViewTramitesEscenarios();

                if (DateTime.Now >= ofertaMovilidad.InscripcionMovilidad.FechaInscripcionInicial && DateTime.Now < ofertaMovilidad.InscripcionMovilidad.FechaInscripcionFinal)
                {

                    VentanillaAlumno alumno = db.Alumnos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();

                    int periodoId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["IdPeriodoActivo"]);
                    int contador = alumno.Tramites.Where(x => x.Periodo.Id == periodoId && x.Tipo == EnumTramitesTipo.MovilidadEstudiantil).Count();
                    int contadorTramitesExistentes = db.TramitesMovilidad.Where(x => x.OfertaMovilidad.Id == ofertaMovilidad.Id).Count();

                    if (contador >= 3)
                    {
                        escenario.Estatus = EnumTramitesEscenarios.ItentosAgotados;
                        escenario.mensaje = "Estimado alumno, ya has agotado tus 3 trámites permitidos de movilidad para este periodo.";
                        TempData["escenario"] = escenario;
                        return RedirectToAction("Estatus", "AlumnoTramiteMovilidad");
                    }

                    if ((ofertaMovilidad.Cupo - contadorTramitesExistentes) <= 0)
                    {
                        escenario.Estatus = EnumTramitesEscenarios.CupoSaturado;
                        escenario.mensaje = "El cupo de la oferta a la que has intentado realizar el trámite, ya ha sido cubierto. Lamentamos las molestias. <br/><br/>" + ofertaMovilidad.InscripcionMovilidad.Programa + "<br/><strong>NRC(" + ofertaMovilidad.NRC + ")</strong> " + ofertaMovilidad.Nombre;
                        TempData["escenario"] = escenario;
                        return RedirectToAction("Estatus", "AlumnoTramiteMovilidad");
                    }

                    if (db.TramitesMovilidad.Where(x => x.Tramite.VentanillaAlumnoId == alumno.Id && x.VentanillaOfertaMovilidadId == ofertaMovilidad.Id).FirstOrDefault() != null)
                    {
                        escenario.Estatus = EnumTramitesEscenarios.RegistroExitente;
                        escenario.mensaje = "El sistema ha detectado que ya has solicitado previamente este trámite. Puedes consultar el seguimiento de tu trámite a través de tu correo electrónico y mediante esta plataforma.<br/><br/>" + ofertaMovilidad.InscripcionMovilidad.Programa + "<br/><strong>NRC(" + ofertaMovilidad.NRC + ")</strong> " + ofertaMovilidad.Nombre;
                        TempData["escenario"] = escenario;
                        return RedirectToAction("Estatus", "AlumnoTramiteMovilidad");
                    }

                    ModelViewTramitesMovilidad modelViewTramitesMovilidad = new ModelViewTramitesMovilidad();
                    VentanillaTramite tramite = new VentanillaTramite();
                    tramite.VentanillaAlumnoId = alumno.Id;
                    tramite.TramiteMovilidad = new VentanillaTramitesMovilidad();

                    tramite.Tipo = EnumTramitesTipo.MovilidadEstudiantil;
                    tramite.Programa = ofertaMovilidad.InscripcionMovilidad.Programa;

                    tramite.TramiteMovilidad.VentanillaOfertaMovilidadId = ofertaMovilidad.Id;
                    tramite.TramiteMovilidad.OfertaMovilidad = ofertaMovilidad;
                    modelViewTramitesMovilidad.periodo = db.Periodos.Where(x => x.Id == ofertaMovilidad.InscripcionMovilidad.Periodo.Id).FirstOrDefault();
                    modelViewTramitesMovilidad.tramite = tramite;
                    modelViewTramitesMovilidad.alumno = alumno;
                    modelViewTramitesMovilidad.inscripcionMovilidad = ofertaMovilidad.InscripcionMovilidad;
                    return View(modelViewTramitesMovilidad);
                }
                else
                {
                    escenario.Estatus = EnumTramitesEscenarios.PeriodoNoDisponible;
                    escenario.mensaje = "Las fechas para la realización del trámite no están disponibles. Por favor atiende a las fechas disponibles del programa de estudios.<br/><br/>" + ofertaMovilidad.InscripcionMovilidad.Programa + "<br/><strong>NRC(" + ofertaMovilidad.NRC + ")</strong> " + ofertaMovilidad.Nombre + "<br/>" + ofertaMovilidad.InscripcionMovilidad.FechaInscripcionInicial + " al " + ofertaMovilidad.InscripcionMovilidad.FechaInscripcionFinal;
                    TempData["escenario"] = escenario;
                    return RedirectToAction("Estatus", "AlumnoTramiteMovilidad");
                }
            }
            else { return RedirectToAction("Index", "AlumnoTramiteMovilidad"); }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [NoCache]
        public ActionResult Register(int? Id, HttpPostedFileBase file)
        {
            if (Id == null) return RedirectToAction("Index", "AlumnoTramiteMovilidad");

            VentanillaOfertaMovilidad ofertaMovilidad = db.OfertaMovilidad.Where(x => x.Id == Id).FirstOrDefault();
            VentanillaAlumno alumno = db.Alumnos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            ModelViewTramitesEscenarios escenario = new ModelViewTramitesEscenarios();


            if (ofertaMovilidad != null)
            {
                try
                {
                    int periodoId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["IdPeriodoActivo"]);
                    VentanillaPeriodo periodo = db.Periodos.Where(x => x.Id == periodoId).FirstOrDefault();

                    int contador = alumno.Tramites.Where(x => x.Periodo.Id == periodoId && x.Tipo == EnumTramitesTipo.MovilidadEstudiantil).Count();
                    int contadorTramitesExistentes = db.TramitesMovilidad.Where(x => x.OfertaMovilidad.Id == ofertaMovilidad.Id).Count();

                    if (contador >= 3)
                    {
                        escenario.Estatus = EnumTramitesEscenarios.ItentosAgotados;
                        escenario.mensaje = "Estimado alumno, ya has agotado tus 3 trámites permitidos de movilidad para este periodo.";
                        TempData["escenario"] = escenario;
                        return RedirectToAction("Estatus", "AlumnoTramiteMovilidad");
                    }
                    if (DateTime.Now >= ofertaMovilidad.InscripcionMovilidad.FechaInscripcionInicial && DateTime.Now < ofertaMovilidad.InscripcionMovilidad.FechaInscripcionFinal)
                    {

                        string extension = Path.GetExtension(file.FileName);


                        ofertaMovilidad.NRC =  ofertaMovilidad.NRC.Replace('/', '-');

                        string name = ofertaMovilidad.InscripcionMovilidad.Programa + "_NRC" + ofertaMovilidad.NRC + "_" + alumno.UserName + "_" + String.Format("{0:ddMMyyyy}", DateTime.Now) + extension;
                        var path = Path.Combine(Server.MapPath("~/Uploads/Movilidad"), name);
                        file.SaveAs(path);

                        string URL = System.Configuration.ConfigurationManager.AppSettings["ServerApp"].ToString() + "Uploads/Movilidad/" + name;


                        VentanillaTramite tramite = new VentanillaTramite();
                        tramite.VentanillaAlumnoId = alumno.Id;
                        tramite.Estatus = EnumTramitesEstatus.Solicitado;
                        tramite.FechaRegistro = DateTime.Now;
                        tramite.FechaActualizacion = DateTime.Now;
                        tramite.Programa = ofertaMovilidad.InscripcionMovilidad.Programa;
                        tramite.Periodo = periodo;
                        tramite.VentanillaPeriodoId = ofertaMovilidad.InscripcionMovilidad.Periodo.Id;

                        tramite.Tipo = EnumTramitesTipo.MovilidadEstudiantil;
                        tramite.TramiteMovilidad = new VentanillaTramitesMovilidad();
                        tramite.TramiteMovilidad.Url = URL;
                        tramite.TramiteMovilidad.VentanillaOfertaMovilidadId = ofertaMovilidad.Id;


                        if ((ofertaMovilidad.Cupo - contadorTramitesExistentes) <= 0)
                        {
                            escenario.Estatus = EnumTramitesEscenarios.CupoSaturado;
                            escenario.mensaje = "El cupo de la oferta a la que has intentado realizar el trámite, ya ha sido cubierto. Lamentamos las molestias. <br/><br/>" + ofertaMovilidad.InscripcionMovilidad.Programa + "<br/><strong>NRC(" + ofertaMovilidad.NRC + ")</strong> " + ofertaMovilidad.Nombre;
                            TempData["escenario"] = escenario;
                            return RedirectToAction("Estatus", "AlumnoTramiteMovilidad");
                        }

                        if (db.Tramites.Where(x => x.VentanillaAlumnoId == alumno.Id && x.TramiteMovilidad.VentanillaOfertaMovilidadId == ofertaMovilidad.Id).FirstOrDefault() != null)
                        {
                            escenario.Estatus = EnumTramitesEscenarios.RegistroExitente;
                            escenario.mensaje = "El sistema ha detectado que ya has solicitado previamente este trámite. Puedes consultar el seguimiento de tu trámite a través de tu correo electrónico y mediante esta plataforma.<br/><br/>" + ofertaMovilidad.InscripcionMovilidad.Programa + "<br/><strong>NRC(" + ofertaMovilidad.NRC + ")</strong> " + ofertaMovilidad.Nombre;
                            TempData["escenario"] = escenario;
                            return RedirectToAction("Estatus", "AlumnoTramiteMovilidad");
                        }

                        db.Tramites.Add(tramite);
                        db.SaveChangesAsync();

                     
                        MessagesTools message = new MessagesTools();
                        message.SendMailTramitemovilidad(tramite);

                        escenario.Estatus = EnumTramitesEscenarios.RegistroExitoso;
                        escenario.mensaje = "La solicitud se ha efectuado con éxito. El estatus de tu solicitud es <span class='label label-warning'>" + tramite.Estatus + "</span>. Puedes consultar el estatus de tu trámite a través de tu correo electrónico y mediante esta plataforma.<br/><br/>" + ofertaMovilidad.InscripcionMovilidad.Programa + "<br/><strong>NRC(" + ofertaMovilidad.NRC + ")</strong> " + ofertaMovilidad.Nombre;
                        TempData["escenario"] = escenario;
                        return RedirectToAction("Estatus", "AlumnoTramiteMovilidad");
                    }
                    else
                    {
                        escenario.Estatus = EnumTramitesEscenarios.PeriodoNoDisponible;
                        escenario.mensaje = "Las fechas para la realización del trámite no están disponibles. Por favor atiende a las fechas disponibles del programa de estudios.<br/><br/>" + ofertaMovilidad.InscripcionMovilidad.Programa + "<br/><strong>NRC(" + ofertaMovilidad.NRC + ")</strong> " + ofertaMovilidad.Nombre + "<br/>" + ofertaMovilidad.InscripcionMovilidad.FechaInscripcionInicial + " al " + ofertaMovilidad.InscripcionMovilidad.FechaInscripcionFinal;
                        TempData["escenario"] = escenario;
                        return RedirectToAction("Estatus", "AlumnoTramiteMovilidad");
                    }
                }
                catch (Exception ex)
                {
                    escenario.Estatus = EnumTramitesEscenarios.Error;
                    escenario.mensaje = "Hubo un error al procesar la solicitud.<br/>" + ex.Message.ToString();
                    TempData["escenario"] = escenario;
                    return RedirectToAction("Estatus", "AlumnoTramiteMovilidad");
                }

            }




            return RedirectToAction("Index", "AlumnoTramiteMovilidad");
        }


    }



}