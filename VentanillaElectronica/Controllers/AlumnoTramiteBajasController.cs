﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VentanillaElectronica.App_Code;
using VentanillaElectronica.Models;

namespace VentanillaElectronica.Controllers
{
    [Authorize(Roles = "ALUMNOS")]

    public class AlumnoTramiteBajasController : Controller
    {
        VentanillaDBContext db = new VentanillaDBContext();

        [NoCache]
        [HttpGet]
        public ActionResult Register()
        {
            int periodoId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["IdPeriodoActivo"]);
            VentanillaInscripcionBajas inscripcionBajas = db.InscripcionBajas.Where(x => x.VentanillaPeriodoId == periodoId).FirstOrDefault();

            ModelViewTramitesEscenarios escenario = new ModelViewTramitesEscenarios();

            if (inscripcionBajas != null && (DateTime.Now >= inscripcionBajas.FechaInscripcionInicial && DateTime.Now < inscripcionBajas.FechaInscripcionFinal))
            {
                ModelViewInscripcionBajas modelViewInscripcionBajas = new ModelViewInscripcionBajas();
                modelViewInscripcionBajas.periodo = db.Periodos.Where(x => x.Id == periodoId).FirstOrDefault();
                modelViewInscripcionBajas.alumno = db.Alumnos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
                modelViewInscripcionBajas.tramiteBaja = new VentanillaTramitesBajas();
                return View(modelViewInscripcionBajas);
            }
            else
            {
                escenario.Estatus = EnumTramitesEscenarios.PeriodoNoDisponible;
                escenario.mensaje = "Las fechas para la realización del trámite no están disponibles. Por favor atiende a las fechas disponibles<br/><br/>" + inscripcionBajas.FechaInscripcionInicial + " al " + inscripcionBajas.FechaInscripcionFinal;
                TempData["escenario"] = escenario;
                return RedirectToAction("Estatus", "AlumnoTramiteBajas");
            }
        }

        [HttpGet]
        public ActionResult Estatus()
        {
            ModelViewTramitesEscenarios escenario = (ModelViewTramitesEscenarios)TempData["escenario"];

            return View(escenario);
        }

        [HttpGet]
        public ActionResult View(int? Id)
        {
            if (Id == null) return RedirectToAction("Dashboard", "Alumnos");

            VentanillaAlumno alumno = db.Alumnos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            VentanillaTramite tramite = db.Tramites.Where(x => x.Id == Id && x.VentanillaAlumnoId == alumno.Id && x.Tipo == EnumTramitesTipo.Baja).FirstOrDefault();

            if (tramite != null)
            {
                return View(tramite);
            }
            else { return RedirectToAction("Dashboard", "Alumnos"); }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [NoCache]
        public ActionResult Register(ModelViewInscripcionBajas inscripcionBaja)
        {
            if (inscripcionBaja.tramiteBaja != null)
            {
                int periodoId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["IdPeriodoActivo"]);
                VentanillaPeriodo periodo = db.Periodos.Where(x => x.Id == periodoId).FirstOrDefault();
                VentanillaInscripcionBajas inscripcionBajas = db.InscripcionBajas.Where(x => x.VentanillaPeriodoId == periodoId).FirstOrDefault();
                ModelViewTramitesEscenarios escenario = new ModelViewTramitesEscenarios();

                if (inscripcionBajas != null && (DateTime.Now >= inscripcionBajas.FechaInscripcionInicial && DateTime.Now < inscripcionBajas.FechaInscripcionFinal))
                {
                    VentanillaAlumno alumno = db.Alumnos.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
                    VentanillaTramite tramite = new VentanillaTramite();
                    tramite.Periodo = periodo;
                    tramite.VentanillaAlumnoId = alumno.Id;
                    tramite.VentanillaPeriodoId = periodoId;
                    tramite.Programa = alumno.Programa;
                    tramite.Estatus = EnumTramitesEstatus.Solicitado;
                    tramite.Tipo = EnumTramitesTipo.Baja;
                    tramite.FechaRegistro = DateTime.Now;
                    tramite.FechaActualizacion = DateTime.Now;
                    tramite.TramiteBaja = new VentanillaTramitesBajas();
                    tramite.TramiteBaja.NRC = inscripcionBaja.tramiteBaja.NRC;
                    tramite.TramiteBaja.ExperienciaEducativa = inscripcionBaja.tramiteBaja.ExperienciaEducativa;
                    tramite.TramiteBaja.BloqueTurno = inscripcionBaja.tramiteBaja.BloqueTurno;               

                    db.Tramites.Add(tramite);
                    db.SaveChangesAsync();

                    MessagesTools message = new MessagesTools();
                    message.SendMailTramiteBaja(tramite);

                    escenario.Estatus = EnumTramitesEscenarios.RegistroExitoso;
                    escenario.mensaje = "La solicitud se ha efectuado con éxito. El estatus de tu solicitud es <span class='label label-warning'>" + tramite.Estatus + "</span>. Puedes consultar el estatus de tu trámite a través de tu correo electrónico y mediante esta plataforma.<br/><br/><strong>NRC(" + tramite.TramiteBaja.NRC + ")</strong> " + tramite.TramiteBaja.ExperienciaEducativa + "<br/>Bloque/turno: " +  tramite.TramiteBaja.BloqueTurno;
                    TempData["escenario"] = escenario;
                    return RedirectToAction("Estatus", "AlumnoTramiteBajas");

                }
                else
                {
                    escenario.Estatus = EnumTramitesEscenarios.PeriodoNoDisponible;
                    escenario.mensaje = "Las fechas para la realización del trámite no están disponibles. Por favor atiende a las fechas disponibles<br/><br/>" + inscripcionBajas.FechaInscripcionInicial + " al " + inscripcionBajas.FechaInscripcionFinal;
                    TempData["escenario"] = escenario;
                    return RedirectToAction("Estatus", "AlumnoTramiteBajas");
                }
            }
            return RedirectToAction("Dashboard", "Alumnos");
        }

    }



}