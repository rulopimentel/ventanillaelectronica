﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using VentanillaElectronica.App_Code;
using VentanillaElectronica.Models;

namespace VentanillaElectronica.Controllers
{
    public class HomeController : Controller
    {
        VentanillaDBContext db = new VentanillaDBContext();


        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ModelViewLogin usuario, string returnUrl = "")
        {

            if (usuario != null && ModelState.IsValid)
            {
                CustomMembershipProvider provider = new CustomMembershipProvider();
                Usuario activeDirectoryUsuario = provider.ValidaUsuario(usuario.Username, usuario.Password);

                if (activeDirectoryUsuario != null)
                {


                    if (!string.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
                        Response.Redirect(FormsAuthentication.GetRedirectUrl(activeDirectoryUsuario.UserName, false));
                    else
                    {
                        FormsAuthentication.RedirectFromLoginPage(activeDirectoryUsuario.UserName, false);
                        if(activeDirectoryUsuario.IsAlumno && activeDirectoryUsuario.Exist == false) return RedirectToAction("Register", "Alumnos", new { Email = activeDirectoryUsuario.Email, Nombre = activeDirectoryUsuario.Nombre });
                        if (activeDirectoryUsuario.IsAlumno && activeDirectoryUsuario.Exist == true) return RedirectToAction("Dashboard", "Alumnos");
                        if (!activeDirectoryUsuario.IsAlumno) return RedirectToAction("Dashboard", "Administrativos");
                    }
                }
                
                ViewBag.Message = "Usuario o contraseña incorrecta";
            }
            return View(usuario);
        }


        [HttpGet]
        [Authorize]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }

    }
}