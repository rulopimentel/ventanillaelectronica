namespace VentanillaElectronica.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using VentanillaElectronica.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<VentanillaElectronica.Models.VentanillaDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(VentanillaElectronica.Models.VentanillaDBContext context)
        {
            AddRegionPrograma(context);
            AddUsuarios(context);
            AddUsuariosProgramas(context);
            AddPeriodos(context);
            AddExperienciasEducativas(context);
            AddModulos(context);
            AddPermisos(context);

            //AddInscripcionMovilidad(context);
            //AddInscripcionAltas(context);
            //AddInscripcionBajas(context);
        }

        void AddPermisos(VentanillaElectronica.Models.VentanillaDBContext context)
        {
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 1, VentanillaModuloId = 9, RegionProgramaId=1 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 1, VentanillaModuloId = 9, RegionProgramaId = 2 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 1, VentanillaModuloId = 9, RegionProgramaId = 3 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 1, VentanillaModuloId = 9, RegionProgramaId = 4 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 1, VentanillaModuloId = 9, RegionProgramaId = 5 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 1, VentanillaModuloId = 9, RegionProgramaId = 6 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 1, VentanillaModuloId = 9, RegionProgramaId = 7 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 1, VentanillaModuloId = 9, RegionProgramaId = 8 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 1, VentanillaModuloId = 9, RegionProgramaId = 9 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 1, VentanillaModuloId = 9, RegionProgramaId = 10 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 1, VentanillaModuloId = 9, RegionProgramaId = 11 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 1, VentanillaModuloId = 9, RegionProgramaId = 12 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 1, VentanillaModuloId = 9, RegionProgramaId = 13 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 1, VentanillaModuloId = 9, RegionProgramaId = 14 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 1, VentanillaModuloId = 9, RegionProgramaId = 15 });

            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 2, VentanillaModuloId = 9, RegionProgramaId = 1 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 2, VentanillaModuloId = 9, RegionProgramaId = 2 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 2, VentanillaModuloId = 9, RegionProgramaId = 3 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 2, VentanillaModuloId = 9, RegionProgramaId = 4 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 2, VentanillaModuloId = 9, RegionProgramaId = 5 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 2, VentanillaModuloId = 9, RegionProgramaId = 6 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 2, VentanillaModuloId = 9, RegionProgramaId = 7 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 2, VentanillaModuloId = 9, RegionProgramaId = 8 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 2, VentanillaModuloId = 9, RegionProgramaId = 9 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 2, VentanillaModuloId = 9, RegionProgramaId = 10 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 2, VentanillaModuloId = 9, RegionProgramaId = 11 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 2, VentanillaModuloId = 9, RegionProgramaId = 12 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 2, VentanillaModuloId = 9, RegionProgramaId = 13 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 2, VentanillaModuloId = 9, RegionProgramaId = 14 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 2, VentanillaModuloId = 9, RegionProgramaId = 15 });

            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 3, VentanillaModuloId = 9, RegionProgramaId = 1 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 3, VentanillaModuloId = 9, RegionProgramaId = 2 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 3, VentanillaModuloId = 9, RegionProgramaId = 3 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 3, VentanillaModuloId = 9, RegionProgramaId = 4 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 3, VentanillaModuloId = 9, RegionProgramaId = 5 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 3, VentanillaModuloId = 9, RegionProgramaId = 6 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 3, VentanillaModuloId = 9, RegionProgramaId = 7 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 3, VentanillaModuloId = 9, RegionProgramaId = 8 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 3, VentanillaModuloId = 9, RegionProgramaId = 9 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 3, VentanillaModuloId = 9, RegionProgramaId = 10 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 3, VentanillaModuloId = 9, RegionProgramaId = 11 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 3, VentanillaModuloId = 9, RegionProgramaId = 12 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 3, VentanillaModuloId = 9, RegionProgramaId = 13 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 3, VentanillaModuloId = 9, RegionProgramaId = 14 });
            context.Permisos.AddOrUpdate(new VentanillaPermisos() { FechaRegistro = DateTime.Now, VentanillaAdministrativoId = 3, VentanillaModuloId = 9, RegionProgramaId = 15 });

        }

        void AddModulos(VentanillaElectronica.Models.VentanillaDBContext context)
        {
            //�ltima oportunidad
            context.Modulos.AddOrUpdate(new VentanillaModulo() { Id = 1, Clave = "T400", Descripcion = "Ver tr�mite del estudiante.", FechaRegistro = DateTime.Now });
            context.Modulos.AddOrUpdate(new VentanillaModulo() { Id = 2, Clave = "T401", Descripcion = "Editar detalles del tr�mite del estudiante.", FechaRegistro = DateTime.Now });
            context.Modulos.AddOrUpdate(new VentanillaModulo() { Id = 3, Clave = "T402", Descripcion = "Marcar como revisado para consejo t�cnico.", FechaRegistro = DateTime.Now });
            context.Modulos.AddOrUpdate(new VentanillaModulo() { Id = 4, Clave = "T403", Descripcion = "Denegar el tr�mite del estudiante.", FechaRegistro = DateTime.Now });
            context.Modulos.AddOrUpdate(new VentanillaModulo() { Id = 5, Clave = "T404", Descripcion = "Editar datos del Consejo T�cnico.", FechaRegistro = DateTime.Now });
            context.Modulos.AddOrUpdate(new VentanillaModulo() { Id = 6, Clave = "T405", Descripcion = "Generar y descargar oficios para los docentes.", FechaRegistro = DateTime.Now });
            context.Modulos.AddOrUpdate(new VentanillaModulo() { Id = 7, Clave = "T406", Descripcion = "Aceptar tr�mite y notificar al estudiante.", FechaRegistro = DateTime.Now });
            context.Modulos.AddOrUpdate(new VentanillaModulo() { Id = 8, Clave = "T407", Descripcion = "Generar reporte de solicitudes para Consejo T�cnico.", FechaRegistro = DateTime.Now });

            context.Modulos.AddOrUpdate(new VentanillaModulo() { Id = 9, Clave = "ADMINISTRADORES", Descripcion = "Permiso de administrador", FechaRegistro = DateTime.Now });

            context.SaveChanges();
        }

            void AddExperienciasEducativas(VentanillaElectronica.Models.VentanillaDBContext context)
        {
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 1, Nombre = "ACCIONES DE VINCULACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 2, Nombre = "ACTOS DE COMERCIO Y SOCIEDADES MERCANTILES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 3, Nombre = "ADMINISTRACION CORPORATIVA DEL APRENDIZAJE" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 4, Nombre = "ADMINISTRACION DE LA CALIDAD" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 5, Nombre = "ADMINISTRACION DE LA PRODUCCION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 6, Nombre = "ADMINISTRACION DE LAS COMPENSACIONES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 7, Nombre = "ADMINISTRACION DE LAS OPERACIONES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 8, Nombre = "ADMINISTRACION DE LAS ORGANIZACIONES TURISTICAS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 9, Nombre = "ADMINISTRACION DE RECURSOS HUMANOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 10, Nombre = "ADMINISTRACION DE VENTAS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 11, Nombre = "ADMINISTRACION EDUCATIVA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 12, Nombre = "ADMINISTRACION ESTRATEGICA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 13, Nombre = "ADMINISTRACION FINANCIERA ESTRATEGICA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 14, Nombre = "ADMINISTRACION MUNICIPAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 15, Nombre = "ADMINISTRACION POR CALIDAD" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 16, Nombre = "ADMINISTRACION PUBLICA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 17, Nombre = "ANALISIS DE LA PRACTICA DOCENTE" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 18, Nombre = "ANALISIS DE RIESGO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 19, Nombre = "ANALISIS DEMOGRAFICO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 20, Nombre = "ANALISIS MULTIDIMENSIONAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 21, Nombre = "APLICACIONES TECNOLOGICAS CONTABLES Y FISCALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 22, Nombre = "ATENCION A TRAYECTORIAS ACADEMICAS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 23, Nombre = "AUDITORIA EN SEGURIDAD SOCIAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 24, Nombre = "AUDITORIA GUBERNAMENTAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 25, Nombre = "AUDITORIA I" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 26, Nombre = "AUDITORIA II" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 27, Nombre = "AUDITORIA OPERACIONAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 28, Nombre = "AUDITORIA PARA EFECTOS DEL IMSS E INFONAVIT" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 29, Nombre = "AUDITORIA PARA EFECTOS FISCALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 30, Nombre = "AUDITORIA Y CERTIFICACION DE LA CALIDAD" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 31, Nombre = "AUDITORIA Y CONSULTORIA ADMINISTRATIVA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 32, Nombre = "CALIDAD TOTAL DE LA EMPRESA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 33, Nombre = "COMERCIO ELECTRONICO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 34, Nombre = "COMERCIO EXTERIOR" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 35, Nombre = "COMPORTAMIENTO ORGANIZACIONAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 36, Nombre = "COMPUTACION BASICA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 37, Nombre = "COMPUTO PARA ESTADISTICA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 38, Nombre = "COMUNICACION AUDIOVISUAL EDUCATIVA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 39, Nombre = "COMUNICACION Y EDUCACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 40, Nombre = "CONSTRUCCION DE DISE�OS DE INVESTIGACION SOCIOLOGICA I" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 41, Nombre = "CONSTRUCCION DE DISE�OS DE INVESTIGACION SOCIOLOGICA II" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 42, Nombre = "CONTABILIDAD ADMINISTRATIVA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 43, Nombre = "CONTABILIDAD CONTEMPORANEA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 44, Nombre = "CONTABILIDAD DE COSTOS I" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 45, Nombre = "CONTABILIDAD DE COSTOS II" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 46, Nombre = "CONTABILIDAD DE COSTOS POR ORDENES ESPECIFICAS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 47, Nombre = "CONTABILIDAD DE COSTOS POR PROCESOS Y PREDETERMINADOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 48, Nombre = "CONTABILIDAD DE SOCIEDADES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 49, Nombre = "CONTABILIDAD EMPRESARIAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 50, Nombre = "CONTABILIDAD EMPRESARIAL POR EXPANSION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 51, Nombre = "CONTABILIDAD FINANCIERA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 52, Nombre = "CONTABILIDAD GUBERNAMENTAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 53, Nombre = "CONTABILIDAD INTERMEDIA I" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 54, Nombre = "CONTABILIDAD INTERMEDIA II" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 55, Nombre = "CONTABILIDAD INTERNACIONAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 56, Nombre = "CONTABILIDAD PARA EL DESARROLLO SUSTENTABLE" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 57, Nombre = "CONTABILIDADES ESPECIALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 58, Nombre = "CONTROL Y FISCALIZACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 59, Nombre = "CORRIENTES PEDAGOGICAS CONTEMPORANEAS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 60, Nombre = "COSTOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 61, Nombre = "CREACION DE EMPRESAS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 62, Nombre = "DATAWAREHOUSING" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 63, Nombre = "DEBATE SOCIOLOGICO CONTEMPORANEO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 64, Nombre = "DERECHO ADMINISTRATIVO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 65, Nombre = "DERECHO AGRARIO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 66, Nombre = "DERECHO CIVIL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 67, Nombre = "DERECHO CONSTITUCIONAL MEXICANO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 68, Nombre = "DERECHO CONSTITUCIONAL Y ADMINISTRATIVO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 69, Nombre = "DERECHO CORPORATIVO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 70, Nombre = "DERECHO DE BIENES Y SUCESIONES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 71, Nombre = "DERECHO DE LA SEGURIDAD SOCIAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 72, Nombre = "DERECHO DE LAS OBLIGACIONES CIVILES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 73, Nombre = "DERECHO DE LOS CONTRATOS CIVILES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 74, Nombre = "DERECHO DE PERSONAS Y FAMILIA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 75, Nombre = "DERECHO FISCAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 76, Nombre = "DERECHO FISCAL TRIBUTARIO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 77, Nombre = "DERECHO INTERNACIONAL PRIVADO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 78, Nombre = "DERECHO INTERNACIONAL PUBLICO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 79, Nombre = "DERECHO LABORAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 80, Nombre = "DERECHO MERCANTIL I" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 81, Nombre = "DERECHO MERCANTIL II" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 82, Nombre = "DERECHO PENAL ESPECIAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 83, Nombre = "DERECHO PENAL GENERAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 84, Nombre = "DERECHO PROCESAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 85, Nombre = "DERECHO PROCESAL ADMINISTRATIVO/FISCAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 86, Nombre = "DERECHO PROCESAL CIVIL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 87, Nombre = "DERECHO PROCESAL LABORAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 88, Nombre = "DERECHO PROCESAL MERCANTIL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 89, Nombre = "DERECHO PROCESAL PENAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 90, Nombre = "DERECHO ROMANO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 91, Nombre = "DERECHOS FUNDAMENTALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 92, Nombre = "DESARROLLO COMUNITARIO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 93, Nombre = "DESARROLLO DE EMPRENDEDORES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 94, Nombre = "DESARROLLO DE HABILIDADES DIRECTIVAS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 95, Nombre = "DESARROLLO DE INTERFACES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 96, Nombre = "DESARROLLO DE LA MICRO, PEQUE�A Y MEDIANA EMPRESA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 97, Nombre = "DESARROLLO DE PERSONAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 98, Nombre = "DESARROLLO DE SOFTWARE EDUCATIVO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 99, Nombre = "DESARROLLO DEL PENSAMIENTO PEDAGOGICO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 100, Nombre = "DESARROLLO HUMANO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 101, Nombre = "DESARROLLO TURISTICO REGIONAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 102, Nombre = "DIDACTICA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 103, Nombre = "DIRECCION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 104, Nombre = "DISE�O CURRICULAR" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 105, Nombre = "DISE�O INSTRUCCIONAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 106, Nombre = "DISE�O Y MULTIMEDIA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 107, Nombre = "DISE�O Y PRODUCCION DE GUIONES EDUCATIVOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 108, Nombre = "ECONOMIA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 109, Nombre = "ECONOMIA INTERNACIONAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 110, Nombre = "ECONOMIA, POLITICA Y LA SOCIEDAD DE LA INFORMACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 111, Nombre = "EDUCACION EN VALORES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 112, Nombre = "EDUCACION MULTICULTURAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 113, Nombre = "ELABORACION Y REVISION DE PEDIMENTOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 114, Nombre = "ELEMENTOS DE DEFENSA Y RECURSOS FISCALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 115, Nombre = "ENLACES DE TELECOMUNICACIONES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 116, Nombre = "EPISTEMOLOGIA Y PARADIGMAS DE LAS CIENCIAS SOCIALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 117, Nombre = "EPISTEMOLOGIA Y PARADIGMAS EN CIENCIAS SOCIALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 118, Nombre = "ESTADISTICA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 119, Nombre = "ESTADISTICA DESCRIPTIVA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 120, Nombre = "ESTADISTICA DESCRIPTIVA Y TECNICAS DE ENCUESTA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 121, Nombre = "ESTADISTICA INFERENCIAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 122, Nombre = "ESTADOS FINANCIEROS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 123, Nombre = "ESTRATEGIAS DE GESTION DIRECTIVA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 124, Nombre = "ESTRATEGIAS DE MERCADOTECNIA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 125, Nombre = "ESTRATEGIAS DE MERCADOTECNIA Y PRODUCCION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 126, Nombre = "ESTRATEGIAS FISCALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 127, Nombre = "ESTRATEGIAS PROMOCIONALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 128, Nombre = "ESTRUCTURAL FUNCIONALISMO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 129, Nombre = "ESTUDIO TRIBUTARIO DE SUELDOS Y SALARIOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 130, Nombre = "ETICA DE LOS NEGOCIOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 131, Nombre = "ETICA Y RESPONSABILIDAD SOCIAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 132, Nombre = "EVALUACION CURRICULAR" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 133, Nombre = "EVALUACION DE LOS APRENDIZAJES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 134, Nombre = "EXPERIENCIA RECEPCIONAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 135, Nombre = "FILOSOFIA DE LA EDUCACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 136, Nombre = "FILOSOFIA DEL DERECHO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 137, Nombre = "FINANZAS CORPORATIVAS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 138, Nombre = "FINANZAS INTERNACIONALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 139, Nombre = "FINANZAS PUBLICAS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 140, Nombre = "FORMACION DEL PRECIO DE EXPORTACION Y FORMAS DE PAGO INTERNACIONAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 141, Nombre = "FORMULACION Y EVALUACION DE PROYECTOS SOCIALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 142, Nombre = "FUNDAMENTOS DE ADMINISTRACION FINANCIERA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 143, Nombre = "FUNDAMENTOS DE ADMINISTRACION PUBLICA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 144, Nombre = "FUNDAMENTOS DE ALGEBRA LINEAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 145, Nombre = "FUNDAMENTOS DE CONTABILIDAD" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 146, Nombre = "FUNDAMENTOS DE DERECHO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 147, Nombre = "FUNDAMENTOS DE LA ADMINISTRACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 148, Nombre = "FUNDAMENTOS DE LA ORIENTACION EDUCATIVA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 149, Nombre = "FUNDAMENTOS DE PROYECTOS DE INVERSION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 150, Nombre = "FUNDAMENTOS DE SISTEMAS DE INFORMACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 151, Nombre = "FUNDAMENTOS PSICOLOGICOS DE LA EDUCACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 152, Nombre = "GENEALOGIA DEL SISTEMA EDUCATIVO MEXICANO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 153, Nombre = "GESTION DEL TALENTO HUMANO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 154, Nombre = "GESTION POR COMPETENCIAS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 155, Nombre = "GESTION SOCIAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 156, Nombre = "HABILIDADES DEL PENSAMIENTO CRITICO Y CREATIVO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 157, Nombre = "IMPUESTOS AL COMERCIO EXTERIOR" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 158, Nombre = "IMPUESTOS AL COMERCIO EXTERIOR Y TRATADOS INTERNACIONALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 159, Nombre = "IMPUESTOS INDIRECTOS Y CONTRIBUCIONES ESTATALES Y LOCALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 160, Nombre = "IMPUESTOS INDIRECTOS Y CONTRIBUCIONES, ESTATALES Y LOCALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 161, Nombre = "IMPUESTOS PERSONAS FISICAS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 162, Nombre = "IMPUESTOS PERSONAS MORALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 163, Nombre = "INFORMATICA APLICADA A LA EDUCACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 164, Nombre = "INGENIERIA DE SOFTWARE AVANZADA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 165, Nombre = "INGENIERIA FINANCIERA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 166, Nombre = "INGLES I" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 167, Nombre = "INGLES II" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 168, Nombre = "INNOVACION TECNOLOGICA EN LOS NEGOCIOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 169, Nombre = "INSTITUCIONES DE DERECHO PRIVADO ROMANO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 170, Nombre = "INTERACCIONISMO SOCIAL Y VIDA COTIDIANA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 171, Nombre = "INTERVENCION EN LA COMUNIDAD" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 172, Nombre = "INTRODUCCION A LA ADMINISTRACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 173, Nombre = "INTRODUCCION A LA AUDITORIA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 174, Nombre = "INTRODUCCION A LA PEDAGOGIA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 175, Nombre = "INTRODUCCION A LAS FINANZAS PUBLICAS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 176, Nombre = "INTRODUCCION AL AMPARO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 177, Nombre = "INTRODUCCION AL COMERCIO EXTERIOR" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 178, Nombre = "INTRODUCCION AL DERECHO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 179, Nombre = "INVESTIGACION DE MERCADOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 180, Nombre = "INVESTIGACION DE OPERACIONES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 181, Nombre = "ISR PERSONAS FISICAS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 182, Nombre = "ISR PERSONAS MORALES E IMPUESTO AL ACTIVO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 183, Nombre = "LABORATORIO DE DOCENCIA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 184, Nombre = "LECTURA Y REDACCION A TRAVES DEL ANALISIS DEL MUNDO CONTEMPORANEO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 185, Nombre = "LEGISLACION EN TECNOLOGIAS DE INFORMACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 186, Nombre = "LOGISTICA INTERNACIONAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 187, Nombre = "MACROECONOMIA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 188, Nombre = "MARCO JURIDICO DEL COMERCIO EXTERIOR I" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 189, Nombre = "MARCO JURIDICO DEL COMERCIO EXTERIOR II" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 190, Nombre = "MARCO JURIDICO FISCAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 191, Nombre = "MARCO JURIDICO GUBERNAMENTAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 192, Nombre = "MARCO JURIDICO Y PROGRAMAS DE APOYO A LOS NEGOCIOS INTERNACIONALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 193, Nombre = "MARCO LEGAL DE LAS RELACIONES COMERCIALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 194, Nombre = "MARCO LEGAL DE LAS RELACIONES LABORALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 195, Nombre = "MARCO TRIBUTARIO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 196, Nombre = "MATEMATICAS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 197, Nombre = "MATEMATICAS ADMINISTRATIVAS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 198, Nombre = "MATEMATICAS FINANCIERAS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 199, Nombre = "MERCADOS E INSTITUCIONES FINANCIERAS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 200, Nombre = "MERCADOS FINANCIEROS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 201, Nombre = "MERCADOTECNIA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 202, Nombre = "MERCADOTECNIA DE SERVICIOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 203, Nombre = "MERCADOTECNIA INTERNACIONAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 204, Nombre = "MERCADOTECNIA SOCIAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 205, Nombre = "METODOLOGIA DE LA CIENCIA DEL DERECHO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 206, Nombre = "METODOLOGIA DE LA INVESTIGACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 207, Nombre = "METODOLOGIA DE LA INVESTIGACION CUALITATIVA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 208, Nombre = "METODOLOGIA DE LA INVESTIGACION CUANTITATIVA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 209, Nombre = "METODOLOGIA DE LA INVESTIGACION JURIDICA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 210, Nombre = "METODOLOGIA DE LA INVESTIGACION SOCIOLOGICA I" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 211, Nombre = "METODOLOGIA DE LA INVESTIGACION SOCIOLOGICA II" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 212, Nombre = "METODOLOGIA DE LA ORIENTACION EDUCATIVA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 213, Nombre = "METODOLOGIAS DE LA INVESTIGACION CUALITATIVA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 214, Nombre = "METODOS CUANTITATIVOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 215, Nombre = "MEXICO: ECONOMIA POLITICA Y SOCIEDAD. PRIMERA MITAD DEL SIGLO XX" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 216, Nombre = "MEXICO: ECONOMIA POLITICA Y SOCIEDAD. SEGUNDA MITAD DEL SIGLO XX" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 217, Nombre = "MEXICO: ECONOMIA POLITICA Y SOCIEDAD. SIGLO XIX" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 218, Nombre = "MICROECONOMIA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 219, Nombre = "MODELOS DE DESARROLLO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 220, Nombre = "NEGOCIOS INTERNACIONALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 221, Nombre = "NUEVAS TECNOLOGIAS EN EDUCACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 222, Nombre = "NUEVAS TECNOLOGIAS EN LA EDUCACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 223, Nombre = "OPERACIONES Y PRODUCTOS BANCARIOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 224, Nombre = "ORGANIZACION CONTABLE" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 225, Nombre = "ORGANIZACION Y COMERCIALIZACION DE EVENTOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 226, Nombre = "ORGANIZACION Y REINGENIERIA DE PROCESOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 227, Nombre = "PENSAMIENTO ADMINISTRATIVO Y RESPONSABILIDAD SOCIAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 228, Nombre = "PLANEACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 229, Nombre = "PLANEACION DIDACTICA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 230, Nombre = "PLANEACION EDUCATIVA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 231, Nombre = "PLANEACION ESTRATEGICA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 232, Nombre = "PLANEACION PARA EL DESARROLLO MUNICIPAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 233, Nombre = "POLITICA Y LEGISLACION EDUCATIVA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 234, Nombre = "POLITICAS SOCIALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 235, Nombre = "PORTAFOLIO DE INVERSION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 236, Nombre = "PRACTICA DE DIDACTICA Y CURRICULO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 237, Nombre = "PRACTICAS BURSATILES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 238, Nombre = "PRACTICAS DE ORIENTACION EDUCATIVA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 239, Nombre = "PRACTICAS FISCALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 240, Nombre = "PRESUPUESTOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 241, Nombre = "PREVISION SOCIAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 242, Nombre = "PROBLEMAS SOCIOECONOMICOS DE MEXICO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 243, Nombre = "PROCESO DE EVALUACION INSTITUCIONAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 244, Nombre = "PROCESO DE INVESTIGACION EN CIENCIAS SOCIALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 245, Nombre = "PROCESO GRUPAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 246, Nombre = "PROCESOS DE AMPARO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 247, Nombre = "PROCESOS DE LAS ORGANIZACIONES EDUCATIVAS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 248, Nombre = "PROCESOS Y OPERACIONES FABRILES Y DE SERVICIOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 249, Nombre = "PROGRAMACION AVANZADA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 250, Nombre = "PROTOCOLO DE NEGOCIOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 251, Nombre = "PROTOCOLO DEL TRABAJO RECEPCIONAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 252, Nombre = "PROYECTO DE EDUCACION COMUNITARIA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 253, Nombre = "PROYECTO DE INVESTIGACION EDUCATIVA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 254, Nombre = "PROYECTO DE SERVICIOS EDUCATIVOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 255, Nombre = "PROYECTOS DE INVERSION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 256, Nombre = "PROYECTOS DE ORIENTACION EDUCATIVA VINCULADOS A LA COMUNIDAD" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 257, Nombre = "PROYECTOS DE VINCULACION ORIENTADOS A LA COMUNIDAD" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 258, Nombre = "PROYECTOS TURISTICOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 259, Nombre = "PSICOLOGIA DEL APRENDIZAJE" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 260, Nombre = "PSICOLOGIA DEL TRABAJO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 261, Nombre = "PSICOLOGIA EVOLUTIVA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 262, Nombre = "REALIDAD VIRTUAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 263, Nombre = "RELACIONES HUMANAS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 264, Nombre = "RESIDENCIAS LABORALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 265, Nombre = "SEGURIDAD EN REDES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 266, Nombre = "SEGURIDAD SOCIAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 267, Nombre = "SEMINARIO DE CONTABILIDAD ADMINISTRATIVA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 268, Nombre = "SEMINARIO DE FINANZAS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 269, Nombre = "SEMINARIO DE FISCAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 270, Nombre = "SERVICIO SOCIAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 271, Nombre = "SIMULACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 272, Nombre = "SIMULACION DE NEGOCIOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 273, Nombre = "SISTEMAS DE INFORMACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 274, Nombre = "SISTEMAS DE INFORMACION CONTABLE" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 275, Nombre = "SOCIOLOGIA CLASICA: MARX, DURKHEIM Y WEBER" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 276, Nombre = "SOCIOLOGIA DE LA EDUCACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 277, Nombre = "SOCIOLOGIA JURIDICA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 278, Nombre = "SOFTWARE DE APLICACION ADMINISTRATIVA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 279, Nombre = "SOFTWARE EDUCATIVO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 280, Nombre = "SOLUCIONES TECNOLOGICAS APLICABLES A LAS ORGANIZACIONES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 281, Nombre = "TECNOLOGIA DE INFORMACION Y COMUNICACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 282, Nombre = "TEMAS SELECTOS DE ORIENTACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 283, Nombre = "TEMAS SELECTOS DE ORIENTACION EDUCATIVA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 284, Nombre = "TEORIA CRITICA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 285, Nombre = "TEORIA DEL ESTADO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 286, Nombre = "TEORIA ECONOMICA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 287, Nombre = "TEORIA POLITICA" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 288, Nombre = "TEORIA SOCIAL EN EL SIGLO XIX" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 289, Nombre = "TEORIAS DE LA ESTRUCTURACION SOCIAL" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 290, Nombre = "TEORIAS DE LOS SISTEMAS SOCIALES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 291, Nombre = "TITULOS Y OPERACIONES DE CREDITO" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 292, Nombre = "TOPICOS DE BASES DE DATOS" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 293, Nombre = "TOPICOS DE PROGRAMACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 294, Nombre = "TOPICOS SELECTOS DE REDES" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 295, Nombre = "TOPICOS SELECTOS DE SISTEMAS DE INFORMACION" });
            context.ExperienciasEducativas.AddOrUpdate(new VentanillaExperienciaEducativa() { Id = 296, Nombre = "TRATADOS DE LIBRE COMERCIO Y ACUERDOS COMERCIALES" });

            context.SaveChanges();
        }

        void AddRegionPrograma(VentanillaElectronica.Models.VentanillaDBContext context)
        {
            context.RegionPrograma.AddOrUpdate(new RegionPrograma() { Id = 1, Region = EnumRegion.XALAPA, Programa = EnumPrograma.ADMINISTRACION });
            context.RegionPrograma.AddOrUpdate(new RegionPrograma() { Id = 2, Region = EnumRegion.XALAPA, Programa = EnumPrograma.CONTADURIA });
            context.RegionPrograma.AddOrUpdate(new RegionPrograma() { Id = 3, Region = EnumRegion.XALAPA, Programa = EnumPrograma.DERECHO });
            context.RegionPrograma.AddOrUpdate(new RegionPrograma() { Id = 4, Region = EnumRegion.XALAPA, Programa = EnumPrograma.PEDAGOGIA });

            context.RegionPrograma.AddOrUpdate(new RegionPrograma() { Id = 5, Region = EnumRegion.VERACRUZ, Programa = EnumPrograma.ADMINISTRACION });
            context.RegionPrograma.AddOrUpdate(new RegionPrograma() { Id = 6, Region = EnumRegion.VERACRUZ, Programa = EnumPrograma.CONTADURIA });
            context.RegionPrograma.AddOrUpdate(new RegionPrograma() { Id = 7, Region = EnumRegion.VERACRUZ, Programa = EnumPrograma.DERECHO });

            context.RegionPrograma.AddOrUpdate(new RegionPrograma() { Id = 8, Region = EnumRegion.ORIZABA, Programa = EnumPrograma.ADMINISTRACION });
            context.RegionPrograma.AddOrUpdate(new RegionPrograma() { Id = 9, Region = EnumRegion.ORIZABA, Programa = EnumPrograma.CONTADURIA });
            context.RegionPrograma.AddOrUpdate(new RegionPrograma() { Id = 10, Region = EnumRegion.ORIZABA, Programa = EnumPrograma.DERECHO });
            context.RegionPrograma.AddOrUpdate(new RegionPrograma() { Id = 11, Region = EnumRegion.ORIZABA, Programa = EnumPrograma.SOCIOLOGIA });

            context.RegionPrograma.AddOrUpdate(new RegionPrograma() { Id = 12, Region = EnumRegion.POZA_RICA, Programa = EnumPrograma.CONTADURIA });
            context.RegionPrograma.AddOrUpdate(new RegionPrograma() { Id = 13, Region = EnumRegion.POZA_RICA, Programa = EnumPrograma.DERECHO });

            context.RegionPrograma.AddOrUpdate(new RegionPrograma() { Id = 14, Region = EnumRegion.COATZACOALCOS, Programa = EnumPrograma.CONTADURIA });
            context.RegionPrograma.AddOrUpdate(new RegionPrograma() { Id = 15, Region = EnumRegion.COATZACOALCOS, Programa = EnumPrograma.DERECHO });

            context.SaveChanges();

        }


        void AddPeriodos(VentanillaElectronica.Models.VentanillaDBContext context)
        {
            context.Periodos.AddOrUpdate(new VentanillaPeriodo() { Id = 1, Nombre = "FEBRERO JULIO 2017", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.SaveChanges();
        }

        void AddUsuarios(VentanillaElectronica.Models.VentanillaDBContext context)
        {
            context.Administrativos.AddOrUpdate(new VentanillaAdministrativo() { Id = 1, Nombre = "Ra�l Fabi�n Colorado Pimentel", UserName = "rcolorado", Email = "rcolorado@uv.mx", Telefono= "01(228) 842 1700 Ext. 10624",  FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.Administrativos.AddOrUpdate(new VentanillaAdministrativo() { Id = 2, Nombre = "Gabriela Calles Barradas", UserName = "gcalles", Email = "gcalles@uv.mx", Telefono = "01(228) 842 1700 Ext. 10677", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.Administrativos.AddOrUpdate(new VentanillaAdministrativo() { Id = 3, Nombre = "Ana� Adelita Hern�ndez S�las", UserName = "anaihernandez", Email = "anaihernandez@uv.mx", Telefono = "01(228) 842 1700 Ext. 10640", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });

            //Pedagog�a
            context.Administrativos.AddOrUpdate(new VentanillaAdministrativo() { Id = 4, Nombre = "Ma. de Lourdes Bonilla Garc�a", UserName = "lobonilla", Email = "lobonilla@uv.mx", Telefono = "01(228) 842 1700 Ext. 10664", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            //Derecho
            context.Administrativos.AddOrUpdate(new VentanillaAdministrativo() { Id = 5, Nombre = "Gisela Mendoza Hern�ndez", UserName = "gismendoza", Email = "gismendoza@uv.mx", Telefono = "01(228) 842 1700 Ext. 10664", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            //Contadur�a
            context.Administrativos.AddOrUpdate(new VentanillaAdministrativo() { Id = 6, Nombre = "Nora Montano Rodr�guez", UserName = "nmontano", Email = "nmontano@uv.mx", Telefono = "01(228) 842 1700 Ext. 10664", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            //Administraci�n
            context.Administrativos.AddOrUpdate(new VentanillaAdministrativo() { Id = 7, Nombre = "Donaji Luna Garc�s", UserName = "doluna", Email = "doluna@uv.mx", Telefono = "01(228) 842 1700 Ext. 10665", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.SaveChanges();
        }

        void AddUsuariosProgramas(VentanillaElectronica.Models.VentanillaDBContext context)
        {
            //rcolorado
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 1, Programa = EnumPrograma.ADMINISTRACION, VentanillaAdministrativoId =1, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 2, Programa = EnumPrograma.CONTADURIA, VentanillaAdministrativoId = 1,  FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 3, Programa = EnumPrograma.DERECHO, VentanillaAdministrativoId = 1, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 4, Programa = EnumPrograma.PEDAGOGIA, VentanillaAdministrativoId = 1, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 5, Programa = EnumPrograma.SOCIOLOGIA, VentanillaAdministrativoId = 1, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            //gcalles
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 6, Programa = EnumPrograma.ADMINISTRACION, VentanillaAdministrativoId = 2, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 7, Programa = EnumPrograma.CONTADURIA, VentanillaAdministrativoId = 2, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 8, Programa = EnumPrograma.DERECHO, VentanillaAdministrativoId = 2, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 9, Programa = EnumPrograma.PEDAGOGIA, VentanillaAdministrativoId = 2, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 10, Programa = EnumPrograma.SOCIOLOGIA, VentanillaAdministrativoId = 2, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            //anaihernandez
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 11, Programa = EnumPrograma.ADMINISTRACION, VentanillaAdministrativoId = 3, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 12, Programa = EnumPrograma.CONTADURIA, VentanillaAdministrativoId = 3, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 13, Programa = EnumPrograma.DERECHO, VentanillaAdministrativoId = 3, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 14, Programa = EnumPrograma.PEDAGOGIA, VentanillaAdministrativoId = 3, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 15, Programa = EnumPrograma.SOCIOLOGIA, VentanillaAdministrativoId = 3, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            //lobonilla
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 16, Programa = EnumPrograma.PEDAGOGIA, VentanillaAdministrativoId = 4, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 17, Programa = EnumPrograma.DERECHO, VentanillaAdministrativoId = 4, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            //gismendoza
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 18, Programa = EnumPrograma.PEDAGOGIA, VentanillaAdministrativoId = 5, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 19, Programa = EnumPrograma.DERECHO, VentanillaAdministrativoId = 5, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            //nmontano
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 20, Programa = EnumPrograma.CONTADURIA, VentanillaAdministrativoId = 6, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 21, Programa = EnumPrograma.ADMINISTRACION, VentanillaAdministrativoId = 6, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            //doluna
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 22, Programa = EnumPrograma.CONTADURIA, VentanillaAdministrativoId = 7, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.AdministrativosPrograma.AddOrUpdate(new VentanillaAdministrativosPrograma() { Id = 23, Programa = EnumPrograma.ADMINISTRACION, VentanillaAdministrativoId = 7, FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });


            context.SaveChanges();
        }

        void AddInscripcionBajas(VentanillaElectronica.Models.VentanillaDBContext context)
        {
            VentanillaInscripcionBajas inscripcionBajas = new VentanillaInscripcionBajas();
            inscripcionBajas.Id = 1;
            inscripcionBajas.VentanillaPeriodoId = 1;
            inscripcionBajas.FechaInscripcionInicial = DateTime.Parse("2017-01-30 12:00:00");
            inscripcionBajas.FechaInscripcionFinal = DateTime.Parse("2017-01-31 23:59:00");
            inscripcionBajas.FechaActualizacion = DateTime.Now;
            inscripcionBajas.FechaRegistro = DateTime.Now;
            context.InscripcionBajas.AddOrUpdate(inscripcionBajas);
            context.SaveChanges();
        }

        void AddInscripcionAltas(VentanillaElectronica.Models.VentanillaDBContext context)
        {
            VentanillaInscripcionAltas inscripcionAltas = new VentanillaInscripcionAltas();
            inscripcionAltas.Id = 1;
            inscripcionAltas.VentanillaPeriodoId = 1;
            inscripcionAltas.Programa = EnumPrograma.ADMINISTRACION;
            inscripcionAltas.FechaInscripcionInicial = DateTime.Parse("2017-01-30 12:00:00");
            inscripcionAltas.FechaInscripcionFinal = DateTime.Parse("2017-01-31 23:59:00");
            inscripcionAltas.FechaActualizacion = DateTime.Now;
            inscripcionAltas.FechaRegistro = DateTime.Now;
            inscripcionAltas.IsCupoCerrado = true;
            inscripcionAltas.OfertaAlta.Add(new VentanillaOfertaAltas() { Id = 1, Cupo = 10, Nombre = "Software Administrativo", NRC = "32345", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            inscripcionAltas.OfertaAlta.Add(new VentanillaOfertaAltas() { Id = 2, Cupo = 2, Nombre = "Software Administrativo II", NRC = "43567", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            inscripcionAltas.OfertaAlta.Add(new VentanillaOfertaAltas() { Id = 3, Cupo = 1, Nombre = "Software Administrativo III", NRC = "78905", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.InscripcionAltas.AddOrUpdate(inscripcionAltas);
            context.SaveChanges();

            inscripcionAltas = new VentanillaInscripcionAltas();
            inscripcionAltas.Id = 2;
            inscripcionAltas.VentanillaPeriodoId = 1;
            inscripcionAltas.Programa = EnumPrograma.CONTADURIA;
            inscripcionAltas.FechaInscripcionInicial = DateTime.Parse("2017-02-07 00:01:00");
            inscripcionAltas.FechaInscripcionFinal = DateTime.Parse("2017-02-07 23:59:00");
            inscripcionAltas.FechaActualizacion = DateTime.Now;
            inscripcionAltas.FechaRegistro = DateTime.Now;
            inscripcionAltas.IsCupoCerrado = true;
            inscripcionAltas.OfertaAlta.Add(new VentanillaOfertaAltas() { Id = 1, Cupo = 0, Nombre = "Derecho a la Seguridad Social", NRC = "56789", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.InscripcionAltas.AddOrUpdate(inscripcionAltas);
            context.SaveChanges();

            inscripcionAltas = new VentanillaInscripcionAltas();
            inscripcionAltas.Id = 3;
            inscripcionAltas.VentanillaPeriodoId = 1;
            inscripcionAltas.Programa = EnumPrograma.DERECHO;
            inscripcionAltas.FechaInscripcionInicial = DateTime.Parse("2017-01-08 00:01:00");
            inscripcionAltas.FechaInscripcionFinal = DateTime.Parse("2017-02-08 23:59:00");
            inscripcionAltas.FechaActualizacion = DateTime.Now;
            inscripcionAltas.FechaRegistro = DateTime.Now;
            inscripcionAltas.IsCupoCerrado = true;
            inscripcionAltas.OfertaAlta.Add(new VentanillaOfertaAltas() { Id = 1, Cupo = 1, Nombre = "Costos", NRC = "12345", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            inscripcionAltas.OfertaAlta.Add(new VentanillaOfertaAltas() { Id = 2, Cupo = 2, Nombre = "Costos II", NRC = "45567", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            inscripcionAltas.OfertaAlta.Add(new VentanillaOfertaAltas() { Id = 3, Cupo = 3, Nombre = "Costos III", NRC = "76876", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            inscripcionAltas.OfertaAlta.Add(new VentanillaOfertaAltas() { Id = 4, Cupo = 4, Nombre = "Costos V", NRC = "24897", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.InscripcionAltas.AddOrUpdate(inscripcionAltas);
            context.SaveChanges();

            inscripcionAltas = new VentanillaInscripcionAltas();
            inscripcionAltas.Id = 4;
            inscripcionAltas.VentanillaPeriodoId = 1;
            inscripcionAltas.Programa = EnumPrograma.PEDAGOGIA;
            inscripcionAltas.FechaInscripcionInicial = DateTime.Parse("2017-02-08 00:01:00");
            inscripcionAltas.FechaInscripcionFinal = DateTime.Parse("2017-02-09 23:59:00");
            inscripcionAltas.FechaActualizacion = DateTime.Now;
            inscripcionAltas.FechaRegistro = DateTime.Now;
            inscripcionAltas.IsCupoCerrado = true;
            inscripcionAltas.OfertaAlta.Add(new VentanillaOfertaAltas() { Id = 1, Cupo = 1, Nombre = "PEDAGOGIA", NRC = "98765", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            inscripcionAltas.OfertaAlta.Add(new VentanillaOfertaAltas() { Id = 2, Cupo = 2, Nombre = "PEDAGOGIA II", NRC = "87654", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            inscripcionAltas.OfertaAlta.Add(new VentanillaOfertaAltas() { Id = 3, Cupo = 3, Nombre = "PEDAGOGIA III", NRC = "65432", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            inscripcionAltas.OfertaAlta.Add(new VentanillaOfertaAltas() { Id = 4, Cupo = 4, Nombre = "PEDAGOGIA V", NRC = "43210", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.InscripcionAltas.AddOrUpdate(inscripcionAltas);

       
            inscripcionAltas = new VentanillaInscripcionAltas();
            inscripcionAltas.Id = 5;
            inscripcionAltas.VentanillaPeriodoId = 1;
            inscripcionAltas.Programa = EnumPrograma.SOCIOLOGIA;
            inscripcionAltas.FechaInscripcionInicial = DateTime.Parse("2017-02-08 00:01:00");
            inscripcionAltas.FechaInscripcionFinal = DateTime.Parse("2017-02-09 23:59:00");
            inscripcionAltas.FechaActualizacion = DateTime.Now;
            inscripcionAltas.FechaRegistro = DateTime.Now;
            inscripcionAltas.IsCupoCerrado = true;
            inscripcionAltas.OfertaAlta.Add(new VentanillaOfertaAltas() { Id = 2, Cupo = 2, Nombre = "SOCIOLOGIA II", NRC = "87654", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            inscripcionAltas.OfertaAlta.Add(new VentanillaOfertaAltas() { Id = 3, Cupo = 3, Nombre = "SOCIOLOGIA III", NRC = "65432", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            context.InscripcionAltas.AddOrUpdate(inscripcionAltas);

            context.SaveChanges();
        }

        void AddInscripcionMovilidad(VentanillaElectronica.Models.VentanillaDBContext context)
        {
            VentanillaInscripcionMovilidad inscripcionMovilidad = new VentanillaInscripcionMovilidad();
            inscripcionMovilidad.Id = 1;
            inscripcionMovilidad.VentanillaPeriodoId = 1;
            inscripcionMovilidad.Programa = EnumPrograma.ADMINISTRACION;
            inscripcionMovilidad.FechaInscripcionInicial = DateTime.Parse("2017-02-07 00:01:00");
            inscripcionMovilidad.FechaInscripcionFinal = DateTime.Parse("2017-02-07 23:59:00");
            inscripcionMovilidad.FechaActualizacion = DateTime.Now;
            inscripcionMovilidad.FechaRegistro = DateTime.Now;
            inscripcionMovilidad.IsCupoCerrado = true;
            inscripcionMovilidad.OfertaMovilidad.Add(new VentanillaOfertaMovilidad() { Id = 1, Cupo = 10, Nombre = "Software Administrativo", NRC = "32345", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            inscripcionMovilidad.OfertaMovilidad.Add(new VentanillaOfertaMovilidad() { Id = 2, Cupo = 2, Nombre = "Software Administrativo II", NRC = "43567", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            inscripcionMovilidad.OfertaMovilidad.Add(new VentanillaOfertaMovilidad() { Id = 3, Cupo = 1, Nombre = "Software Administrativo III", NRC = "78905", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });

            context.InscripcionMovilidad.AddOrUpdate(inscripcionMovilidad);
            context.SaveChanges();

            inscripcionMovilidad = new VentanillaInscripcionMovilidad();
            inscripcionMovilidad.Id = 2;
            inscripcionMovilidad.VentanillaPeriodoId = 1;
            inscripcionMovilidad.Programa = EnumPrograma.CONTADURIA;
            inscripcionMovilidad.FechaInscripcionInicial = DateTime.Parse("2017-02-07 00:01:00");
            inscripcionMovilidad.FechaInscripcionFinal = DateTime.Parse("2017-02-07 23:59:00");
            inscripcionMovilidad.FechaActualizacion = DateTime.Now;
            inscripcionMovilidad.FechaRegistro = DateTime.Now;
            inscripcionMovilidad.IsCupoCerrado = true;
            inscripcionMovilidad.OfertaMovilidad.Add(new VentanillaOfertaMovilidad() { Id = 1, Cupo = 0, Nombre = "Derecho a la Seguridad Social", NRC = "56789", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });

            context.InscripcionMovilidad.AddOrUpdate(inscripcionMovilidad);
            context.SaveChanges();

            inscripcionMovilidad = new VentanillaInscripcionMovilidad();
            inscripcionMovilidad.Id = 3;
            inscripcionMovilidad.VentanillaPeriodoId = 1;
            inscripcionMovilidad.Programa = EnumPrograma.DERECHO;
            inscripcionMovilidad.FechaInscripcionInicial = DateTime.Parse("2017-02-08 00:01:00");
            inscripcionMovilidad.FechaInscripcionFinal = DateTime.Parse("2017-02-08 23:59:00");
            inscripcionMovilidad.FechaActualizacion = DateTime.Now;
            inscripcionMovilidad.FechaRegistro = DateTime.Now;
            inscripcionMovilidad.IsCupoCerrado = true;

            inscripcionMovilidad.OfertaMovilidad.Add(new VentanillaOfertaMovilidad() { Id = 1, Cupo = 1, Nombre = "Costos", NRC = "12345", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            inscripcionMovilidad.OfertaMovilidad.Add(new VentanillaOfertaMovilidad() { Id = 2, Cupo = 2, Nombre = "Costos II", NRC = "45567", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            inscripcionMovilidad.OfertaMovilidad.Add(new VentanillaOfertaMovilidad() { Id = 3, Cupo = 3, Nombre = "Costos III", NRC = "76876", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });
            inscripcionMovilidad.OfertaMovilidad.Add(new VentanillaOfertaMovilidad() { Id = 4, Cupo = 4, Nombre = "Costos V", NRC = "24897", FechaRegistro = DateTime.Now, FechaActualizacion = DateTime.Now });



            context.InscripcionMovilidad.AddOrUpdate(inscripcionMovilidad);
            context.SaveChanges();

            inscripcionMovilidad = new VentanillaInscripcionMovilidad();
            inscripcionMovilidad.Id = 4;
            inscripcionMovilidad.VentanillaPeriodoId = 1;
            inscripcionMovilidad.Programa = EnumPrograma.PEDAGOGIA;
            inscripcionMovilidad.FechaInscripcionInicial = DateTime.Parse("2017-02-08 00:01:00");
            inscripcionMovilidad.FechaInscripcionFinal = DateTime.Parse("2017-02-09 23:59:00");
            inscripcionMovilidad.FechaActualizacion = DateTime.Now;
            inscripcionMovilidad.FechaRegistro = DateTime.Now;
            inscripcionMovilidad.IsCupoCerrado = true;

            context.InscripcionMovilidad.AddOrUpdate(inscripcionMovilidad);

            context.SaveChanges();
        }

     
    }
}
