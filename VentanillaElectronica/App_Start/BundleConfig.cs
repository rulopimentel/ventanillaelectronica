﻿using System;
using System.Web.Optimization;

namespace VentanillaElectronica
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-{version}.js", "~/Scripts/jquery-ui-1.12.1.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/moment.js", "~/Scripts/bootstrap.js", "~/Scripts/bootstrap-datetimepicker.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/ajax").Include("~/Scripts/jquery.unobtrusive-ajax.js"));
            bundles.Add(new ScriptBundle("~/bundles/validate").Include("~/Scripts/jquery.validate.js", "~/Scripts/jquery.validate.unobtrusive.js", "~/Scripts/jquery.validate.unobtrusive.bootstrap.js", "~/Scripts/fileinput.js", "~/Scripts/locales/es.js"));
            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/bootstrap.css", "~/Content/bootstrap-fileinput/css/fileinput.css",  "~/Content/PagedList.css", "~/Content/bootstrap-datetimepicker.min.css", "~/Content/themes/base/base.css", "~/Content/custom.css"));



        }
    }
}